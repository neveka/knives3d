using System;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Tween the audio source's volume.
/// </summary>

[AddComponentMenu( "NGUI/Tween/Tween Value (float)" )]
public class TweenValue : UITweener
{
    public float from = 0f;
    public float to = 1f;

    float _value;

    public Transform target;

    public string customFunction = "";

    /// <summary>
    /// Cached version of 'audio', as it's always faster to cache.
    /// </summary>

    /// <summary>
    /// Audio source's current volume.
    /// </summary>

    public float Value { get { return _value; } set { _value = value; } }

    /// <summary>
    /// Tween update function.
    /// </summary>
    [HideInInspector]
	public List<Action<float>> onValueChanged = new List<Action<float>>();

    override protected void OnUpdate( float factor , bool isFinished )
    {
        _value = from * ( 1f - factor ) + to * factor;
        for( int i = 0 ; i < onValueChanged.Count ; ++i )
        {
            var tmp = onValueChanged[ i ];
            if( tmp != null )
                tmp(_value );
        }

        if( isFinished )
            onValueChanged.Clear( );
        //         if( target != null )
        //             target.SendMessage( string.IsNullOrEmpty( customFunction ) ? "SetValue" : customFunction , _value );
    }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>

    static public TweenValue Begin( GameObject go , float duration , float targetValue )
    {
        TweenValue comp = UITweener.Begin<TweenValue>( go , duration );
        comp.customFunction = "";
        comp.from = comp._value;
        comp.to = targetValue;
        return comp;
    }
}
