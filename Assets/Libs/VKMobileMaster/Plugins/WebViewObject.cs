#if UNITY_ANDROID

using System;
using UnityEngine;

public class WebViewObject : MonoBehaviour
{
    private Action<string> _callback;
    private AndroidJavaObject _webView;

    public void Init(Action<string> callback = null)
    {
        _callback = callback;
        _webView = new AndroidJavaObject("net.gree.unitywebview.WebViewPlugin");
        _webView.Call("Init", name);
    }

    public void OnDestroy()
    {
        _webView.Call("Destroy");
    }

    public void SetMargins(int left, int top, int right, int bottom)
    {
        _webView.Call("SetMargins", left, top, right, bottom);
    }

    public void SetVisibility(bool v)
    {
        _webView.Call("SetVisibility", v);
    }

    public void LoadUrl(string url)
    {
        _webView.Call("LoadURL", url);
    }

    public void EvaluateJs(string js)
    {
        _webView.Call("LoadURL", "javascript:" + js);
    }

    public void onGetAccessToken(string accessToken)
    {
        Debug.Log ("Unity > onGetAccessToken " + accessToken);

        if (_callback != null) _callback(accessToken);
    }
}

#endif