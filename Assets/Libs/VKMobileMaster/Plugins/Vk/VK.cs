

using System;
using UnityEngine;
using System.Collections;
//using Assets.Scripts.Common;

public class VK
{
	public static VK Instance
	{
		get{
			if(_instance == null)
				_instance = new VK();
			return _instance;
		}
	}
	private static VK _instance;

    private string _accessToken;
    private const string AuthorizePattern = "https://oauth.vk.com/authorize?client_id=CLIENT_ID&scope=SCOPE&redirect_uri=https://oauth.vk.com/blank.html&response_type=token&display=touch";
	private const string LogoutPattern =    "https://oauth.vk.com/logout?client_id=CLIENT_ID&access_token=ACCESS_TOKEN&origin=http://vk.com";
	//https://login.vk.com/?act=openapi&oauth=1&aid=APP_ID&location=YOUR_DOMAIN&do_logout=1&token=TOKEN
	private const string ApiPattern = "https://api.vk.com/method/METHOD_NAME?PARAMETERS&access_token=ACCESS_TOKEN";

	public string AccessToken
	{
		get{return _accessToken;}
	}

    public void Authorize(string appId, string scope, Action<string> callback)
    {
		#if UNITY_ANDROID
        var container = new GameObject(typeof(WebViewObject).Name);
        var browser = container.AddComponent<WebViewObject>();
        browser.Init(token =>
        {
            Debug.Log("AccessToken " + token);

            _accessToken = token;

            if (callback != null)
            {
                callback(token);
            }

            GameObject.Destroy(container);
        });

        var authorizeLink = AuthorizePattern.Replace("CLIENT_ID", appId).Replace("SCOPE", scope);

        Debug.Log("authorizeLink: " + authorizeLink);

		browser.LoadUrl(authorizeLink);
		browser.SetMargins(5, 100, 5, Screen.height / 4);
        browser.SetVisibility(true);
		#else
		_accessToken = "ade1aaf636661541abaf32056df3f08d83210aba5480b7a82fc8f4dd719ba2a2a465c65d5ad8c144baeda";
		callback(_accessToken);
		#endif
	}

	public void Logout(string appId, Action<string> callback)
	{
		#if UNITY_ANDROID
		/*var container = new GameObject(typeof(WebViewObject).Name);
		var browser = container.AddComponent<WebViewObject>();
		browser.Init(result =>
		             {
			_accessToken = null;*/
			
			if (callback != null)
			{
				callback(/*result*/"Ok");
			}
			
			/*GameObject.Destroy(container);
		});
		var url = LogoutPattern.Replace("CLIENT_ID", appId);
		url = url.Replace("ACCESS_TOKEN", _accessToken);

		browser.LoadUrl(url);
		browser.SetMargins(5, 100, 5, Screen.height / 4);
		browser.SetVisibility(true);*/
#endif
		_accessToken = null;//?
	}

    public IEnumerator CallApiCoroutine(string methodName, IDictionary parameters, Action<string> callback)
	{
		var url = ApiPattern.Replace("METHOD_NAME", methodName);
		var paramValues = "";
		
		foreach (var item in parameters.Keys)
        {
			paramValues += string.Format("{0}={1}&", item, WWW.EscapeURL(parameters[item].ToString()));
		}
		
		url = url.Replace("PARAMETERS", paramValues);
        url = url.Replace("ACCESS_TOKEN", _accessToken);
		
		Debug.Log ("url: " + url);
		
		var www = new WWW (url);

		yield return www;

		Debug.Log ("www.text: " + www.text);

	    if (callback != null)
	    {
	        callback(www.text);
	    }
	}
}
