﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class SkinnedClothes : MonoBehaviour {

	public Transform[] bones;
	public bool bindposes;
	SkinnedMeshRenderer skin;

	// Use this for initialization
	void Start () 
	{
		skin = GetComponent<SkinnedMeshRenderer>();
		/*int k=0;
		foreach(Transform bone in skin.bones)
		{
			Debug.LogWarning(k+" "+(bone==null?"null":bone.name));
			k++;
		}
		/*foreach(BoneWeight bw in skin.sharedMesh.boneWeights)
			Debug.LogWarning(bw.boneIndex0+"=>"+bw.weight0);*/


		if(bones!=null && bones.Length == skin.bones.Length)
		{
			if(bindposes)
			{
				Matrix4x4[] bindPoses = new Matrix4x4[bones.Length];
				for(int i = 0; i < bones.Length; i++)
				{
					Transform b = bones[i];
					bindPoses[i] = b.worldToLocalMatrix * transform.localToWorldMatrix;
				}
				skin.sharedMesh.bindposes = bindPoses;
			}
			skin.bones = bones;
		}
	}
}
