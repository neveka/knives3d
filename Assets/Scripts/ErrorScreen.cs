﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ErrorScreen : GameScreen 
{
	public Text title;
	public InputField inputText;

	public void ShowInfo(string titleText, string text, Action onClose)
	{
		title.text = titleText;
		inputText.text = text;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}
}
