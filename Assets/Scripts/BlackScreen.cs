﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class BlackScreen : GameScreen 
{
	public Text title;

	public void ShowInfo(Action onClose)
	{
		_onClose = onClose;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}

	public void Close()
	{
		if(!_justCreated)
		{
			if(_onClose != null)
				_onClose();
			Destroy(gameObject);
		}
	}
}
