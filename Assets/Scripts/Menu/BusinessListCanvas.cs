using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class BusinessListCanvas : ListCanvas
{
    public BusinessItemPanel item;
	public BusinessItemPanel selection;
	public List<Button> filterButtons = new List<Button>();
	public int selectedFilter;
	public Action<object> onSelect;
	public Action<object> onDeselect;
    //public CurrencyPanel cornerCurrencyPanel;

	void Awake()
	{
		item.gameObject.SetActive (false);
	}

	void Update()
	{
		RectTransform rt = GetComponent<RectTransform> ();
		if(rt && Camera.main)
			rt.sizeDelta = new Vector2(rt.sizeDelta.x, rt.sizeDelta.x / Camera.main.aspect);
	}

	public void SetStones(List<StoneData> stones)
	{
		reloadAction = ()=>SetStones(stones);
		RemoveOldItems( );
		item.OnSelect();
		List<int> amounts = new List<int>();
		List<StoneData> diffDatas = new List<StoneData>();

		for( int i = 0 ; i < stones.Count ; i++ )
		{	
			StoneData sameData = diffDatas.Find(d=>d.name == stones[i].name);
			if(sameData==null)
			{
				amounts.Add(1);
				diffDatas.Add(stones[i]);
			}
			else
			{
				amounts[diffDatas.IndexOf(sameData)]++;
			}
		}

		for( int i = 0 ; i < diffDatas.Count ; i++ )
		{
			BusinessItemPanel newItem = AddBusinessItem(diffDatas[i]);
			newItem.SetStone(diffDatas[i], amounts[i], null, reloadAction);
		}
		_items.ForEach(i=>i.GetComponent<BusinessItemPanel>().OnDeselect());//?
	}

	public void SetInteractive(bool interactive)
	{
		_items.ForEach(i=>i.GetComponent<BusinessItemPanel>().pointerButton.interactable = interactive);//?
	}

	public void SetKnives(List<KnifeData> knives, ProgressData progressData, int level)
	{
		knives.Sort((a,b)=>a.price+a.gold*1000000>b.price+b.gold*1000000?1:(a.price+a.gold*1000000==b.price+b.gold*1000000?0:-1));
		title.text = "НОЖИ";
		reloadAction = ()=>SetKnives(knives, progressData, level );
		RemoveOldItems( );
		item.OnSelect();
		for( int i = 0 ; i < knives.Count ; i++ )
		{
			if(!ShowKnife(knives[i], progressData))
				continue;
			BusinessItemPanel newItem = AddBusinessItem(knives[i]);
			newItem.SetKnife(knives[i], progressData, level, reloadAction);
		}
		_items.ForEach(i=>i.GetComponent<BusinessItemPanel>().OnDeselect());//?
		string []filters = {"простые", "крутые", "мои"};
		SetFilters(filters);
	}

	bool ShowKnife(KnifeData knifeData, ProgressData progressData)
	{
		if(selectedFilter==0)
			return knifeData.price>0 && !progressData.knives.Contains(knifeData.name);
		if(selectedFilter==1)
			return knifeData.gold>0 && !progressData.knives.Contains(knifeData.name);
		return progressData.knives.Contains(knifeData.name);
	}
	
	public void AddIAPs( IIAPStore iapStore , ProgressData progressData )
	{
		title.text = "ВАЛЮТА";
		reloadAction = ()=>AddIAPs(iapStore, progressData);
		RemoveOldItems( );
		item.OnSelect();

		List<CurrencyIAPData> iaps = iapStore.GetCurrencyIAPs( );
		for( int i = 0 ; i < iaps.Count ; i++ )
		{
			if(!ShowIAP(iaps[i], progressData))
				continue;
			BusinessItemPanel newItem = AddBusinessItem(iaps[i]);
			newItem.SetCurrencyIAP( iaps[ i ], progressData , iapStore );
		}
		_items.ForEach(i=>i.GetComponent<BusinessItemPanel>().OnDeselect());//?
		string []filters = {"золото", "доллары"};
		SetFilters(filters);
	}

	void SetFilters(string []filters)
	{
		int k = 0;
		filterButtons.ForEach(b=>{
			if(k>=filters.Length)
			{
				b.gameObject.SetActive(false);
				return;
			}
			b.gameObject.SetActive(true);
			b.onClick.RemoveAllListeners();
			b.GetComponentInChildren<Text>().text = filters[k];
			b.GetComponentInChildren<Text>().color = k==selectedFilter?Color.yellow:Color.black;
			b.GetComponent<Image>().color = k==selectedFilter?Color.white:Color.grey;
			int k1 = k;
			b.onClick.AddListener(()=>{
				selectedFilter = k1;
				reloadAction();
			});
			k++;
		});
	}

	bool ShowIAP(CurrencyIAPData iapData, ProgressData progressData)
	{
		if(selectedFilter==0)
			return iapData.gold>0;
		return iapData.revenu>0;
	}

	void OnClickItem(BusinessItemPanel newItem, object data)
	{
		if(selection)
		{
			selection.OnDeselect();			
			if(onDeselect!=null)
				onDeselect(data);
		}
		if(newItem && selection!=newItem)
		{
			newItem.OnSelect();
			if(onSelect!=null)
				onSelect(data);
		}
		selection = selection==newItem?null:newItem;
	}

	BusinessItemPanel AddBusinessItem(object data)
	{			
		BusinessItemPanel newItem = AddNewItem( item );
		if(newItem.pointerButton)
			newItem.pointerButton.onPressedUp =()=>{ OnClickItem(newItem, data); };
		else
		{
			newItem.selectButton.onClick.RemoveAllListeners();
			newItem.selectButton.onClick.AddListener(()=>{ OnClickItem(newItem, data); });
		}
		return newItem;
	}
}