﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DependenciesBuilder : MonoBehaviour 
{

	// Use this for initialization
	void Awake () 
	{
		MenuConfig menuConfig = GetComponent<MenuConfig>();

		ProfileControll profileControll = FindObjectOfType<ProfileControll>();
		profileControll.Init();

		ProfileData profileData = new ProfileData ();
		profileData.Init (menuConfig);

		WebLogin wlogin = FindObjectOfType<WebLogin>();
		wlogin.Init (profileControll);

		FacebookLogin flogin = FindObjectOfType<FacebookLogin>();
		flogin.Init (menuConfig, profileControll);

		VKLogin vklogin = FindObjectOfType<VKLogin>();
		vklogin.Init (menuConfig, profileControll);

		MenuState menuState = new MenuState ();

		PhotonLobby lobby = PhotonLobby.GetOrCreate(menuConfig.locations.ConvertAll(l=>l.name));
		lobby.Init (menuConfig, menuState, profileControll);

		DailyBonus bonus = FindObjectOfType<DailyBonus>();
		bonus.Init(menuConfig.progressData);

		#if UNITY_WEBPLAYER
		WebStore store = FindObjectOfType<WebStore>();
		if(!store)
		{
			store = new GameObject("WebStore").AddComponent<WebStore>();
			StartCoroutine(store.Init(iaps));
		}
		#else
		OpenIABStore store = FindObjectOfType<OpenIABStore>();
		if(!store)
		{
			store = new GameObject("OpenIAB").AddComponent<OpenIABStore>();
			store.gameObject.AddComponent<OpenIABEventManager>();
			StartCoroutine(store.Init(menuConfig.iaps));
		}
		#endif
		MenuCamera menuCamera = FindObjectOfType<MenuCamera>();
		menuCamera.Init();

		ADCAdManager adcolony = FindObjectOfType<ADCAdManager>();

		WheelOfFortuneManager wheelManager = FindObjectOfType<WheelOfFortuneManager>();
		wheelManager.Init (menuConfig.progressData, menuConfig.wheel);

		menuState.Init (menuConfig, profileControll, lobby, flogin, menuCamera, store, profileData);

		MainMenuController mainMenuController = new MainMenuController ();
		mainMenuController.Init (menuState, menuConfig, menuCamera, adcolony, bonus, wheelManager, profileData);
	}
}
