﻿using UnityEngine;
using System.Collections;

public class FixedFOV : MonoBehaviour 
{
	public float hFOV = 10.0f;
	Camera _camera;
	// Use this for initialization
	void Start () {
		_camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		float hFOVrad = hFOV*Mathf.Deg2Rad;
		float camH = Mathf.Tan(hFOVrad*.5f)/_camera.aspect;
		float vFOVrad = Mathf.Atan(camH)*2;
		_camera.fieldOfView = vFOVrad * Mathf.Rad2Deg;
		//_camera.fieldOfView = (hFOV*((float)(_camera.pixelWidth) / _camera.pixelHeight));//ugly int to float
	}
}
