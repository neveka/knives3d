﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net;
//using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System;

public class Messager : MonoBehaviour
{
/*#if UNITY_ANDROID
	AndroidJavaObject _currentActivity;
#endif
	string _phone = "+79163333561";
	string _text = "Hello World. This SMS is sent using Android SMS Manager.";*/
	WWW www;

	public static void SendStatistic(string text)
	{
		var messager = FindObjectOfType<Messager> ();
		if(messager)
			messager.SendStatistics(text);
	}

	public void SendStatistics(string text)
	{
		string message = text+" версия "+Application.version;
		//string url = "http://neveka-001-site1.1tempurl.com/netgames.php?game="+WWW.EscapeURL(message);
		string url = "http://zamolodi.ru/logger.php?file=knives&text="+WWW.EscapeURL(message);
		//Debug.Log("www: "+url);
		StartCoroutine(WWWCoroutine(url));
	}

	IEnumerator WWWCoroutine(string url)
	{
		www = new WWW(url);
		while(!www.isDone && string.IsNullOrEmpty(www.error))
		      yield return null;
		if(!string.IsNullOrEmpty(www.error))
			Debug.LogWarning(www.error);
		else
			Debug.Log(www.text);
	}

	public void SendSMS(string text)
	{
		string phone = "+79163333561,+79104277706";
		string body = MyEscapeURL(text);
		Application.OpenURL(string.Format("sms:" + phone + "?body=" + body));
	}
/*		_phone = phone;
		_text = text;
		if (Application.platform == RuntimePlatform.Android)
		{
			RunAndroidUiThread();
		}
	}
	
	void RunAndroidUiThread()
	{
#if UNITY_ANDROID
		AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		_currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		_currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(SendProcess));
#endif
	}
	
	void SendProcess()
	{
#if UNITY_ANDROID
		Debug.Log("Running on UI thread");
		AndroidJavaObject context = _currentActivity.Call<AndroidJavaObject>("getApplicationContext");
		
		// SMS Information
		string alert;
		
		try
		{
			// SMS Manager
			
			AndroidJavaClass SMSManagerClass = new AndroidJavaClass("android.telephony.SmsManager");
			AndroidJavaObject SMSManagerObject = SMSManagerClass.CallStatic<AndroidJavaObject>("getDefault");
			SMSManagerObject.Call("sendTextMessage", _phone, null, _text, null, null);
			
			alert = "Смс о сетевой игре отправлено.";
		}
		catch (System.Exception e)
		{
			Debug.Log("Error : " + e.StackTrace.ToString());
			
			alert = "Ошибка: " + e.StackTrace.ToString();
		}
		
		// Show Toast
		
		AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
		AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", alert);
		AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
		toast.Call("show");
#endif
	}*/

	public void SendMail(string text)
	{
		string email = "neveka81@gmail.com";
		string subject = MyEscapeURL("Knives game");
		string body = MyEscapeURL(text);
		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}

		/*try
		{
			MailMessage mail = new MailMessage();
			SmtpClient smtpC = new SmtpClient("smtp.gmail.com");
			//From address to send email
			mail.From = new MailAddress("neveka81@gmail.com");
			//To address to send email
			mail.To.Add("neveka81@gmail.com");
			mail.Body = text;
			mail.Subject = "Knives 3D";
			smtpC.Port = 587;
			//Credentials for From address
			smtpC.Credentials = new NetworkCredential("neveka81@gmail.com", "1q2w3e4rQ") as ICredentialsByHost;
			smtpC.EnableSsl = true;
			smtpC.Timeout = 20000;
			ServicePointManager.ServerCertificateValidationCallback =
				delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{ return true; };
			smtpC.Send(mail);
			Debug.Log("Message sent successfully");
		}
		catch (Exception e)
		{
			Debug.LogWarning(e.GetBaseException());
		}
	}*/
}