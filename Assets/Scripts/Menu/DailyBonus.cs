﻿using UnityEngine;
using System.Collections;
using System;

public class DailyBonus : MonoBehaviour 
{
	const int bonusHours = 4;
	ProgressData _progressData;
	public void Init (ProgressData progressData) 
	{
		_progressData = progressData;
		if(progressData.lastNotificationTime.Ticks>DateTime.UtcNow.Ticks)
		{
			progressData.lastNotificationTime = DateTime.UtcNow;
		}

		SendNotification(GetLeftTime().TotalSeconds);
	}

	TimeSpan GetLeftTime()
	{
		return _progressData.lastNotificationTime - DateTime.UtcNow + new TimeSpan(bonusHours,0,0);
	}

	void Update()
	{
		if(GetLeftTime().TotalHours<0)
		{
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Бонус", 
			                                                  "Каждые "+bonusHours+" часа получай <color=#33aa00ff>$</color>100!", 
			                                                  ()=>{_progressData.money+=100;});
			_progressData.lastNotificationTime = DateTime.UtcNow;
			SendNotification(GetLeftTime().TotalSeconds);
		}
	}

	public string GetText()
	{
		TimeSpan timeSpan = GetLeftTime();
		return /*"Бонус через "+*/string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
	}

	public void SendNotification(double secs)
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		//send notification here
		AndroidJavaObject ajc = new AndroidJavaObject("com.zeljkosassets.notifications.Notifier");
		ajc.CallStatic("sendNotification", "Ножички 3D", "Ножички 3D", "Забери бонус", (int)secs);
		#else
		Debug.LogWarning("This asset is for Android only. It will not work inside the Unity editor!");
		#endif
	}
}
