﻿using UnityEngine;
using System.Collections;
using System;

public class Strings
{
	public static string[] pool = {
		"Играть", "play",
		"Магазин", "store",
		"правила игры", "rules",
		"Персонаж","character",
		"гость", "guest",
		"назад", "back",
		"Войти за <color=#33aa00ff>$</color>1000", "login for <color=#33aa00ff>$</color>1000",
		"Войти", "login",
		"тренировка","training",
		"битва по сети","play online",
		"купить","buy",
		"выбрать", "select",
		"выбран", "selected",
		"мало $", "no $"};

	public static string Get(string origin)
	{
		//if(language.ToLower().Contains("ru"))
		//	return origin;
		int idx = Array.IndexOf(pool, origin.ToLower());		
		if(idx == -1)
			return origin;
		if(idx%2==0 && Application.systemLanguage.ToString().ToLower().Contains("en"))
			idx++;
		else if(idx%2==1 && Application.systemLanguage.ToString().ToLower().Contains("ru"))
			idx--;
		if(idx>=0 && idx<pool.Length)
			return pool[idx];
		else
			return origin;
	}

}
