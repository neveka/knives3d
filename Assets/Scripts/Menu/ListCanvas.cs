using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ListCanvas: MonoBehaviour
{
    public Text title;
    protected List<MonoBehaviour> _items = new List<MonoBehaviour>( );
    protected Action reloadAction;

    protected virtual void Init( string text , Action onReloadAction , Action onCloseAction )
    {
        if( !string.IsNullOrEmpty( text ) )
        {
            title.text = text.ToUpper( );
        }
        reloadAction = onReloadAction;
        reloadAction( );
    }

    protected virtual void RemoveOldItems( )
    {
        while( _items.Count > 1 )
        {
            DestroyImmediate( _items[ _items.Count - 1 ].gameObject );
            _items.RemoveAt( _items.Count - 1 );
        }
		if(_items.Count>0)
			_items[0].gameObject.SetActive(false);
		_items.Clear( );
    }

    protected T AddNewItem<T>( T item ) where T : MonoBehaviour
    {
        T newItem = item;
		item.gameObject.SetActive(true);
        if( _items.Count > 0 )
        {
            newItem = Instantiate( item );
            newItem.transform.SetParent( item.transform.parent );
            newItem.transform.localScale = Vector3.one;
			newItem.transform.localRotation = Quaternion.identity;
			Vector3 pos = newItem.transform.localPosition;
			pos.z = 0;
			newItem.transform.localPosition = pos;
        }
		newItem.gameObject.SetActive(true);
        _items.Add( newItem );
        return newItem;
    }
}
