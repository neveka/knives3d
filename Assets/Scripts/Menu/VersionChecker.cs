﻿using UnityEngine;
using System.Collections;

public class VersionChecker : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		StartCoroutine( CheckInternetCoroutine() );
	}
	
	public IEnumerator CheckInternetCoroutine()
	{
		WWW www = new WWW( "http://nev-eka.narod.ru/knives.txt" );
		while( !www.isDone )
			yield return null;
		if( www.error != null )
		{

		}
		else if(float.Parse(Application.version) < float.Parse(www.text))
		{
			Debug.LogWarning(Application.version+" "+www.text);
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Внимание!", "Обновите приложение до последней "+www.text+" версии", 
			                                                 ()=>{ Application.OpenURL ("https://play.google.com/store/apps/details?id=com.neveka.knives");});
		}
	}
}
