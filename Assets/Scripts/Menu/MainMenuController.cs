﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController
{
	private MainMenu _visual;
	public void Init( 
		MenuState menuState, 
		MenuConfig menuConfig, 
		MenuCamera menuCamera, 
		ADCAdManager adcolony, 
		DailyBonus dailybonus, 
		WheelOfFortuneManager wheel,
		ProfileData profileData)
	{
		_visual = GameObject.FindObjectOfType<MainMenu>();

		_visual.money.SetGetStringFunc(()=>{return menuConfig.progressData.money.ToString();});
		_visual.gold.SetGetStringFunc(()=>{return menuConfig.progressData.gold.ToString();});
		_visual.bonus.SetGetStringFunc(dailybonus.GetText);
		_visual.xpControll.SetExperience(profileData.newExp, profileData.nextExp, profileData.level);

		_visual.gotoLocations = menuCamera.GotoLocations;
		_visual.gotoStores = menuCamera.GotoStores;
		_visual.gotoDollars = menuCamera.GotoDollars;
		_visual.gotoGold = menuCamera.GotoGold;
		_visual.onCheat = () => menuConfig.cheatCounter++;
		_visual.onCharacter = menuState.OnCharacter;
		_visual.onChampionship = menuState.OnChampionShip;
		_visual.seeVideo = ()=>{ 
			adcolony.addMoney = (int amount)=>{	menuConfig.progressData.money+=amount;};
			adcolony.ShowVideoAdByZoneKey(0);
		};
		_visual.showTVButton = () => {
			return AdColony.IsV4VCAvailable (adcolony.videoZones [0].zoneId);
		};
		_visual.onWheel =	wheel.Show;

		wheel.onNoGold = () => {
			menuCamera.GotoGold ();
			_visual.Hide();
		};

		menuCamera.Escape = _visual.OnEscape;
	}
}

