﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SoundButton : Button 
{
	static AudioSource source;
	protected override void Awake () 
	{
		base.Awake();
		onClick.AddListener(()=>{
			if(!source)
			{
				source = new GameObject("ButtonSound").AddComponent<AudioSource>();
				source.playOnAwake = false;
				source.clip = (AudioClip)Resources.Load("button");
			}
			source.Play();
		});	
	}

	void OnApplicationQuit()
	{
		if(source)
		{
			DestroyImmediate(source.gameObject);
			source = null;
		}
	}
}
