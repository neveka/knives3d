using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class LocationCanvas : MonoBehaviour {

	public Button startButton;
	public Button netStartButton;
	public CurrencyControll netStatus;
	public CurrencyControll netDashStatus;
	public Text locationName;
	public Image locationImage;
	public Text playersText;
	public Text prizeText;

	void Update()
	{
		RectTransform rt = GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2(rt.sizeDelta.x, rt.sizeDelta.x / Camera.main.aspect);
	}

	public void Init(LocationData data, int locationIndex, Action startLandsAction, Action startDashAction, Func<int, CombatTransfer.Mode, bool, int> getNetPlayers)
	{
		locationName.text = data.name;
		locationImage.sprite = Resources.Load<Sprite>(data.photoName);
		playersText.text = "почва: "+data.soilType;
		prizeText.text = "ставка: $"+data.stake;
		startButton.onClick.RemoveAllListeners();
		startButton.onClick.AddListener(()=>{startDashAction();});
		startButton.transform.Find("Text").GetComponent<Text>().text = Strings.Get("В яблочко");
		netStartButton.onClick.RemoveAllListeners();
		netStartButton.onClick.AddListener(()=>{startLandsAction();});
		netStartButton.transform.Find("Text").GetComponent<Text>().text = Strings.Get("Земельки");
		netStatus.SetGetStringFunc(()=>"Хотят играть: "+getNetPlayers(locationIndex, CombatTransfer.Mode.net, false)+
		                           "\nУже играют: "+getNetPlayers(locationIndex, CombatTransfer.Mode.net, true));
		netDashStatus.SetGetStringFunc(()=>"Хотят играть: "+getNetPlayers(locationIndex, CombatTransfer.Mode.netDash, false)+
		                               "\nУже играют: "+getNetPlayers(locationIndex, CombatTransfer.Mode.netDash, true));
	}
}
