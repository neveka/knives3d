using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;
using SimpleJson;

public class VKLogin : LoginControll 
{
	public void Init(MenuConfig menuConfig, ProfileControll profileControll)
	{
		#if UNITY_WEBPLAYER
		enabled = false;
		#else
		wasLogin = ()=>menuConfig.progressData.wasVKLogin;
		onLogin = ()=>{
			if(!menuConfig.progressData.wasVKLogin)
			{
				menuConfig.progressData.money+=1000; 
				OnLogin();
			} 
			menuConfig.progressData.wasVKLogin = true; 
		};
		SetName = profileControll.SetName;
		SetPicture = profileControll.SetPicture;
		setText = (string text)=>{Debug.Log(text);};
		#endif
	}

	//ID приложения:	5633893
	//238F66787B6A7633A7196799BC0D2F61860EFCC1
	void Start ()
	{
		InitGUIElements(false);
	}
			// Update is called once per frame
	protected override void Login()
	{ 
		FindObjectOfType<FacebookLogin>().LogoutOnOtherLogin();
		VK.Instance.Authorize("5633893", "12290", AuthCallback);
	}	

	protected override void Logout()
	{
		VK.Instance.Logout("5633893",(string result)=>{
			InitGUIElements(false);
		});
	}

	protected override bool IsLoggedIn()
	{
		return VK.Instance.AccessToken != null;
	}

	private void AuthCallback (string result) 
	{
		InitGUIElements(false);
	}

	protected override void RequestPicAndName()
	{
		StartCoroutine(VK.Instance.CallApiCoroutine("users.get", new Dictionary<string, string>(){{"user_ids", ""},{"fields","photo_50"},{"version", "5.0"}}, OnGetUser));
	}
	
	void OnGetUser(string result)
	{
		JsonObject obj = (JsonObject)SimpleJson.SimpleJson.DeserializeObject(result);
		SetName((string)((JsonObject)((JsonArray)obj["response"])[0])["first_name"]);
		StartCoroutine(LoadPicture((string)((JsonObject)((JsonArray)obj["response"])[0])["photo_50"]));
	}

	IEnumerator LoadPicture(string url) 
	{
		// Start a download of the given URL
		WWW www = new WWW(url);
		
		// Wait for download to complete
		yield return www;
		
		// assign texture
		SetPicture(www.texture!=null?Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2()):null);
	}

	public void OnCombatWin()
	{
		if(VK.Instance.AccessToken != null)
		{
			StartCoroutine(VK.Instance.CallApiCoroutine("wall.post", new Dictionary<string, string>(){{"message", "Я выиграл бой в ножички 3D. А тебе слабо? Качай с Play Market, всегда рады новичкам."},{"attachments", "https://play.google.com/store/apps/details?id=com.neveka.knives"},{"version", "5.0"}}, (string result)=>{}));
		}
	}

	public void OnLogin()
	{
		StartCoroutine(OnLoginSendingCoroutine());
	}

	IEnumerator OnLoginSendingCoroutine()
	{
		ClearUIDsAndSending();
		CollectFriends();
		while(_counter>0)
			yield return null;
		_sending = true;
		StartCoroutine(SendToFriendsCoroutine("Я только что залогинился в игре Нoжички 3D, айда со мной! Группа игры: https://vk.com/clubknivesgame"));
	}
	//========================================================================================

	public Action<string> setText;
	public Func<string> getCaptureInput;
	public Action<Texture> setCaptureImage;
	public Func<bool> captureInputFinished;
	private int _requests = 1;
	private List<string> _uids = new List<string>(){""};
	private List<string> _uidsMyFriends = new List<string>(){""};
	private List<string> _uidsRequested = new List<string>();
	private List<string> _uidsSent = new List<string>();
	private string _message;
	private int _counter;
	private bool _sending;
	private bool _post;

	public void ClearUIDsAndSending()
	{
		_counter=0;
		_sending = false;
		_uids.Clear();
		_uids.Add("");
		_uidsRequested.Clear();
		_uidsSent.Clear();
		_uidsMyFriends.Clear();
	}

	public void SendToFriends(string text)
	{
		if(string.IsNullOrEmpty(text))
			setText("введи сообщение");
		else if(VK.Instance.AccessToken != null && _counter==0 && !_sending)
		{
			_sending = true;
			_uids.RemoveAll(u=>_uidsMyFriends.Contains(u));
			StartCoroutine(SendToFriendsCoroutine(text));
		}
		else
		{
			setText(VK.Instance.AccessToken==null?"Залогинься":"Подожди");
		}
	}

	IEnumerator SendToFriendsCoroutine(string text)
	{
		while(_sending)
		{
			yield return new WaitForSeconds(1);
			int counter = 0;
			while(counter<_requests && _uids.Count>0)
			{
				int idx = UnityEngine.Random.Range(0, _uids.Count);
				bool ok = false;
				string captureSid = "";
				string captureImg = "";
				while(!ok)
				{
					bool wait = true;
					if(captureImg!="")
					{
						WWW www = new WWW(captureImg);
						setText("качаем капчу "+captureImg);
						yield return www;
						setCaptureImage(www.texture);
						while(!captureInputFinished())
						{
							setText("введи текст с капчи "+_uidsSent.Count+" осталось "+_uids.Count);
							yield return null;
						}
						StartCoroutine(VK.Instance.CallApiCoroutine(_post?"wall.post":"messages.send", new Dictionary<string, string>(){
							{_post?"owner_id":"user_id", _uids[idx]},
							{"message", text},
							{"attachments", "https://play.google.com/store/apps/details?id=com.neveka.knives"},
							{"version", "5.0"},
							{"captcha_sid", captureSid},
							{"captcha_key", getCaptureInput()}}, 
						(string res)=>OnSendMessage(res, ()=>{wait = false; ok = true;}, 
						(string newCaptureSid, string newCaptureImg)=>{captureSid = newCaptureSid; captureImg = newCaptureImg; ok = false; wait = false;})));
						captureSid = "";
						captureImg = "";
					}
					else
					{
						StartCoroutine(VK.Instance.CallApiCoroutine(_post?"wall.post":"messages.send", new Dictionary<string, string>(){
							{_post?"owner_id":"user_id", _uids[idx]},
							{"message", text},
							{"attachments", "https://play.google.com/store/apps/details?id=com.neveka.knives"},
							{"version", "5.0"}}, 
						(string res)=>OnSendMessage(res, ()=>{wait = false; ok = true;}, 
						(string newCaptureSid, string newCaptureImg)=>{captureSid = newCaptureSid; captureImg = newCaptureImg; ok = false; wait = false;})));
					}
					while(wait)
					{
						//setText("отправляем сообщение");
						yield return null;
					}
				}
				_uidsSent.Add(_uids[idx]);
				_uids.RemoveAt(idx);

				counter++;
				//_sending = false;
			}
			if(_uids.Count == 0)
				_sending = false;
			//setText("разослали "+_uidsSent.Count+" осталось "+_uids.Count);
		}
	}

	void OnSendMessage(string result, Action onFinish, Action<string, string> onNeedCapture)
	{
		setText("разослали "+_uidsSent.Count+" осталось "+_uids.Count+" последний"+result);
		try
		{
			JsonObject obj = (JsonObject)SimpleJson.SimpleJson.DeserializeObject(result);
			//setText(obj.ToString()+" "+obj.ContainsKey("error"));
			//setText(obj["error"].ToString()+" "+obj["error"].GetType()+((JsonObject)obj["error"]).ContainsKey("captcha_sid")+((JsonObject)obj["error"]).ContainsKey("captcha_img"));
			if(obj.ContainsKey("response"))
			{
				setText("ок "+_uidsSent.Count+" осталось "+_uids.Count+" последний"+result);
				onFinish();
			}
			else if(obj.ContainsKey("error"))
			{
				//setText(((JsonObject)obj["error"])["captcha_sid"].ToString()+" "+((JsonObject)obj["error"])["captcha_img"].ToString());
				JsonObject errorObj = (JsonObject)obj["error"];
				if(errorObj.ContainsKey("captcha_sid"))
				{
					setText("onNeedCapture "+(string)errorObj["captcha_sid"]+","+ (string)errorObj["captcha_img"]+_uidsSent.Count+" осталось "+_uids.Count+" последний"+result);
					onNeedCapture((string)errorObj["captcha_sid"], (string)errorObj["captcha_img"]);
				}
				else
				{
					onFinish();
				}
			}
		}
		catch(Exception ex)
		{
			setText("Ошибка: "+ex.ToString());
		}
	}

	public void CollectFriends()
	{
		if(VK.Instance.AccessToken != null && _counter==0 && !_sending)
		{
			_counter = 0;
			while(_counter<_requests && _uids.Count>0)
			{
				int idx = UnityEngine.Random.Range(0, _uids.Count);
				StartCoroutine(VK.Instance.CallApiCoroutine("friends.get", new Dictionary<string, string>(){{"user_id", _uids[idx]},{"count", "100"},{"fields",_post?"can_post":"can_write_private_message"},{"version", "5.0"}}, OnGetFriends));

				_uidsRequested.Add(_uids[idx]);
				_uids.RemoveAt(idx);
				_counter++;
			}
		}
		else
		{
			setText(VK.Instance.AccessToken==null?"Залогинься":"Подожди");
		}
	}

	void OnGetFriends(string result)
	{
		//setText("_counter="+_counter+" "+result);
		JsonObject obj = (JsonObject)SimpleJson.SimpleJson.DeserializeObject(result);
		if(obj.ContainsKey("response"))
		{
			bool myFriends = _uidsRequested.Count == 1;
			JsonArray list = (JsonArray)obj["response"];
			for(int i = 0; i<list.Count; i++)
			{
				JsonObject user = (JsonObject)list[i];
				string uid = user["uid"].ToString();
				bool can = ((long)user[_post?"can_post":"can_write_private_message"])==1;
				if(can && !_uids.Contains(uid) && !_uidsRequested.Contains(uid))
				{
					_uids.Add(uid);
					if(myFriends)
						_uidsMyFriends.Add(uid);
				}
			}
		}
		_counter--;
		if(_counter == 0)
			setText("набрали айдишники ("+_uidsRequested.Count+"->"+_uids.Count+") из них своих френдов "+_uidsMyFriends.Count);
	}
}