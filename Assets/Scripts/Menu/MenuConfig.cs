using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuConfig : MonoBehaviour 
{
	public ProgressData progressData;
	public List<LocationData> locations = new List<LocationData>();
	public List<LocationData> championShip = new List<LocationData>();
	public List<KnifeData> knives = new List<KnifeData>();
	public List<PlayerData> players = new List<PlayerData>();
	public List<CurrencyIAPData> iaps = new List<CurrencyIAPData>();
	public List<int> levels = new List<int>();
	public WheelData wheel = new WheelData ();

	public int cheatCounter = 0;

	// Use this for initialization
	void Awake () 
	{
		progressData.Fix (knives);
		progressData.tutorials = 0;
	}

	//#if DEBUG
	void OnGUI()
	{
		if(cheatCounter<7)
			return;
		GUIStyle style = new GUIStyle("button");
		style.fontSize = Screen.height/30;
		GUILayout.Space(Screen.height/4f);
		GUILayoutOption[] options = new GUILayoutOption[] {GUILayout.Height(Screen.height/12f), GUILayout.Width(Screen.height/5f)};
		if(GUILayout.Button("+ $100", style, options))
			progressData.money += 100;
		if(GUILayout.Button("+ @1", style, options))
			progressData.gold += 1;
		if(GUILayout.Button("+ 50 опыта", style, options))
			progressData.experience += 50;
		if(GUILayout.Button("очистить", style, options))
			PlayerPrefs.DeleteAll();//?
		//if (GUILayout.Button("notify", style, options))
		//	FindObjectOfType<DailyBonus>().SendNotification(5);
		if(GUILayout.Button("рассылка", style, options))
		{
			cheatCounter = 0;
			GameScreen.CreateScreen<VKCenterScreen>();
		}
		if(GUILayout.Button("Х", style, options))
			cheatCounter=0;
		//if(GUILayout.Button("SMS", style, options))
		//	FindObjectOfType<SendSMS>().Send("+79163333561", "test sms");
	}
	//#endif
}
