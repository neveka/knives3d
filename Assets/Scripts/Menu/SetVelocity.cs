﻿using UnityEngine;
using System.Collections;

public class SetVelocity : MonoBehaviour 
{
	public Vector2 velocity;
	void Awake () {
		GetComponent<Rigidbody2D>().velocity = velocity;
	}
}
