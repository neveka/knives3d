﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class CurrencyControll : MonoBehaviour 
{
	Text _text;
	Image _image;
	Image _back;
	Func<float> _getDollars;
	Func<float> _getGold;
	Func<string> _getString;
	void Awake () 
	{
		_text = GetComponentInChildren<Text>();
		if(_text)
		{
			_text.supportRichText = true;//?
			//foreach(Material m in _text.GetComponent<Renderer>().materials)
			//	Debug.LogWarning(m.name);
		}
		if(transform.Find("Icon"))
			_image = transform.Find("Icon").GetComponent<Image>();
		if(transform.Find("Back"))
			_back = transform.Find("Back").GetComponent<Image>();
	}

	public void Hide()
	{
		if(_text)
			_text.gameObject.SetActive(false);
		if(_image)
			_image.gameObject.SetActive(false);
		if(_back)
			_back.gameObject.SetActive(false);
	}

	public void SetDollars (float price) 
	{
		_text.text = "<color=#33aa00ff>$</color>"+price;
		if(_image)
			_image.gameObject.SetActive(false);
	}

	public void SetGold (float price) 
	{
		_text.text = /*"<quad material=1 size=20 x=0.1 y=0.1 width=0.5 height=0.5 />"*/
			"<color=yellow>@</color>"+price;
		if(_image)
			_image.gameObject.SetActive(false);
	}

	public void SetGetDollarsFunc(Func<float> getDollars)
	{
		_getDollars = getDollars;
	}

	public void SetGetGoldFunc(Func<float> getGold)
	{
		_getGold = getGold;
	}

	public void SetGetStringFunc(Func<string> getString)
	{
		_getString = getString;
	}

	public void SetText(string str)
	{
		_text.text = str;
	}

	public void SetTextAndFill(string str, float part)
	{
		SetText(str);
		_image.fillAmount = part;
	}

	public void Update()
	{
		if(_getDollars!=null)
		{
			SetDollars(_getDollars());
		}
		if(_getGold!=null)
		{
			SetGold(_getGold());
		}
		if(_getString!=null)
		{
			SetText(_getString());
		}
	}
}
