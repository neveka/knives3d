﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class VKCenterScreen : GameScreen 
{
	public InputField textField;
	public Text statusText;
	public Button sendButton;
	public Button stopSendButton;
	public Button collectButton;

	public InputField captureField;
	public RawImage captureImage;
	public Button captureButton;
	

	// Use this for initialization
	void Start () {
		VKLogin vklogin = FindObjectOfType<VKLogin>();
		vklogin.setText = (string text)=>statusText.text = text;
		collectButton.onClick.RemoveAllListeners();
		collectButton.onClick.AddListener(()=>vklogin.CollectFriends());
		sendButton.onClick.RemoveAllListeners();
		sendButton.onClick.AddListener(()=>vklogin.SendToFriends(textField.text));
		vklogin.ClearUIDsAndSending();
		vklogin.getCaptureInput = ()=> captureField.text;
		vklogin.captureInputFinished = ()=>captureImage.texture == null;
		vklogin.setCaptureImage = (Texture tex)=>{
			captureImage.gameObject.SetActive(true);
			captureButton.gameObject.SetActive(true);
			captureField.gameObject.SetActive(true);
			captureImage.texture = tex;
		};
		captureButton.onClick.RemoveAllListeners();
		captureButton.onClick.AddListener(()=>{
			captureImage.texture = null;
			captureField.text = "";
			captureImage.gameObject.SetActive(false);
			captureButton.gameObject.SetActive(false);
			captureField.gameObject.SetActive(false);}
		);
		captureImage.gameObject.SetActive(false);
		captureButton.gameObject.SetActive(false);
		captureField.gameObject.SetActive(false);
		_onClose = vklogin.ClearUIDsAndSending;
	}
}
