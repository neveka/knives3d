﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuCamera : MonoBehaviour 
{
	public float rotSpeed = 60;//in degrees
	public List<Vector3> quadPositions = new List<Vector3>();
	public List<Vector3> quadRotations = new List<Vector3>();
	public List<GameObject> quads = new List<GameObject> ();
	public List<RenderTexture> targetTextures = new List<RenderTexture> ();

	public Action Escape;
	public Action<LocationCanvas, int> StartRotateToLocation;
	public Action<BusinessListCanvas, int> StartRotateToBusiness;

	Action FinishRotate;

	bool _rotating;  
	int _currentCanvas = 0;
	bool _locations;

	List<LocationCanvas> _locCanvasses = new List<LocationCanvas>();
	List<BusinessListCanvas> _listCanvasses = new List<BusinessListCanvas>(); 

	public void Init()
	{
		for (int i = 0; i < 3; i++) 
		{
			GameObject quad = Instantiate (quads [0]);
			quad.transform.parent = quads [0].transform.parent;
			quad.name = "Quad" + (i + 1);
			quads.Add (quad);
		}
		for (int i = 0; i < quads.Count; i++) 
		{
			quads[i].transform.position = quadPositions[i];
			quads[i].transform.rotation = Quaternion.Euler (quadRotations[i]);
			quads[i].GetComponentInChildren<Camera>().targetTexture = targetTextures[i];
			quads[i].GetComponentInChildren<RawImage>().texture = targetTextures[i];
		}
		_locCanvasses = new List<LocationCanvas>(FindObjectsOfType<LocationCanvas>());
		_locCanvasses.Sort((x,y)=>string.Compare(x.transform.parent.name, y.transform.parent.name));
		_listCanvasses = new List<BusinessListCanvas>(FindObjectsOfType<BusinessListCanvas>());
		_listCanvasses.Sort((x,y)=>string.Compare(x.transform.parent.name, y.transform.parent.name));
	}

	// Update is called once per frame
	Vector3 _dragStart;
	Quaternion _dragFromRot; 
	float _dragAngle;
	void Update () 
	{
		if(Input.GetMouseButtonDown(0) && !_rotating && (!EventSystem.current.currentSelectedGameObject||
		                                                 !EventSystem.current.currentSelectedGameObject.GetComponentInParent<BusinessItemPanel>()) /*&& !EventSystem.current.IsPointerOverGameObject()*/)
		{
			_dragStart = Input.mousePosition;
			_dragFromRot = transform.rotation;
		}
		if(Input.GetMouseButton(0) && !_rotating/* && !EventSystem.current.IsPointerOverGameObject()*/ && 
		   _dragStart!=Vector3.zero)
		{
			float delta = Input.mousePosition.x-_dragStart.x;
			if(Mathf.Abs(delta)>Screen.width/4)
			{
				StartCoroutine(RotateCoroutine(delta<0, false));
				_dragStart = Vector3.zero;
			}
			else
			{
				_dragAngle = delta/-10f;
				transform.rotation = _dragFromRot*Quaternion.Euler(Vector3.up*_dragAngle);
			}
		}
		if(Input.GetMouseButtonUp(0) && _dragStart!=Vector3.zero && !_rotating)
		{
			StartCoroutine(RotateCoroutine(_dragAngle>0, true));
			//ResetDrag();
		}
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			ResetDrag();
			Escape();
		}
	}

	void ResetDrag()
	{
		if(_dragAngle != 0)
		{
			_dragAngle = 0;			
			transform.rotation = _dragFromRot;
			_dragStart = Vector3.zero;
		}
	}

	public void RotateRight()
	{
		if(!_rotating)
			StartCoroutine(RotateCoroutine(true, false));
	}

	public void RotateLeft()
	{
		if(!_rotating)
			StartCoroutine(RotateCoroutine(false, false));
	}

	public void GotoStores()
	{
		ResetDrag();
		Vector3 toPos = transform.position;
		toPos.y = -1.1f;
		transform.position = toPos;
		StartRotateToBusiness(_listCanvasses[_currentCanvas], 0);
		_locations = false;
	}

	void GotoMoney()
	{
		GotoStores();
		if(_listCanvasses[_currentCanvas].title.text!="ВАЛЮТА")
		{
			float oldSpeed = rotSpeed;
			rotSpeed = 100500;
			FinishRotate = ()=>{rotSpeed = oldSpeed;FinishRotate=null;};
			RotateRight();
		}
	}

	public void GotoDollars()
	{
		GotoMoney();
		_listCanvasses[_currentCanvas].selectedFilter = 1;
		StartRotateToBusiness(_listCanvasses[_currentCanvas], 0);
	}

	public void GotoGold()
	{
		GotoMoney();
		_listCanvasses[_currentCanvas].selectedFilter = 0;
		StartRotateToBusiness(_listCanvasses[_currentCanvas], 0);
	}

	public void GotoLocations()
	{
		Vector3 toPos = transform.position;
		toPos.y = 0.25f;
		transform.position = toPos;
		StartRotateToLocation(_locCanvasses[_currentCanvas], 0);
		_locations = true;
	}

	/*IEnumerator LiftCoroutine(bool up)
	{
		Vector3 fromPos = transform.position;
		Vector3 toPos = transform.position;
		if(toPos.y == (up?0:-1))
			yield break;
		toPos.y = up?0:-1;

		float part = 0;
		float dist = (toPos-fromPos).magnitude;
		while(part<1 && dist>0)
		{
			transform.position = Vector3.Lerp(fromPos, toPos, part);
			part += Time.deltaTime*liftSpeed/dist;
			yield return null;
		}
		transform.position = toPos;
	}*/

	/*IEnumerator LookCoroutine(bool up)
	{
		_rotating = true;
		float angle = 90*(right?1:-1);
		Quaternion fromRot = transform.rotation;
		Quaternion toRot = fromRot*Quaternion.Euler(Vector3.up*angle);
		
		float alpha = Mathf.Abs(angle);
		while(alpha>0)
		{
			float a = rotSpeed*Time.deltaTime;
			alpha -= a;
			if(alpha>0)
			{	
				transform.Rotate(Vector3.up*(angle>0?1:-1)*a);
				yield return null;
			}
		}
		transform.rotation = toRot;
		_rotating = false;
	}*/

	IEnumerator RotateCoroutine(bool right, bool stay)
	{
		if(!stay)
		{
			_currentCanvas += (right?1:-1);
			if(_currentCanvas<0)
				_currentCanvas = _locCanvasses.Count-1;
			if(_currentCanvas>_locCanvasses.Count-1)
				_currentCanvas = 0;
			if(_locations)
				StartRotateToLocation(_locCanvasses[_currentCanvas], right?1:-1);
			else
				StartRotateToBusiness(_listCanvasses[_currentCanvas], right?1:-1);
		}
		_rotating = true;
		float angle = ((stay?0:90)-Mathf.Abs(_dragAngle)) *(right?1:-1);
		//Debug.Log(angle+"---------------"+_dragAngle);
		_dragAngle = 0;
		_dragStart = Vector3.zero;
		Quaternion fromRot = transform.rotation;
		Quaternion toRot = fromRot*Quaternion.Euler(Vector3.up*angle);

		toRot.eulerAngles.Set(0,(int)toRot.eulerAngles.y,0);//?

		float alpha = Mathf.Abs(angle);
		while(alpha>0)
		{
			float a = rotSpeed*Time.deltaTime;
			alpha -= a;
			if(alpha>0)
			{	
				transform.Rotate(Vector3.up*(angle>0?1:-1)*a);
				yield return null;
			}
		}
		transform.rotation = toRot;
		//Debug.Log(toRot.eulerAngles.y);
		_rotating = false;
		if(FinishRotate!=null)
			FinishRotate();
	}
}
