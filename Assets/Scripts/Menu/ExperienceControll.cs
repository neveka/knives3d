﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ExperienceControll : MonoBehaviour 
{
	public Text expText;
	public Text expTextShadow;
	public Text levelText;
	public Image image;
	public Image back;

	public void SetExperience(int newExp, int nextExp, int level)
	{
		expText.text = newExp+"/"+nextExp;
		levelText.text = level.ToString();
		expTextShadow.text = newExp+"/"+nextExp;
		image.fillAmount = (float)newExp/nextExp;
	}
}
