﻿using UnityEngine;
using System.Collections;
using System;

public class WebLogin : MonoBehaviour 
{
	public Action<Sprite> SetPicture;
	public Action<string> SetName;

	public void Init(ProfileControll profileControll)
	{
		#if !UNITY_WEBPLAYER
		enabled = false;
		profileControll.loaded = true;
		#else
		SetName = profileControll.SetName;
		SetPicture = profileControll.SetPicture;
		SetPicture+= (Sprite sprite)=>{profileControll.loaded = true;};
		#endif
	}

	void Start()
	{
		Application.ExternalCall("OnWebLoginLoaded", "");
	}

	public void SetFirstNameAndPhoto50(string var)
	{
		if(SetName != null)
		{
			//OnEnterGame(var.Split('|')[0]);
			SetName( var.Split('|')[0]);
		}
		StartCoroutine(LoadPicture(var.Split('|')[1]));
	}
	
	IEnumerator LoadPicture(string url) 
	{
		// Start a download of the given URL
		WWW www = new WWW(url);
		
		// Wait for download to complete
		yield return www;
		
		// assign texture
		if(SetPicture != null)
			SetPicture(www.texture!=null?Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2()):null);
	}

	public void OnCombatWin()
	{
		Application.ExternalCall("SendWallPost", "Я выиграл бой в ножички 3D. А тебе слабо? Вступай в клуб https://vk.com/clubKnivesGame, всегда рады новичкам.");
	}

	public void OnTutorialComplete()
	{
		Application.ExternalCall("SendWallPost", "Я научился кидать ножи в ножички 3D. Помнишь такую игру https://vk.com/clubKnivesGame?");
	}

	/*public void OnEnterGame(string profileName)
	{
		Application.ExternalCall("SendNotification", profileName+" зашел поиграть в Ножички 3D, хочешь сразиться с ним пор сети?");
	}*/
}
