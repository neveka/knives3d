﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class BusinessItemPanel : MonoBehaviour
{
    public Text text;
	public RectTransform modelRoot;
    public Image icon;
    public Image filling;
    public CurrencyControll currencyPanel;
    public List<CurrencyControll> currencyPanels;
    public Button button;
	public Button selectButton;
	public PointerListener pointerButton;
    public Text buttonText;
    public Text revenuText;
	public Image locked;
	public bool allwaysShow;
    private Func<float> _getProgressFunc;
    private Action _progressFinishedAction;
	private float modelRotationSpeed;
	private Quaternion _oldRotation;
	private float _selectedPreferredHeight;
	private float _deselectedPreferredHeight = 200;

	public void SetPrize(PrizeData prizeData)
	{
		text.text = prizeData.amount <1?"":prizeData.amount.ToString();
		_selectedPreferredHeight = 50;
		_deselectedPreferredHeight = 50;
		TuneModel(prizeData.modelName, 
			prizeData.type == "knife"?new Vector3(750,750,600):(Vector3.one*700), 
			prizeData.type == "knife"?new Vector3(90,90,0):new Vector3(0,180,0), 
			prizeData.type == "knife"?new Vector3(0,68,0):Vector3.zero);
		/*RawImage raw = GetComponentInChildren<RawImage>();
		if(raw)
		{
			allwaysShow = true;
			raw.texture = new RenderTexture(256, 256, 0);
			raw.GetComponentInChildren<Camera>().targetTexture = (RenderTexture)raw.texture;
		}*/
	}

	public void SetStone(StoneData stoneData, int amount, ProgressData progressData, Action reloadList)
	{
		text.text = stoneData.name;
		_selectedPreferredHeight = 50;
		_deselectedPreferredHeight = 50;
		TuneModel(stoneData.modelName, Vector3.one*10, Vector3.up*90, Vector3.zero);
		currencyPanel.SetText(""+amount);
		//currencyPanels[0].SetTextAndFill("осталось", (float)stoneData.leftTurns/stoneData.totalTurns);
		RawImage raw = GetComponentInChildren<RawImage>();
		if(raw)
		{
			allwaysShow = true;
			raw.texture = new RenderTexture(256, 256, 0);
			raw.GetComponentInChildren<Camera>().targetTexture = (RenderTexture)raw.texture;
		}
	}

	public void SetKnife(KnifeData knifeData, ProgressData progressData, int level, Action reloadList)
	{
		text.text = knifeData.name;
		revenuText.text = knifeData.desc;
		_selectedPreferredHeight = 330;
		TuneModel(knifeData.modelName, Vector3.one*1000, Vector3.up*90, Vector3.zero);

		if(progressData!=null)
		{
			bool owned = progressData.knives.Contains(knifeData.name);
			bool selected = progressData.currentKnife == knifeData.name;
			bool canBuy = progressData.money>=knifeData.price && progressData.gold>=knifeData.gold;

			locked.gameObject.SetActive(!owned && knifeData.unlockLevel>level);
			TuneButton(()=>{
				if(!owned && knifeData.unlockLevel>level)
				{
					GameScreen.CreateScreen<MessageScreen>().ShowInfo("Покупка ножа", "Этот нож будет доступен после "+knifeData.unlockLevel+"-го уровня", ()=>{});
				}
				else if(!owned && canBuy)
				{
					progressData.money-=knifeData.price;
					progressData.gold-=knifeData.gold;
					progressData.AddKnife(knifeData.name);
					Messager.SendStatistic(FindObjectOfType<ProfileControll>().profileName.text+" купил нож "+knifeData.name);
					reloadList();
				}
				else if(owned && !selected)
				{
					progressData.currentKnife = knifeData.name;
					reloadList();
				}
				else if(!owned && !canBuy)
				{
					MenuCamera menuCamera = FindObjectOfType<MenuCamera>();
					if(knifeData.gold>0)
						menuCamera.GotoGold();
					else
						menuCamera.GotoDollars();
				}
			}, (!owned && canBuy)||(owned && !selected)||(!owned && !canBuy), 
			Strings.Get(//knifeData.unlockLevel>level?"Не доступен":
				       ((owned && !selected)?"Выбрать":
			           ((!owned && canBuy)?"Купить":
			            (owned?"Выбран":"Купить")))), 
			owned?0:knifeData.price, owned?0:knifeData.gold);
		}
		currencyPanels[0].SetTextAndFill("точность", knifeData.range/100f);
		currencyPanels[1].SetTextAndFill("баланс", knifeData.thrustChance/100f);
		currencyPanels[2].SetTextAndFill("контроль", knifeData.control/100f);
		currencyPanels[3].SetTextAndFill("вес", knifeData.weight/100f);
		currencyPanels[4].SetTextAndFill("длина", knifeData.lenfth/100f);
	}
	
	public void SetCurrencyIAP( CurrencyIAPData iap , ProgressData progressData, IIAPStore iapStore/* , Func<SpritesCache> getSpritesCache*/ )
	{
		text.text = iap.name;
		_selectedPreferredHeight = 200;
		locked.gameObject.SetActive(false);
		TuneModel(iap.modelName, Vector3.one*700, Vector3.up*180, Vector3.zero);

		TuneButton( ( ) => { StartCoroutine( iapStore.PurchaseProduct( iap.sku , ( ) => { 
				progressData.gold += iap.gold; 
				progressData.money += iap.revenu; 
			} , ( ) => { } ) );
		} , true , Strings.Get("Купить"), 0, 0 );
		currencyPanel.SetText( "..." );
		StartCoroutine( iapStore.QueryPrice( iap.sku , ( string price ) => { currencyPanel.SetText( price ); } ) );
		if(iap.revenu>0)
			revenuText.text = "Добавляет <color=#33aa00ff>$</color>"+iap.revenu;
		else
			revenuText.text = "Добавляет <color=yellow>@</color>"+iap.gold;
		//icon.sprite = getSpritesCache( ).Find( SpritesCache.SpriteType.CURRENCY , iap.sprite.name );
		currencyPanels.ForEach(p=>p.Hide());
		//revenuText.text = Localization.GetString( "adds" );
	}
	
	void TuneModel(string modelName, Vector3 scales, Vector3 angles, Vector3 pos)
	{
		/*bool found = false;
		for(int i=0; i<modelRoot.childCount; i++)
		{
			if(modelRoot.GetChild(i).name == modelName)
			{
				found = true;
				modelRoot.GetChild(i).gameObject.SetActive(true);
				modelRoot.GetChild(i).transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
				_oldRotation = modelRoot.GetChild(i).transform.localRotation;
			}
			else
			{
				modelRoot.GetChild(i).gameObject.SetActive(false);
			}
		}
		if(!found)*/
		while(modelRoot.childCount>0)
			DestroyImmediate(modelRoot.GetChild(0).gameObject);
		StartCoroutine(LoadModelCoroutine(modelName, scales, angles, pos));
	}

	static Dictionary<string, GameObject> _models = new Dictionary<string, GameObject>();
	IEnumerator LoadModelCoroutine(string modelName, Vector3 scales, Vector3 angles, Vector3 pos)
	{
		GameObject model;
		if(_models.ContainsKey(modelName))
			model = _models[modelName];
		else
		{
			ResourceRequest request = Resources.LoadAsync<GameObject>(modelName);
			while(!request.isDone)
				yield return null;

			model = (GameObject)request.asset;
			if(!_models.ContainsKey(modelName))
				_models.Add(modelName, model);
		}
		GameObject go = Instantiate(model);
		go.name = modelName;
		go.transform.SetParent(modelRoot);
		go.transform.localScale = scales;
		go.transform.localRotation = Quaternion.Euler(angles);
		go.transform.localPosition = pos;
		int layer = LayerMask.NameToLayer ("Water");
		Array.ForEach (go.GetComponentsInChildren<Transform> (), t => t.gameObject.layer = layer);
		_oldRotation = go.transform.localRotation;
		Update();
	}

	void TuneButton( Action onClick , bool enable , string text , float dollars, float gold )
    {
        button.onClick.RemoveAllListeners( );
        if( onClick != null )
            button.onClick.AddListener( ( ) => { onClick( ); } );
        button.interactable = enable;
		buttonText.text = text;//Localization.GetString( text );
		if(dollars>0)
        	currencyPanel.SetDollars( dollars );
		else if(gold>0)
			currencyPanel.SetGold(gold);
		else 
			currencyPanel.SetText("");
    }

	void Update()
	{
		//Debug.Log(modelRoot.transform.position.y);
		//modelRoot.gameObject.SetActive(allwaysShow||(modelRoot.transform.position.y<-0.9f && 
		  //                             modelRoot.transform.position.y>-1.42f));
		if(modelRotationSpeed != 0 && modelRoot && modelRoot.childCount>0)
			//for(int i=0; i<modelRoot.childCount; i++)
			modelRoot.GetChild(0).Rotate(Vector3.forward*Time.deltaTime*modelRotationSpeed);
	}

	public void OnSelect()
	{
		GetComponent<Image>().color = Color.black/4;
		GetComponent<LayoutElement>().preferredHeight = _selectedPreferredHeight;
		currencyPanels.ForEach(p=>p.gameObject.SetActive(true));
		modelRotationSpeed = 100;
	}

	public void RotateModel()
	{
		modelRotationSpeed = 100;
	}

	public void OnDeselect()
	{
		GetComponent<Image>().color = Color.clear;
		GetComponent<LayoutElement>().preferredHeight = _deselectedPreferredHeight;
		currencyPanels.ForEach(p=>p.gameObject.SetActive(false));
		modelRotationSpeed = 0;
		//for(int i=0; i<modelRoot.childCount; i++)
		if(modelRoot.childCount>0)
			modelRoot.GetChild(0).transform.localRotation = _oldRotation;
	}
}

