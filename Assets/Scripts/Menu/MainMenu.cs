﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class MainMenu : MonoBehaviour 
{
	public CurrencyControll money;
	public CurrencyControll gold;
	public CurrencyControll bonus;
	public Text userName;
	public Button championshipButton;
	public Button storesButton;
	public Button locationsButton;
	public Button backButton;
	public Button cheatButton;
	public Button rulesButton;
	public Button addGold;
	public Button addDollars;
	public Button charButton;
	public Button fbButton;
	public Button vkButton;
	public Button tvButton;
	public Button wheelButton;
	public RectTransform rules;
	public ExperienceControll xpControll;

	public Action gotoLocations;
	public Action gotoStores;
	public Action gotoDollars;
	public Action gotoGold;
	public Action onCheat;
	public Action onCharacter;
	public Action onChampionship;
	public Action seeVideo;
	public Func<bool> showTVButton;
	public Action onWheel;

	public void Awake()
	{
#if UNITY_WEBPLAYER
		vkButton.gameObject.SetActive(false);
		fbButton.gameObject.SetActive(false);
		tvButton.gameObject.SetActive(false);
#endif
		tvButton.onClick.AddListener(()=>{seeVideo();});
		rules.gameObject.SetActive(false);
		rulesButton.onClick.AddListener(()=>{
			rules.gameObject.SetActive(true);
			backButton.gameObject.SetActive(true);
		});
		backButton.onClick.AddListener(()=>{ 
			gameObject.SetActive(true);
			backButton.gameObject.SetActive(false);
			rules.gameObject.SetActive(false);
		});
		backButton.gameObject.SetActive(false);
		locationsButton.onClick.AddListener(()=>{
			gameObject.SetActive(false);
			backButton.gameObject.SetActive(true);
			gotoLocations();
		});
		storesButton.onClick.AddListener(()=>{
			gameObject.SetActive(false);
			backButton.gameObject.SetActive(true);
			gotoStores();
		});

		championshipButton.onClick.AddListener(()=>{
			onChampionship();
		});

		charButton.onClick.AddListener(()=>{
			onCharacter();
		});

		cheatButton.onClick.AddListener(()=>{
			onCheat();
		});

		addDollars.onClick.AddListener(()=>{
			gameObject.SetActive(false);
			backButton.gameObject.SetActive(true);
			gotoDollars();
		});
		addGold.onClick.AddListener(()=>{
			gameObject.SetActive(false);
			backButton.gameObject.SetActive(true);
			gotoGold();
		});
		wheelButton.onClick.AddListener(()=>{onWheel();});
	}

	void Update()
	{
		tvButton.gameObject.SetActive(showTVButton());
	}

	public void Hide()
	{
		gameObject.SetActive (false);
		backButton.gameObject.SetActive(true);
	}

	public void OnEscape()
	{
		if (backButton.gameObject.activeSelf) 
		{
			gameObject.SetActive(true);
			backButton.gameObject.SetActive(false);
			rules.gameObject.SetActive(false);
		}
		else if(FindObjectOfType<GameScreen>() == null)
		{
			Application.Quit();
		}
	}
}