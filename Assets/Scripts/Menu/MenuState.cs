﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuState
{
	public int currentLocation = 0;
	public int currentBusiness = 0;
	public CombatTransfer.Mode currentMode = CombatTransfer.Mode.net;

	//dependencies
	MenuConfig _menuConfig;
	ProfileData _profileData;
	OpenIABStore _store;
	ProfileControll _profileControll;
	PhotonLobby _lobby;
	MenuCamera _menuCamera;

	public void Init(MenuConfig menuConfig, ProfileControll profileControll, PhotonLobby lobby, FacebookLogin flogin, MenuCamera menuCamera, OpenIABStore store, ProfileData profileData)
	{
		_menuConfig = menuConfig;
		_profileControll = profileControll;
		_profileData = profileData;
		_lobby = lobby;
		_store = store;

		_menuCamera = menuCamera;
		_menuCamera.StartRotateToLocation = OnNextLocation;
		_menuCamera.StartRotateToBusiness = OnNextBusiness;

		MenuTransfer transfer = GameObject.FindObjectOfType<MenuTransfer>();
		if(_menuConfig.progressData.tutorials == 0 && transfer == null)
		{
			Messager.SendStatistic(_profileControll.profileName.text+" запустил туториал");
			flogin.enabled = false;
			currentLocation = 3;
			_menuConfig.StartCoroutine(WaitWebLoginAndStartCurrentLocation());
			return;
		}

		if(transfer)
			transfer.Process(_menuConfig.progressData, _menuConfig.levels, _menuConfig.knives, _menuConfig.locations, _menuConfig.championShip, _profileControll.profileName.text);
	}

	public void OnCharacter()
	{
		CharSelectorTransfer transfer = new GameObject("CharSelectorTransfer").AddComponent<CharSelectorTransfer>();
		transfer.playerDatas = _menuConfig.players;
		ProfileData profileData = new ProfileData(){combats = _menuConfig.progressData.combats, experience = _menuConfig.progressData.experience};
		profileData.CalcLevel(_menuConfig.levels);
		transfer.level = profileData.level;
		transfer.charIdx = _menuConfig.progressData.charIdx;
		transfer.profileName = _profileControll.profileName.text;
		GameObject.DontDestroyOnLoad(transfer.gameObject);
		transfer.onEsc = (int charId)=>{ 
			if(charId != -1)
				_menuConfig.progressData.charIdx = charId; 
			Application.LoadLevel("menu"); 
		};
		Application.LoadLevel("chars");
	}

	public void OnChampionShip()
	{
		ChampTransfer transfer = new GameObject("ChampTransfer").AddComponent<ChampTransfer>();
		//transfer.locations = championShip;
		GameObject.DontDestroyOnLoad(transfer.gameObject);
		transfer.onSelectLocation = (int idx)=>StartCurrentLocation(_menuConfig.championShip[idx], CombatTransfer.Mode.championship, 0);
		transfer.onEsc = ()=>{ 
			Application.LoadLevel("menu"); 
		};
		transfer.locations = _menuConfig.championShip.ConvertAll(t=>t.name);
		transfer.progressData = _menuConfig.progressData;
		Application.LoadLevel("championship");
	}

	public void ConnectToNetGame()
	{
		_lobby.StartNetGame();//Reconnect();
		ConnectingScreen screen = GameScreen.CreateScreen<ConnectingScreen>();
		screen.message = _lobby.getProfileName()+" хочет играть в "+_lobby.getLocationName()+" версия "+Application.version;
		screen.ShowDynamicInfo("Игра по сети", _lobby.GetStatus, _lobby.LeaveNetGame/*ForceDisconnect*/);
		screen.onWaitFinished = ()=>{
			Messager.SendStatistic(_profileControll.profileName.text+" запустил тренировку в "+_menuConfig.locations[currentLocation].name);
			_menuConfig.progressData.money -= _menuConfig.locations[currentLocation].stake;
			StartCurrentLocation(currentMode == CombatTransfer.Mode.net?CombatTransfer.Mode.training:CombatTransfer.Mode.dash, 0);
		};
	}

	public void TryConnectToNetGame(CombatTransfer.Mode mode)
	{
		Debug.LogWarning("TryConnectToNetGame ");		
		if(currentLocation>4)
		{
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Игра по сети", "Эта локация пока в разработке", ()=>{});
		}
		else if(_menuConfig.locations[currentLocation].stake>_menuConfig.progressData.money)
		{
			MessageScreen screen = GameScreen.CreateScreen<MessageScreen>();
			screen.ShowInfo("Игра по сети", "Недостаточно денег на ставку", ()=>{_menuCamera.GotoDollars();});
		}
		else
		{
			currentMode = mode;
			ConnectToNetGame();
		}
	}

	public void ForceConnectToNetGame(int locationIdx, CombatTransfer.Mode mode)
	{
		if(_menuConfig.locations[locationIdx].stake<=_menuConfig.progressData.money)
		{
			ForceLoadRoomLocation(locationIdx, mode);
		}
	}

	public void ForceLoadRoomLocation(int locationIdx, CombatTransfer.Mode mode)
	{
		Debug.LogWarning("ForceLoadRoomLocation ");
		currentLocation = locationIdx;
		currentMode = mode;
		_menuCamera.GotoLocations();
		ConnectToNetGame();
	}

	IEnumerator WaitWebLoginAndStartCurrentLocation()
	{
		if(!_profileControll.loaded)
		{		
			MessageScreen screen = GameScreen.CreateScreen<MessageScreen>();
			screen.ShowInfo("VK", "Загрузка профиля", ()=>GameObject.Destroy(screen.gameObject));

			while(!_profileControll.loaded)
				yield return null;
			//Destroy(screen.gameObject);
		}
		StartCurrentLocation(CombatTransfer.Mode.training, 0);
	}

	public void OnNextLocation(LocationCanvas canvas, int add)
	{
		currentLocation+=add;
		if(currentLocation<0)
			currentLocation = _menuConfig.locations.Count-1;
		if(currentLocation>_menuConfig.locations.Count-1)
			currentLocation = 0;
		canvas.Init(_menuConfig.locations[currentLocation], currentLocation, ()=>TryConnectToNetGame(CombatTransfer.Mode.net),//()=>TryStartCurrentLocation(), 
			()=>TryConnectToNetGame(CombatTransfer.Mode.netDash), _lobby.GetPlayersForLocation);
	}

	public void OnNextBusiness(BusinessListCanvas canvas, int add)
	{
		currentBusiness+=add;
		if(currentBusiness<0)
			currentBusiness = 2-1;
		if(currentBusiness>2-1)
			currentBusiness = 0;
		if(currentBusiness == 0)
		{
			canvas.SetKnives(_menuConfig.knives, _menuConfig.progressData, _profileData.level);
		}
		else if(currentBusiness == 1)
		{
			canvas.AddIAPs(_store, _menuConfig.progressData);
		}
	}

	public void StartCurrentLocationOnConnect()
	{
		_menuConfig.progressData.UnlockLocation(currentLocation);
		_menuConfig.progressData.money -= _menuConfig.locations[currentLocation].stake;
		//_lobby.getLocationName = ()=>locations[_currentLocation].name;
		StartCurrentLocation(currentMode, _lobby.connectionId);
	}

	public void StartCurrentLocation(CombatTransfer.Mode mode, int humanIndex)
	{
		StartCurrentLocation(_menuConfig.locations[currentLocation], mode, humanIndex);
	}

	public void StartCurrentLocation(LocationData locationData, CombatTransfer.Mode mode, int humanIndex)
	{
		CombatTransfer transfer = CombatTransfer.Create(_menuConfig.players, 
			_menuConfig.knives, 
			locationData, 
			_menuConfig.progressData.currentKnife, 
			mode, 
			humanIndex, 
			_menuConfig.progressData.charIdx,
			_profileControll.profileName.text,
			_profileControll.profilePicture.sprite,
			_profileData.GetString(),
			mode==CombatTransfer.Mode.championship?_menuConfig.progressData.pathPlace:currentLocation, 
			_menuConfig.progressData.tutorials );
		transfer.cheatCounter = _menuConfig.cheatCounter;
		Application.LoadLevel(_menuConfig.locations[currentLocation].sceneName);
	}
}