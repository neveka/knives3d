using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Facebook.Unity;
using UnityEngine.UI;

public class LoginControll : MonoBehaviour 
{
	public Button loginButton;
	public Text loginButtonText;
	public Func<bool> wasLogin;
	public Action onLogin;
	public Action<Sprite> SetPicture;
	public Action<string> SetName;

	public void LogoutOnOtherLogin()
	{
		Logout();
		InitGUIElements(false);
	}

	protected virtual void RequestPicAndName(){}
	protected virtual void Login() {}
	protected virtual void Logout() {}

	protected void InitGUIElements(bool loading)
	{
		loginButton.interactable = !loading;
		if(loading)
		{
			loginButtonText.text = "...";
			loginButton.interactable = false;
		}
		else if (IsLoggedIn()) 
		{
			loginButtonText.text = Strings.Get("");
			loginButton.onClick.RemoveAllListeners();
			loginButton.onClick.AddListener(()=>{Logout(); InitGUIElements(false);});
			loginButton.interactable = false;
			RequestPicAndName();
			onLogin();
		}
		else
		{
			loginButtonText.text = Strings.Get(wasLogin!=null && wasLogin()?"":"$1000");
			loginButton.onClick.RemoveAllListeners();
			loginButton.onClick.AddListener(()=>{InitGUIElements(true); Login();});
			loginButton.interactable = true;
			SetPicture(null);
			SetName(null);
		}
	}

	protected virtual bool IsLoggedIn()
	{
		return false;
	}
}

public class FacebookLogin : LoginControll 
{
	public void Init(MenuConfig menuConfig, ProfileControll profileControll)
	{
		#if UNITY_WEBPLAYER
		enabled = false;
		#else
		wasLogin = ()=>menuConfig.progressData.wasFacebookLogin;
		onLogin = ()=>{
			if(!menuConfig.progressData.wasFacebookLogin)
				menuConfig.progressData.money+=1000; 
			menuConfig.progressData.wasFacebookLogin = true; 
		};
		SetName = profileControll.SetName;
		SetPicture = profileControll.SetPicture;
		#endif
	}

	// Awake function from Unity's MonoBehavior
	void Start ()
	{
		if (!FB.IsInitialized) 
		{
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
			InitGUIElements(true);
		} 
		else 
		{
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
			InitGUIElements(false);
		}
	}

	protected override void Login()
	{
		FindObjectOfType<VKLogin>().LogoutOnOtherLogin();
		var perms = new List<string>(){"public_profile", "email", "user_friends"};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}

	protected override void Logout() 
	{
		FB.LogOut();
	}

	protected override bool IsLoggedIn()
	{
		return FB.IsLoggedIn;
	}
	
	private void InitCallback ()
	{
		if (FB.IsInitialized) 
		{
			// Signal an app activation App Event
			FB.ActivateApp();
			// Continue with Facebook SDK
			// ...

			InitGUIElements(false);
		} 
		else 
		{
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}
	
	private void OnHideUnity (bool isGameShown)//?
	{
		if (!isGameShown) 
		{
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} 
		else 
		{
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}


	
	private void AuthCallback (ILoginResult result) 
	{
		if (FB.IsLoggedIn) 
		{
			// AccessToken class will have session details
			/*var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			Debug.Log(aToken.UserId);
			// Print current access token's granted permissions
			foreach (string perm in aToken.Permissions) 
			{
				Debug.Log(perm);
			}*/
		} 
		else 
		{
			GameScreen.CreateScreen<ErrorScreen>().ShowInfo("Facebook", "Не удалось залогиниться : "+result.Error, ()=>{});
			//Debug.Log("User cancelled login");
		}
		InitGUIElements(false);
	}

	protected override void RequestPicAndName()
	{
		FB.API("me?fields=first_name", HttpMethod.GET, DisplayUsername);
		FB.API("me/picture?type=square&height=64&width=64", HttpMethod.GET, FbGetPicture);
	}

	private void FbGetPicture(IGraphResult result)
	{
		if (result.Texture != null)
			SetPicture(Sprite.Create(result.Texture, new Rect(0, 0, result.Texture.width, result.Texture.height), new Vector2()));
		else
			SetPicture(null);
	}
	
	void DisplayUsername(IResult result) 
	{
		var FacebookFirstName = result.ResultDictionary ["first_name"];
		SetName(FacebookFirstName.ToString());
	}

	public void OnCombatWin()
	{
		if (FB.IsLoggedIn)
			FB.ShareLink(new Uri("https://vk.com/clubKnivesGame"),
		             Strings.Get("Я выиграл бой в ножички 3D"),
		             Strings.Get("А тебе слабо? Качай с Play Market, всегда рады новичкам"),
			             new Uri("https://pp.vk.me/c604829/v604829415/4301/Xf2awYV36qg.jpg"),
		             callback: this.ShareCallback
		             );
	}

	private void ShareCallback (IShareResult result) 
	{
		if(!String.IsNullOrEmpty(result.Error))
		{
			GameScreen.CreateScreen<ErrorScreen>().ShowInfo("Facebook", "Не удалось поделится с друзьями : "+result.Error, ()=>{});
		}
		else if (result.Cancelled) 
		{
			Debug.Log("Cancelled");
		} 
		else if (!String.IsNullOrEmpty(result.PostId)) 
		{
			// Print post identifier of the shared content
			Debug.Log(result.PostId);
		} else 
		{
			// Share succeeded without postID
			Debug.Log("ShareLink success!");
		}
	}
}
