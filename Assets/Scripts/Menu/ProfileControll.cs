﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Facebook.Unity;
using UnityEngine.UI;

public class ProfileControll: MonoBehaviour
{
	public Image profilePicture;
	public Text profileName;
	public Sprite guestSprite;
	public bool loaded = false;

	public void Init()
	{
		SetPicture(null);
		SetName(null);
	}

	public void SetPicture(Sprite sprite)
	{
		profilePicture.sprite = sprite==null?guestSprite:sprite;
	}
	
	public void SetName(string name)
	{
		profileName.text = string.IsNullOrEmpty(name)?ProgressData.GetGuestName():name;
		
	}
}