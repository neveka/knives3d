﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WebStore : MonoBehaviour , IIAPStore
{
	protected Action _onSucсess;
	protected Action _onFail;
	protected Action<string> _onGetPrice;
	protected List<CurrencyIAPData> _currencyIAPs;

	public List<CurrencyIAPData> GetCurrencyIAPs( )
	{
		return _currencyIAPs;
	}

	public bool IsBusy()
	{
		return _onSucсess != null||_onGetPrice != null;
	}

	public IEnumerator Init( List<CurrencyIAPData> currencyIAPs )
	{
		_currencyIAPs = currencyIAPs;
		yield break;
	}

	public IEnumerator PurchaseProduct( string sku , Action onSucсess , Action onFail )
	{
		if(IsBusy())
			yield break;
		Application.ExternalCall("OnOrder", sku);
		_onSucсess = onSucсess;
		_onFail = onFail;
	}

	public IEnumerator QueryPrice( string sku , Action<string> onGetPrice )
	{
		//if(IsBusy())
			yield break;
		//Application.ExternalCall("OnInfo", sku);
	}

	public void OnOrderSuccess(string orderId)
	{
		_onSucсess();
		_onSucсess = null;
		_onFail = null;
	}

	public void OnOrderFail()
	{
		_onFail();
		_onSucсess = null;
		_onFail = null;
	}
}
