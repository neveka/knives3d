﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ConnectingScreen : MessageScreen 
{
	public string message;
	public Button smsButton;
	public Button mailButton;
	public Action onWaitFinished; 
	public Text timer;
	float _startTime;

	void Awake()
	{
		#if UNITY_WEBPLAYER
		smsButton.gameObject.SetActive(false);
		mailButton.gameObject.SetActive(false);
		#endif
		_startTime = Time.time;
	}

	public void SendSms()
	{
		FindObjectOfType<Messager>().SendSMS(message);
	}

	public void SendMail()
	{
		FindObjectOfType<Messager>().SendMail(message);
	}

	protected override void Update()
	{
		base.Update();
		float time = 5 - Time.time + _startTime;
		if(time<=0 && onWaitFinished!=null)
		{
			onWaitFinished();
			onWaitFinished = null;
			time = 0;
		}

		int secs = (int)time;
		int milisecs = (int)((time-(int)time)*100);
		timer.text = (secs<10?"0":"")+secs+":"+(milisecs<10?"0":"")+milisecs;
	}
}
