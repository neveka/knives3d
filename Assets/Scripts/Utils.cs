﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public static class Utils
{
	public static bool DoBelong(Vector3 a, Vector3 b, Vector3 point)
	{ 
		float eps = 0.000001f; 
		return ((point.x<b.x+eps && point.x>a.x-eps)||(point.x>b.x-eps && point.x<a.x+eps))&&
			((point.z<b.z+eps && point.z>a.z-eps)||(point.z>b.z-eps && point.z<a.z+eps))&&
				Mathf.Abs((point.z-a.z)*(b.x-a.x)-(point.x-a.x)*(b.z-a.z)) <= eps;
	} 

	public static bool DoOverlap(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
	{
		return(DoBelong(a,b,c)?1:0)+(DoBelong(a,b,d)?1:0)+(DoBelong(c,d,b)?1:0)+(DoBelong(c,d,a)?1:0)==2;
	}

	public static bool AreNeighors(List<Vector3> poly1, List<Vector3> poly2)
	{
		for(int i=0; i<poly1.Count; i++)
		{
			Vector3 a = poly1[i];
			Vector3 b = poly1[i<poly1.Count-1?i+1:0];
			for(int j=0; j<poly2.Count; j++)
			{
				Vector3 c = poly2[j];
				Vector3 d = poly2[j<poly2.Count-1?j+1:0];
				if(Utils.DoOverlap(a, b, c, d ))
					return true;
			}
		}
		return false;
	}

	public static bool AreNeighors(List<List<Vector3>> vertexes1, List<List<Vector3>> vertexes2)
	{
		for(int i=0; i<vertexes1.Count; i++)
		{
			for(int j=0; j<vertexes2.Count; j++)
			{
				if(Utils.AreNeighors(vertexes1[i], vertexes2[j]))
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool IsArounded(float radius, List<List<Vector3>> vertexes)
	{
		float eps = 0.0001f; 
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{
				if(vertexes[i][j].magnitude>=radius-eps)
					return false;
			}
		}
		return true;
	}

	public static bool IsInside(Vector3 point, List<List<Vector3>> vertexes)
	{
		Vector3 throwOut = point+Vector3.left*100;
		Vector3 intersecPoint = Vector3.zero;
		for(int i=0; i<vertexes.Count; i++)
		{
			/*
(x1 - x0) * (y2 - y1) - (x2 - x1) * (y1 - y0)
(x2 - x0) * (y3 - y2) - (x3 - x2) * (y2 - y0)
(x3 - x0) * (y1 - y3) - (x1 - x3) * (y3 - y0)
			 */
			int counter = 0;
			for(int j=0; j<vertexes[i].Count; j++)
			{
				float res = Utils.IsIntersection(point, throwOut, vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], ref intersecPoint);
				if(res == 0)
					counter++;
			}
			if(counter == 1)
				return true;
		}
		return false;
	}

	public static bool IsInside(Vector3 point1, Vector3 point2, List<List<Vector3>> vertexes)
	{
		if(!IsInside(point1, vertexes)||!IsInside(point2, vertexes))
			return false;
		Vector3 intersecPoint = Vector3.zero;
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{				
				float res = Utils.IsIntersection(point1, point2, vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], ref intersecPoint);
				if(res == 0 && IsOnEdge(intersecPoint, vertexes))
					return false;
			}
		}
		return true;
	}

	public static bool IsOnEdge(Vector3 point, List<List<Vector3>> vertexes)
	{
		int counter = 0;
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{
				if(Utils.DoBelong(vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], point))
					counter++;
			}
		}
		return counter == 1;
	}

	public static Vector3 FindCenter(List<List<Vector3>> vertexes)
	{
		int counter = 0;
		Vector3 sum = Vector3.zero;
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{
				sum+=vertexes[i][j];
				counter++;
			}
		}
		return sum/counter;
	}

	public static Vector3 FindCenterInside(List<List<Vector3>> vertexes)
	{
		Vector3 pos = Vector3.one*100500;
		List<List<Vector3>> vertexes2 = new List<List<Vector3>>(vertexes);
		while(!IsInside(pos, vertexes) && vertexes2.Count>0)
		{
			pos = FindCenter(vertexes2);
			vertexes2.RemoveAt(0);
		}
		return pos;
	}

	public static float IsIntersection(Vector3 start1, Vector3 end1, Vector3 start2, Vector3 end2, ref Vector3 out_intersection)
	{
		Vector3 dir1 = end1 - start1;
		Vector3 dir2 = end2 - start2;
		
		//считаем уравнения прямых проходящих через отрезки
		float a1 = -dir1.z;
		float b1 = +dir1.x;
		float d1 = -(a1*start1.x + b1*start1.z);
		
		float a2 = -dir2.z;
		float b2 = +dir2.x;
		float d2 = -(a2*start2.x + b2*start2.z);
		
		//подставляем концы отрезков, для выяснения в каких полуплоскотях они
		float seg1_line2_start = a2*start1.x + b2*start1.z + d2;
		float seg1_line2_end = a2*end1.x + b2*end1.z + d2;
		
		float seg2_line1_start = a1*start2.x + b1*start2.z + d1;
		float seg2_line1_end = a1*end2.x + b1*end2.z + d1;
		
		//если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
		if (seg2_line1_start * seg2_line1_end >= 0) 
		{
			return seg2_line1_start;
		}
		if (seg1_line2_start * seg1_line2_end >= 0 ) 
		{
			return seg1_line2_start;
		}
		float u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
		out_intersection =  start1 + u*dir1;
		
		return 0;
	}

	public static float PolyArea(List<Vector3> poly)
	{
		float sum=0;
		//Debug.LogWarning(poly.Count);
		for(int i=1; i<poly.Count-1; i++)
		{
			sum+=Vector3.Cross(poly[i]-poly[0], poly[i+1]-poly[0]).magnitude;
			//Debug.Log(i+":"+Vector3.Cross(poly[i]-poly[0], poly[i+1]-poly[0]).magnitude+" "+sum);
		}
		return sum/2;
	}

	public static float PolyArea( List<List<Vector3>> vertexes)
	{
		float sum = 0;

		for(int i=0; i<vertexes.Count; i++)
		{
			sum += PolyArea(vertexes[i]);
		}
		return sum;
	}

	public static float GetSqrDistance(Vector3 point1, Vector3 point2, Vector3 point)
	{
		Vector3 pp1 = point-point1;
		Vector3 pp2 = point-point2;
		Vector3 p1p2 = point1-point2;
		if(Vector3.Dot(pp1,-p1p2)<0 || Vector3.Dot(pp2,p1p2)<0)
			return Mathf.Min(pp1.sqrMagnitude, pp2.sqrMagnitude);
		return Vector3.Cross(-pp2,-pp1).sqrMagnitude/p1p2.sqrMagnitude;	}

	public static float GetDistance(List<List<Vector3>> vertexes, Vector3 point)
	{
		float minSqrDist = float.MaxValue;
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{
				minSqrDist = Mathf.Min(minSqrDist, Utils.GetSqrDistance(vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], point));
			}
		}
		return Mathf.Sqrt(minSqrDist);
	}

	public static Vector3 GetVector3(string rString)
	{
		string[] temp = rString.Split(',');
		float x = float.Parse(temp[0]);
		float y = float.Parse(temp[1]);
		float z = float.Parse(temp[2]);
		Vector3 rValue = new Vector3(x,y,z);
		return rValue;
	}

	public static string VertexesToString(List<List<Vector3>> vertexes)
	{
		List<string> strings = new List<string>();
		vertexes.ForEach(l=>strings.Add(string.Join("*", l.ConvertAll(v=>v.x.ToString()+","+v.y.ToString()+","+v.z.ToString()).ToArray())));
		return string.Join("+", strings.ToArray());
	}

	public static List<List<Vector3>> StringToVertexes(string str)
	{
		List<List<Vector3>> vertexes = new List<List<Vector3>>();
		if(string.IsNullOrEmpty(str))
			return vertexes;
		List<string> strings = new List<string>(str.Split('+'));
		strings.ForEach(s=>vertexes.Add( new List<string>(s.Split('*')).ConvertAll(ss=>GetVector3(ss)) ));
		return vertexes;
	}

	public static string SpriteToString(Sprite sprire)
	{
		byte[] bytes = sprire.texture.GetRawTextureData();
		/*try
		{
			bytes = sprire.texture.EncodeToPNG();
		}
		catch(Exception ex)
		{
		}
		if(bytes == null)
		{
			try
			{
				bytes = sprire.texture.EncodeToJPG();
			}
			catch(Exception ex)
			{
			}
		}*/
		return bytes==null?null:(sprire.texture.width+"|"+sprire.texture.height+"|"+sprire.texture.format.ToString()+"|"+sprire.texture.mipmapCount+"|"+Convert.ToBase64String(bytes));
	}

	public static Sprite StringToSprite(string str)
	{
		if(str == null)
			return null;
		string[] strs = str.Split('|');
		byte[] bytes = System.Convert.FromBase64String (strs[4]);
		Texture2D tex = new Texture2D(int.Parse(strs[0]),int.Parse(strs[1]), 
		                              (TextureFormat)Enum.Parse(typeof(TextureFormat), strs[2]), int.Parse(strs[3])>1);
		tex.LoadRawTextureData(bytes);//LoadImage(bytes);
		tex.Apply();
		return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
	}

	public static string ColorToHex(Color color)
	{
		return string.Format("#{0}{1}{2}", 
				                              ((int)(color.r * 255)).ToString("X2"), 
				                              ((int)(color.g * 255)).ToString("X2"), 
				                              ((int)(color.b * 255)).ToString("X2"));
	}
}
