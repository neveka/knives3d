﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using AIMLbot;
using SimpleJson;

//using System.Linq;
//using System.Threading.Tasks;
//using System.Windows.Forms;
//using System.Text;
//using System.Net.Sockets;

public class ChatBot : MonoBehaviour 
{
	public string yandexApiKey = "trnsl.1.1.20161119T092054Z.55b6b974a1a2fd9f.a31f12651cb11d9cd28ba4b5040211e3e7f79f12";

	Bot myBot;
	User myUser;
	void Start()
	{
		myBot = new Bot();
		myBot.loadSettings();
		myUser = new User("consoleUser", myBot);
		myBot.isAcceptingUserInput = false;
		myBot.loadAIMLFromFiles();
		myBot.isAcceptingUserInput = true;
	}

	string result;
	public IEnumerator Ask(string text, Action<string> onAnswer)
	{
		yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 5f));
		yield return StartCoroutine(Translate(text, "ru-en"));

		Request r = new Request(result, myUser, myBot);
		Result res = myBot.Chat(r);
		Debug.Log("Bot: " + res.Output);
		
		yield return StartCoroutine(Translate(res.Output, "en-ru"));
		Debug.Log(result);
		onAnswer(result);
	}

	IEnumerator Translate(string text, string langs)
	{
		WWW www = new WWW("https://translate.yandex.net/api/v1.5/tr.json/translate?key="+yandexApiKey+"&text="+text+"&lang="+langs);
		while(!www.isDone)
			yield return null;
		//Debug.Log(www.text);
		JsonObject obj = (JsonObject)SimpleJson.SimpleJson.DeserializeObject(www.text);
		result = obj["text"].ToString();
		result = result.Substring(2, result.Length-4);
	}
	/*private NetworkStream stream;
	private TcpClient client;

	// Use this for initialization
	void Start () 
	{
		Int32 port = 6667;
		client = new TcpClient("irc.twitch.tv", port);
		// Enter in channel (the username of the stream chat you wish to connect to) without the #
		string channel = "awesomejoey";
		
		// Get a client stream for reading and writing.
		//  Stream stream = client.GetStream();
		
		stream = client.GetStream();
		
		// Send the message to the connected TcpServer. 
		
		string loginstring = "PASS oauth:byni1ivxyqpf3ih6jbagenm46spbp9\r\nNICK neveka81\r\n";
		Byte[] login = System.Text.Encoding.ASCII.GetBytes(loginstring);
		stream.Write(login, 0, login.Length);

		Debug.Log("Sent login.\r\n");
		Debug.Log(loginstring);
		
		// Receive the TcpServer.response.
		// Buffer to store the response bytes.
		byte[] data = new Byte[512];
		
		// String to store the response ASCII representation.
		String responseData = String.Empty;
		
		// Read the first batch of the TcpServer response bytes.
		Int32 bytes = stream.Read(data, 0, data.Length);
		responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
		Debug.LogFormat("Received WELCOME: \r\n\r\n{0}", responseData);
		
		// send message to join channel
		
		string joinstring = "JOIN " + "#" + channel + "\r\n";
		Byte[] join = System.Text.Encoding.ASCII.GetBytes(joinstring);
		stream.Write(join, 0, join.Length);
		Debug.Log("Sent channel join.\r\n");
		Debug.Log(joinstring);
		
		// PMs the channel to announce that it's joined and listening
		// These three lines are the example for how to send something to the channel
		
		string announcestring = channel + "!" + channel + "@" + channel +".tmi.twitch.tv PRIVMSG " + channel + " BOT ENABLED\r\n";
		Byte[] announce = System.Text.Encoding.ASCII.GetBytes(announcestring);
		stream.Write(announce, 0, announce.Length);
		
		// Lets you know its working
		
		Debug.Log("TWITCH CHAT HAS BEGUN.\r\n\r\nr.");
		Debug.Log("\r\nBE CAREFUL.");

	//{
		byte[] myReadBuffer = new byte[1024];
		StringBuilder myCompleteMessage = new StringBuilder();
		int numberOfBytesRead = 0;
		
		// Incoming message may be larger than the buffer size.
		do
		{
			try { numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length); }
			catch (Exception e)
			{
				Debug.LogFormat("OH SHIT SOMETHING WENT WRONG\r\n", e);
			}
			
			myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
		}
		
		// when we've received data, do Things
		
		while (stream.DataAvailable);
		
		// Print out the received message to the console.
		Debug.Log(myCompleteMessage.ToString());
		switch (myCompleteMessage.ToString())
		{
			// Every 5 minutes the Twitch server will send a PING, this is to respond with a PONG to keepalive
			
		case "PING :tmi.twitch.tv\r\n":
			try { 
				Byte[] say = System.Text.Encoding.ASCII.GetBytes("PONG :tmi.twitch.tv\r\n");
				stream.Write(say, 0, say.Length);
				Debug.Log("Ping? Pong!");
			}
			catch (Exception e)
			{
				Debug.LogFormat("OH SHIT SOMETHING WENT WRONG\r\n", e);
			}
			break;
			
			// If it's not a ping, it's probably something we care about.  Try to parse it for a message.
		default:
			try { 
				string messageParser = myCompleteMessage.ToString();
				string[] message = messageParser.Split(':');
				string[] preamble = message[1].Split(' ');
				string tochat;
				
				// This means it's a message to the channel.  Yes, PRIVMSG is IRC for messaging a channel too
				if (preamble[1] == "PRIVMSG")
				{
					string[] sendingUser = preamble[0].Split('!');
					tochat = sendingUser[0] + ": " + message[2];
					
					// sometimes the carriage returns get lost (??)
					if (tochat.Contains("\n") == false)
					{
						tochat = tochat + "\n";
					}
					
					// Ignore some well known bots
					if (sendingUser[0] != "moobot" && sendingUser[0] != "whale_bot")
					{
						//SendKeys.SendWait(tochat.TrimEnd('\n'));
					}
					Debug.Log(tochat);
				}
				// A user joined.
				else if (preamble[1] == "JOIN")
				{
					string[] sendingUser = preamble[0].Split('!');
					tochat = "JOINED: " + sendingUser[0];
					    Debug.Log(tochat);
					//SendKeys.SendWait(tochat.TrimEnd('\n'));
				}
				//Debug.Log("Raw output: " + message[0] + "::" + message[1] + "::" + message[2]);
			}
			// This is a disgusting catch for something going wrong that keeps it all running.  I'm sorry.
			catch (Exception e)
			{
				Debug.LogFormat("OH SHIT SOMETHING WENT WRONG\r\n", e);
			}
			
			// Uncomment the following for raw message output for debugging
			//
			//
			Debug.Log("You received the following message : " + myCompleteMessage);
			break;
		}
	}*/
}
