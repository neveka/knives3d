﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class MessageScreen : GameScreen 
{
	public Text title;
	public CurrencyControll rewardControll;

	public void ShowInfo(string titleText, string text, Action onClose)
	{
		_onClose = onClose;
		title.text = titleText;
		rewardControll.SetText(text);
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}
	
	public void ShowDynamicInfo(string titleText, Func<string> getText, Action onClose)
	{
		_onClose = onClose;
		title.text = titleText;
		rewardControll.SetGetStringFunc(getText);
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}
}
