﻿using UnityEngine;
using System.Collections;

public class SphereRotation : MonoBehaviour 
{
	public float speed = 0.1f;
	private Vector3 _oldMousePos = Vector3.zero;
	private Vector3 _delta;
	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{
			_oldMousePos = Input.mousePosition;
		}
		if(Input.GetMouseButton(0) && _oldMousePos!=Vector3.zero)
		{
			_delta = (Input.mousePosition-_oldMousePos)*speed;
			transform.RotateAround(Vector3.zero, Vector3.up, -_delta.x);
			transform.RotateAround(Vector3.zero, Vector3.right, _delta.y);
			_oldMousePos = Input.mousePosition;
		}
		if(Input.GetMouseButtonUp(0))
		{
			_oldMousePos = Vector3.zero;
		}
	}
}
