﻿using UnityEngine;
using System.Collections;

public class Town : MonoBehaviour 
{
	private bool _selected;
	//public LocationData data;
	public float speed = 10;
	private SpriteRenderer _sprite;
	private TextMesh _text;

	void Awake()
	{
		_sprite = GetComponentInChildren<SpriteRenderer>();
		_text = GetComponentInChildren<TextMesh>();
	}

	void Update()
	{
		if(_selected)
			_sprite.transform.localScale = Vector3.one*0.1f*(1+Mathf.Sin(Time.time*speed)*0.3f);
	}

	public void SetText(string text)
	{
		_text.text = text;
	}

	public void SetColor(Color c)
	{
		_sprite.color = c;
	}

	public void SetSelected(bool selected)
	{
		_selected = selected;
		_sprite.transform.localScale = Vector3.one*0.1f;
	}
}
