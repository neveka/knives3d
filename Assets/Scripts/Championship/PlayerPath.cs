﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerPath : MonoBehaviour {

	public List<Town> towns = new List<Town>();
	private SandDrawing _drawing;
	private ChampTransfer _transfer;
	private PhotonLobby _lobby;

	void Awake()
	{
		_drawing = FindObjectOfType<SandDrawing>();
		_lobby = FindObjectOfType<PhotonLobby>();
		_transfer = ChampTransfer.GetOrCreate();

		FindObjectOfType<CameraZoom>().onSelectTown = (Town town)=>{
			_transfer.onSelectLocation(towns.IndexOf(town));
			RePaint();
		};
	}

	void Start()
	{
		RePaint();
	}

	// Use this for initialization
	void RePaint () 
	{
		_drawing.DeleteWithName("Path");
		for(int i=0; i<towns.Count; i++)
		{
			Color c = i<_transfer.progressData.pathPlace?(Color.yellow+Color.red)/2:Color.white;
			if(i>0)
				_drawing.DrawLine(towns[i-1].transform.GetChild(0).position, 
			                  towns[i].transform.GetChild(0).position, "Path", c);
			towns[i].SetColor(c);
			towns[i].SetText(_transfer.locations[i]);
			towns[i].SetSelected(i==_transfer.progressData.pathPlace);
		}
	}

	void OnDestroy()
	{
		if(_transfer && _transfer.gameObject)
			DestroyImmediate(_transfer.gameObject);
		if(_lobby && _lobby.gameObject)
			DestroyImmediate(_lobby.gameObject);//?
	}
	
	public void Escape()
	{
		_transfer.onEsc();
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape) && FindObjectOfType<GameScreen>() == null) 
		{
			Escape();
		}
	}
}
