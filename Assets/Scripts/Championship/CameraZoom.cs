﻿using UnityEngine;
using System.Collections;
using System;

public class CameraZoom : MonoBehaviour 
{
	public Camera cam;
	public float speed = 1;
	public float min = 8;
	public float max = 80;
	public Action<Town> onSelectTown;
	
	// Update is called once per frame
	void Update () 
	{
		float zoom = Input.GetAxis("Mouse ScrollWheel");
		if(zoom!=0)
		{
			cam.fieldOfView+=zoom*speed;
			cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, min, max); 
		}

		if(Input.GetMouseButtonDown(0))
		{
			RaycastHit[] hits = Physics.RaycastAll(cam.ScreenPointToRay(Input.mousePosition));
			if(hits!=null && hits.Length>0 && hits[0].collider && hits[0].collider.GetComponentInParent<Town>())
			{
				onSelectTown(hits[0].collider.GetComponentInParent<Town>());
			}
		}
	}
}
