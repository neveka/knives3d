﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChampTransfer : MonoBehaviour 
{
	public ProgressData progressData;
	public List<string> locations = new List<string>();
	public Action onEsc;
	public Action<int> onSelectLocation;
	public static ChampTransfer GetOrCreate()
	{
		ChampTransfer trans = FindObjectOfType<ChampTransfer>();
		if(trans != null)
			return trans;
		trans = new GameObject("ChampTransfer").AddComponent<ChampTransfer>();
		trans.locations.Add("Деревня1");
		trans.locations.Add("Деревня2");
		trans.locations.Add("Деревня3");
		trans.progressData = new ProgressData();
		trans.onSelectLocation = (int idx)=>{trans.progressData.pathPlace = idx+1;};
		return trans;
	}
}
