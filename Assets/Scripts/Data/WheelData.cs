﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class PrizeData 
{
	public string type;
	public int amount;
	public string name;
	public int chance;
	public string modelName;
}
[Serializable]
public class WheelData
{
	public List<PrizeData> prizeDatas = new List<PrizeData>();
}
