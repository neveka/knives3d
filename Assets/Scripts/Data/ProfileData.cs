﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ProfileData 
{
	public int combats;
	public int experience;

	public int level = 1;
	public int nextExp = 1000;
	public int newExp = 0;

	public void Init(MenuConfig menuConfig)
	{
		combats = menuConfig.progressData.combats;
		experience = menuConfig.progressData.experience;
		CalcLevel(menuConfig.levels);
	}

	public string GetString()
	{
		return ""+combats+"|"+experience+"|"+level+"|"+nextExp+"|"+newExp;
	}

	public void CalcLevel(List<int> levels)
	{
		level = 0;
		nextExp = levels[level];
		newExp = experience;
		while(newExp>nextExp)
		{
			newExp -= nextExp;
			level++;
			nextExp = level<levels.Count?levels[level]:(levels[levels.Count-1]+1000*(level-levels.Count));
		}
		level++;
	}

	public void FillFromString(string str)
	{
		string[] splitted = str.Split(new char[]{'|'});
		combats = int.Parse(splitted[0]);
		if(splitted.Length>1)
			experience = int.Parse(splitted[1]);
		if(splitted.Length>2)
			level = int.Parse(splitted[2]);
		if(splitted.Length>3)
			nextExp = int.Parse(splitted[3]);
		if(splitted.Length>4)
			newExp = int.Parse(splitted[4]);
	}
}
