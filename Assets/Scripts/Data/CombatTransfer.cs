﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatTransfer: MonoBehaviour
{
	public enum Mode
	{
		none,
		training,
		net,
		championship,
		dash,
		netDash
	}

	public List<string> players = new List<string>();
	public List<string> knives = new List<string>();

	public List<PlayerData> playerDatas = new List<PlayerData>();
	public List<KnifeData> knifeDatas = new List<KnifeData>();
	//public List<StoneData> stoneDatas = new List<StoneData>();

	public Tutorial.Step tutorialStep;
	public int count;
	public Mode mode;
	public int humanIndex = 0;
	public float minArea = 1;
	public float throwTime;
	public Vector3[] windDirInterval;
	public int locationIdx;
	public int cheatCounter;

	public PlayerData GetPlayerData(int i)
	{
		return playerDatas.Find(pd=>pd.name == players[i]);
	}

	public KnifeData GetKnifeData(int i)
	{
		return knifeDatas.Find(pd=>pd.name == knives[i]);
	}

	public Vector3 GetRandomWindDir()
	{
		if(windDirInterval.Length == 0)
			return Vector3.zero;
		return windDirInterval[Random.Range(0, windDirInterval.Length)];
	}

	//static Color [] _colors = {Color.red, Color.blue, Color.green, Color.yellow, Color.cyan, Color.black, Color.white};
	public static CombatTransfer Create(List<PlayerData> playerDatas, 
	                          List<KnifeData> knifeDatas, 
	                          LocationData locationData, 
	                          string playerKnifeName, 
	                          Mode mode, 
	                          int humanIndex,
	                          int charIndex,
	                          string userText, 
	                          Sprite userSprite, 
	                          string userProfileString, 
	                          int locationIdx, 
	                          int userTutorials)
	{
		bool tut = userTutorials==0 && mode == Mode.training;
		bool dash = mode == Mode.dash||mode == Mode.netDash;
		CombatTransfer trans = new GameObject("CombatTransfer").AddComponent<CombatTransfer>();
		trans.mode = mode;
		trans.locationIdx = locationIdx;
		trans.throwTime = dash?locationData.dashTime:locationData.throwTime;
		trans.windDirInterval = tut?new Vector3[1]{Vector3.zero}:locationData.windInterval;
		trans.minArea = locationData.minArea;
		trans.humanIndex = humanIndex;
		trans.count = mode == Mode.net||mode == Mode.netDash||mode == Mode.dash?2:locationData.players;
		trans.knifeDatas = knifeDatas;
		trans.knives.Add(tut?"Сабля":playerKnifeName);
		trans.tutorialStep = tut?Tutorial.Step.arrowsLesson:Tutorial.Step.none;
		trans.playerDatas = new List<PlayerData>(playerDatas);//?

		if(mode == Mode.net||mode == Mode.netDash)
		{
			trans.ReplacePlayerData(charIndex, "исправленный враг N"+humanIndex, userText, userSprite, userProfileString);
			trans.players.Add("исправленный враг N"+humanIndex);//?
		}
		else
		{
			trans.ReplacePlayerData(charIndex, "исправленный враг N0", userText, userSprite, userProfileString);
			trans.players.Add("исправленный враг N0");

			for(int i=1; i<trans.count; i++)
			{
				trans.players.Add("враг N"+UnityEngine.Random.Range(0,5));
				trans.knives.Add(knifeDatas[UnityEngine.Random.Range(0,knifeDatas.Count)].name);
			}
		}
		DontDestroyOnLoad(trans);
		return trans;
	}

	public void PatchOtherPlayerDatas(int prototypeId, int otherPlayerConnectionId, string otherPlayerName, Sprite otherPlayerSprite, string otherPlayerProfileString)
	{
		ReplacePlayerData(prototypeId, "исправленный враг N"+otherPlayerConnectionId, otherPlayerName, otherPlayerSprite, otherPlayerProfileString);
	}

	void ReplacePlayerData(int prototypeId, string otherPlayerDataName, string otherPlayerName, Sprite otherPlayerSprite, string otherPlayerProfileString)
	{
		PlayerData oldData = playerDatas.Find(pd=>pd.name == "враг N"+prototypeId);
		PlayerData newData = oldData.MakeClone();
		newData.prototypeId = prototypeId;
		newData.name = otherPlayerDataName;
		newData.profileData.FillFromString(otherPlayerProfileString);
		newData.profileName = otherPlayerName;
		newData.profileSprite = otherPlayerSprite;
		//newData.profileSpriteUrl = otherPlayerSpriteUrl;
		playerDatas.Add(newData);
	}

	public static CombatTransfer GetOrCreateDefault()
	{
		CombatTransfer trans = GameObject.FindObjectOfType<CombatTransfer>();
		if(!trans)
		{
			trans = new GameObject("CombatTransfer").AddComponent<CombatTransfer>();
			trans.count = 2;
			trans.mode = Mode.dash;
			trans.throwTime = 600000;
			trans.windDirInterval = new Vector3[]{Vector3.left, Vector3.left*2};
			trans.minArea = 1;
			trans.locationIdx = 3;
			trans.tutorialStep = Tutorial.Step.none;//arrowsLesson;
			trans.players.Add("игрок N1");
			trans.players.Add("игрок N2");
			//trans.players.Add("игрок N3");
			trans.knives.Add("Заточка");
			trans.knives.Add("Заточка");
			//trans.knives.Add("Заточка");
			trans.playerDatas.Add(new PlayerData(){name="игрок N1", color= Color.red, materialName = "", modelName = "Dudess"});
			trans.playerDatas.Add(new PlayerData(){name="игрок N2", color= Color.green, materialName = "", modelName = "Dudess"});
			//trans.playerDatas.Add(new PlayerData(){name="игрок N3", color= Color.blue, materialName = "", modelName = "Dude"});
			trans.knifeDatas.Add(new KnifeData(){name="Заточка", modelName = "knife3a", price=10, range=50, thrustChance=100, control=100});
			trans.knifeDatas.Add(new KnifeData(){name="Заточка", modelName = "knife3a", price=10, range=50, thrustChance=100, control=100});
			//trans.knifeDatas.Add(new KnifeData(){name="Заточка", modelName = "knife3a", price=10, range=50, thrustChance=100, control=100});
		}
		return trans;
	}
}
