﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class KnifeData 
{
	public string name;
	public string modelName;
	public string effectName;
	public float price;
	public float gold;
	public string desc;
	public int unlockLevel;

	public float range = 50;
	public float thrustChance = 50;
	public float control = 20;
	public float weight = 30;
	public float lenfth = 15;
}
