﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[Serializable]
public class CurrencyIAPData
{
	public string name;
    public string sku;
	public float revenu;
	public float gold;
	public string modelName;
}