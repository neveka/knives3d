﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class PlayerData
{
	public string name;
	public int prototypeId;
	public int unlockLevel;

	public Sprite profileSprite;
	//public Sprite profileSpriteUrl;
	public string profileName;
	public ProfileData profileData = new ProfileData();

	public string modelName;
	public Color color;
	public string materialName;
	//public List<string> clothes;

	public PlayerData MakeClone()
	{
		return (PlayerData)base.MemberwiseClone();
	}
}
