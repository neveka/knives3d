﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ProgressData
{
	/*public string name
	{
		get { return PlayerPrefs.GetString("name", "Unknown");}
		set { PlayerPrefs.SetString("name", value);}
	}*/

	public static string GetGuestName()
	{
		return Strings.Get("Гость")+"_"+
		      (SystemInfo.deviceName.Contains("unknown")?SystemInfo.deviceModel:SystemInfo.deviceName);
		
	}

	public static string GetId()
	{
		return Facebook.Unity.AccessToken.CurrentAccessToken!=null?("user"+Facebook.Unity.AccessToken.CurrentAccessToken.UserId):
			(VK.Instance.AccessToken!=null?("user"+VK.Instance.AccessToken):
			  ("guest"+SystemInfo.deviceModel).Replace(" ",""));

	}

	public void Fix(List<KnifeData> knives)
	{
		if(knives.Find(k=>k.name == currentKnife)==null)
		{
			currentKnife = "Заточка";
			AddKnife("Заточка");
		}
		if(charIdx == -1)
			charIdx = 0;
	}

	public int pathPlace
	{
		get { return PlayerPrefs.GetInt("pathPlace", 0);}
		set { PlayerPrefs.SetInt("pathPlace", value);}
	}

	public float money 
	{ 
		get { return PlayerPrefs.GetFloat("money", 100);}
		set { PlayerPrefs.SetFloat("money", value);}
	}

	public int experience 
	{ 
		get { return PlayerPrefs.GetInt("experience", 0);}
		set { PlayerPrefs.SetInt("experience", value);}
	}

	public float gold 
	{ 
		get { return PlayerPrefs.GetFloat("gold", 1);}
		set { PlayerPrefs.SetFloat("gold", value);}
	}

	public int charIdx
	{
		get { return PlayerPrefs.GetInt("charIdx", 0);}
		set { PlayerPrefs.SetInt("charIdx", value);}
	}

	public int combats 
	{ 
		get { return PlayerPrefs.GetInt("combats", 0);}
		set { PlayerPrefs.SetInt("combats", value);}
	}

	public bool wasFacebookLogin
	{
		get { return PlayerPrefs.GetInt("wasFacebookLogin", 0)==1;}
		set { PlayerPrefs.SetInt("wasFacebookLogin", value?1:0);}
	}

	public bool wasVKLogin
	{
		get { return PlayerPrefs.GetInt("wasVKLogin", 0)==1;}
		set { PlayerPrefs.SetInt("wasVKLogin", value?1:0);}
	}

	public int tutorials
	{
		get { return PlayerPrefs.GetInt("tutorials", 0);}
		set { PlayerPrefs.SetInt("tutorials", value);}
	}

	public string currentKnife
	{
		get{ return PlayerPrefs.GetString("currentKnife", "Заточка");}
		set{ PlayerPrefs.SetString("currentKnife", value); }
	}

	public void AddKnife(string knifeName)
	{
		string list = PlayerPrefs.GetString("knives", "Заточка");
		list += "|"+knifeName;
		PlayerPrefs.SetString("knives", list);
	}

	public List<string> knives
	{
		get{ return new List<string>(PlayerPrefs.GetString("knives", "Заточка").Split('|')); }
		set{ 
			PlayerPrefs.SetString("knives", string.Join("|", value.ToArray()));
		}
	}

	public bool IsLocationUnlocked(int idx)
	{
		string str = PlayerPrefs.GetString("unlockedLocations", "0");
		return str.Contains (idx.ToString()); 
	}

	public void UnlockLocation(int idx)
	{
		if(!IsLocationUnlocked(idx))
		{
			string str = PlayerPrefs.GetString("unlockedLocations", "0");
			str += "|"+idx.ToString();
		}
	}

	private string timeFormat = "yyyy/MM/dd HH:mm:ss";

	public DateTime lastNotificationTime
	{
		get{ return DateTime.ParseExact(PlayerPrefs.GetString("lastNotificationDateTime", DateTime.MinValue.ToString(timeFormat)), timeFormat, null);}
		set{ PlayerPrefs.SetString("lastNotificationDateTime", value.ToString(timeFormat)); }
	}

	public DateTime lastSpinTime
	{
		
		get{return DateTime.ParseExact(PlayerPrefs.GetString("lastSpinDateTime", DateTime.MinValue.ToString(timeFormat)), timeFormat, null);}
		set{ PlayerPrefs.SetString("lastSpinDateTime", value.ToString(timeFormat)); }
	}
}
