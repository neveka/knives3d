﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class StoneData 
{
	public string name;
	public string modelName;
	public int totalTurns;
	public float radius;
}
