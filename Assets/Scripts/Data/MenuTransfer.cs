﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuTransfer : MonoBehaviour 
{
	public bool wasVictory;
	public CombatTransfer.Mode mode;
	public string reason;
	public int locationIdx;

	public static void Create(bool victory, CombatTransfer.Mode mode, string reason, int locationIdx)
	{
		MenuTransfer trans = new GameObject("MenuTransfer").AddComponent<MenuTransfer>();
		trans.wasVictory = victory;
		trans.mode = mode;
		trans.reason = reason;
		trans.locationIdx = locationIdx;
		DontDestroyOnLoad(trans);
	}

	public void Process(ProgressData progressData, List<int> levels, List<KnifeData> knifeDatas, List<LocationData> locations, List<LocationData> championShip, string profileName)
	{
		LocationData locationData = mode == CombatTransfer.Mode.championship?championShip[locationIdx]:locations[locationIdx];
		progressData.combats++;
		float reward = 0;
		float rewardForLevel = 0;
		int exp = 0;
		ProfileData profileData = new ProfileData(){experience = progressData.experience};
		profileData.CalcLevel(levels);

		if(/*mode != CombatTransfer.Mode.training &&*/ wasVictory)
		{
			if(mode == CombatTransfer.Mode.championship)
			{
				progressData.pathPlace = Mathf.Max(locationIdx+1, progressData.pathPlace);
			}
			reward += locationData.stake*2;
			progressData.money += reward;
		}
		KnifeData knifeData = null;
		//if(mode != CombatTransfer.Mode.training)
		{
			exp += wasVictory?locationData.winExperience:locationData.loseExperience;
			int oldLevel = profileData.level;
			progressData.experience += exp;
			profileData.experience = progressData.experience;
			profileData.CalcLevel(levels);
			if(oldLevel != profileData.level)
			{
				knifeData = knifeDatas.Find(k=>k.unlockLevel == profileData.level);
				rewardForLevel+=1000;
				progressData.money += rewardForLevel;
			}
		}

		if(progressData.tutorials == 0/* && wasVictory*/)
		{
			progressData.tutorials = 1;
			Messager.SendStatistic(profileName+" прошел туториал");
#if UNITY_WEBPLAYER
			WebLogin webLogin = FindObjectOfType<WebLogin>();
			webLogin.OnTutorialComplete();
#else
			ADCAdManager adcolony = FindObjectOfType<ADCAdManager>();
			adcolony.ShowVideoAdByZoneKey(1);
#endif
		}
		Messager.SendStatistic(profileName+" "+reason+" в "+locationData.name);
		ResultScreen screen = GameScreen.CreateScreen<ResultScreen>();
		if(wasVictory)
			screen.ShowVictory(reward, rewardForLevel, exp, progressData, profileData.level, reason, knifeData, ()=>{
#if UNITY_WEBPLAYER
				WebLogin webLogin = FindObjectOfType<WebLogin>();
				webLogin.OnCombatWin();
#else
				FacebookLogin flogin = FindObjectOfType<FacebookLogin>();
				flogin.OnCombatWin();
				VKLogin vklogin = FindObjectOfType<VKLogin>();
				vklogin.OnCombatWin();
#endif
			});
		else
			screen.ShowLost(reward, rewardForLevel, exp, progressData, profileData.level, reason, knifeData, ()=>{});
		Destroy(gameObject);
	}
}
