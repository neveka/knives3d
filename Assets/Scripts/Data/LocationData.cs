﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class LocationData
{
	public string name;
	public int players;
	public float stake;
	public float minArea;
	public float throwTime;
	public float dashTime;
	public Vector3[] windInterval;
	public string sceneName;
	public string photoName;
	public string soilType;
	public int winExperience;
	public int loseExperience;
}
