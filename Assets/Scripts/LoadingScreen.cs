﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
	
public class LoadingScreen : GameScreen 
{
	public Transform upper;
	public Transform lower;
	public float speed = 30;
	public void SetText(string text)
	{
		transform.transform.GetComponentInChildren<Text>().text = text;
	}

	public void Open()
	{
		StartCoroutine(OpenCoroutine());
	}

	IEnumerator OpenCoroutine()
	{
		float time = 2;
		while(time>0)
		{
			time-=Time.deltaTime;
			upper.position+=Time.deltaTime*speed*Vector3.up*Screen.height;
			lower.position-=Time.deltaTime*speed*Vector3.up*Screen.height;
			yield return null;
		}
		Destroy(gameObject);
	}

	public void Show(Action onClose)
	{
		_onClose = onClose;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}
}
