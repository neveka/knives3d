﻿using UnityEngine;
using System.Collections.Generic;
using Photon;
using System;
using Facebook.Unity;

[RequireComponent(typeof(PhotonView))]
public class PhotonLobby : Photon.MonoBehaviour
{
	public static PhotonLobby GetOrCreate(List<string> locations)
	{
		PhotonLobby lobby = FindObjectOfType<PhotonLobby>();
		GameObject.DontDestroyOnLoad(lobby.gameObject);
		/*if(lobby == null)
		{
			PhotonLobby lobbyInstance = Resources.Load("PhotonLobby", typeof(PhotonLobby)) as PhotonLobby;
			lobby = Instantiate(lobbyInstance);
			GameObject.DontDestroyOnLoad(lobby.gameObject);
			lobby.GetComponent<PhotonView>().viewID = 101;
		}*/
		return lobby;
	}

	public void Init(MenuConfig menuConfig, MenuState menuState, ProfileControll profileControll)
	{
		onAllPlayersAvailable = menuState.StartCurrentLocationOnConnect;
		onVacantRoomPlace = menuState.ForceConnectToNetGame;	
		getMode = ()=>menuState.currentMode;
		getLocationIndex = ()=>menuState.currentLocation;
		getLocationName = ()=>menuConfig.locations[menuState.currentLocation].name;
		//getRoomName = ()=>ProgressData.GetId();	
		getProfileName = ()=>profileControll.profileName.text;
		getProfileSprite = ()=>profileControll.profilePicture.sprite;
		getMaxPlayers = ()=>2;//locations[_currentLocation].players;
		Reconnect();
	}

	public string GetStatus()
	{
		return _status;
	}

	public bool IsConnected()
	{
		return PhotonNetwork.connectionState == ConnectionState.Connected;
	}

	public bool IsDisConnected()
	{
		return PhotonNetwork.connectionState == ConnectionState.Disconnected;
	}

	public void CreateNewRoom()
	{
		RoomOptions ro = new RoomOptions();
		ro.maxPlayers = (byte)getMaxPlayers();
		ro.isVisible = true;
		ro.isOpen = true;
		ro.customRoomPropertiesForLobby =  new string[] { "location", "mode", "version" };
		ro.customRoomProperties = new ExitGames.Client.Photon.Hashtable() { { "location", getLocationIndex() }, { "mode", getMode().ToString() }, {"version", Application.version} };
		_status = getProfileName()+" зовет друзей";
		Messager.SendStatistic(getProfileName()+" хочет играть в "+getLocationName());
		connectionId = 0;
		if(!PhotonNetwork.CreateRoom("", ro, _lobby))
		{
			_status = "Не могу создать комнату";//+PhotonNetwork.connectedAndReady+" "+PhotonNetwork.isMasterClient;
		}
	}

	public void JoinExistigRoom(RoomInfo roomInfo)
	{		
		_status = getProfileName()+" присоединяется к друзьям";
		connectionId = roomInfo.playerCount;
		if(PhotonNetwork.JoinRoom(roomInfo.name))
			;
	}
	
	public void ConnectToPhoton()
	{
		PhotonNetwork.ConnectUsingSettings("v0.1");
		_status = "Присоединение к сети";
		//_startConnectTime = DateTime.Now;
	}

	void OnConnectedToMaster()
	{
		//Debug.Log ("onConnectedToMaster");
		_lobby = new TypedLobby("MyLobby", LobbyType.Default); //?
		PhotonNetwork.JoinLobby(_lobby);
		_status = "Присоединение к сети";
	}

	void OnJoinedLobby()
	{
		_canCreateRooms = true;
	}

	public bool CanCreateRooms()
	{
		return _canCreateRooms;
	}

	int[][] _fakePlayers = new int[8][];
	void InitFakePlayers()
	{
		for(int i = 0; i<8; i++)
		{
			_fakePlayers[i] = new int[4];
			if(i>4)
			{
				_fakePlayers[i][0] = 0;
				_fakePlayers[i][1] = 0;
				_fakePlayers[i][2] = 0;
				_fakePlayers[i][3] = 0;
			}
			else
			{
				int coef = 8-i;
				_fakePlayers[i][0] = UnityEngine.Random.Range(coef, coef*3)/2;
				_fakePlayers[i][1] = UnityEngine.Random.Range(coef, coef*3);
				_fakePlayers[i][2] = UnityEngine.Random.Range(coef, coef*3)/2;
				_fakePlayers[i][3] = UnityEngine.Random.Range(coef, coef*3);
			}
		}
	}

	public int GetPlayersForLocation(int location, CombatTransfer.Mode mode, bool already)
	{
		int counter = 0;
		RoomInfo[] roomInfos = PhotonNetwork.GetRoomList();
		if(roomInfos != null)
		{
			foreach(RoomInfo r in roomInfos)
			{
				if(IsSame(r, mode, location) && already && r.playerCount==r.maxPlayers)
				{
					counter += r.playerCount;
				}
				if(IsSame(r, mode, location) && !already && r.playerCount<r.maxPlayers)
				{
					counter += r.playerCount;
				}
			}
		}

		return counter+_fakePlayers[location][mode == CombatTransfer.Mode.netDash?(already?3:2):(already?1:0)];
	}

	void OnReceivedRoomListUpdate()//void OnJoinedLobby()
	{

	}

	bool IsSame(RoomInfo r, CombatTransfer.Mode mode = CombatTransfer.Mode.none, int location = -1)
	{
		return r.customProperties!=null && 
				r.customProperties.ContainsKey("location") && 
				(int)r.customProperties["location"]==(location == -1?getLocationIndex():location) && 
				r.customProperties.ContainsKey("mode") && 
				(CombatTransfer.Mode)Enum.Parse(typeof(CombatTransfer.Mode), (string)r.customProperties["mode"])==(mode == CombatTransfer.Mode.none?getMode():mode ) &&
				r.customProperties.ContainsKey("version") && 
				(string)r.customProperties["version"] == Application.version;
	}
	
	public void StartNetGame()
	{
		if(IsDisConnected())
			Reconnect ();
		_status = "Пока никто не играет";
		RoomInfo[] roomInfos = PhotonNetwork.GetRoomList();
		if(roomInfos != null)
		{
			RoomInfo sameLocationRoomInfo = Array.Find(roomInfos, r=>IsSame(r) && r.playerCount<r.maxPlayers);
			if(sameLocationRoomInfo != null)
			{
				_status = "Есть играющие";
				JoinExistigRoom(sameLocationRoomInfo);
			}
			else
			{
				CreateNewRoom();
			}
		}
	}

	public void LeaveNetGame()
	{
		if(PhotonNetwork.room!=null && PhotonNetwork.room.playerCount==1)
			PhotonNetwork.LeaveRoom();
		_loadedLevelsCount = 0;
		connectionId = -1;
	}

	void OnPhotonCreateRoomFailed()
	{
		_status = "Не удалось создать комнату";
		StartNetGame();
	}

	void OnPhotonJoinRoomFailed()
	{
		_status = "Не удалось присоединиться к комнате";

		//StartNetGame();
		GameObject.FindObjectOfType<ConnectingScreen>().Close();
	}

	void OnJoinedRoom()
	{
		_status = "Присоединился к игре";
		photonView.RPC("NewPlayerJoinedRoom", PhotonTargets.All);
	}

	void OnDisconnectedFromPhoton()
	{
	}

	void OnConnectionFail(DisconnectCause cause)
	{
		_status = "Не удалось присоединиться: "+cause;
	}

	public void Reconnect()
	{
		if(IsConnected())
			ForceDisconnect();
		ConnectToPhoton();
		InitFakePlayers();
	}

	public virtual void ForceDisconnect()
	{
		PhotonNetwork.Disconnect();
	}
	
	void OnLevelWasLoaded(int level)
	{
		if(level == 0 || level == 9 || PhotonNetwork.room == null)
			return;
		Debug.Log("PhotonNetwork.connectionState="+PhotonNetwork.connectionState);
		photonView.RPC("LevelLoadedRemote", PhotonTargets.All);
	}

	[PunRPC]
	void LevelLoadedRemote()
	{
		_loadedLevelsCount++;
		if (_loadedLevelsCount >= getMaxPlayers())
		{
			FindObjectOfType<ScenaryCreator>().AwakeAfterLoadAllLevels();//?
			Destroy(gameObject);
		}
	}
	
	[PunRPC]
	void NewPlayerJoinedRoom()
	//void OnPhotonPlayerConnected(PhotonPlayer newPlayer) 
	{
		_status =  getProfileName()+ " присоединился к игре";
		if (PhotonNetwork.isMasterClient)
		{
			Debug.Log("NewPlayerJoinedRoom "+PhotonNetwork.room.playerCount+"\\"+PhotonNetwork.room.maxPlayers);
			if (PhotonNetwork.room.playerCount >= PhotonNetwork.room.maxPlayers) 
			{
				//PhotonNetwork.room.visible = false;
				photonView.RPC("AllPlayersAvailable", PhotonTargets.All);
			} 
			else
			{
				photonView.RPC("NewPlayersAvailable", PhotonTargets.All);
			}
		}

	}

	/*[PunRPC]
	void SomePlayerDisconnectedFromPhoton()
	{
		if (_status.Contains ("собрал")) 
		{
			_status = "Один из игроков вышел из игры";
		}
	}*/

	[PunRPC]
	void NewPlayersAvailable() 
	{
		_status = getProfileName()+" собрал "+PhotonNetwork.room.playerCount+" из "+PhotonNetwork.room.maxPlayers+" игроков";//use Room.playerCount instead?
	}

	[PunRPC]
	void AllPlayersAvailable()
	{
		_status = "Сейчас поиграем!";
		onAllPlayersAvailable();
		Messager.SendStatistic(getProfileName()+" присоединился в "+getLocationName());
	}

	void Update()
	{
		/*if ((DateTime.Now - _startConnectTime).Minutes > timeoutMinutes) 
		{
			//ForceDisconnect();
			_status = "TimeOut error";
		}*/
		/*if(IsDisConnected())
		{
			if(FindObjectOfType<ConnectingScreen>())
				FindObjectOfType<ConnectingScreen>().Close();
			ConnectToPhoton();
		}*/
		if(!IsConnected())
			return;
		RoomInfo[] roomInfos = PhotonNetwork.GetRoomList();
		if(roomInfos != null)
		{
			foreach(RoomInfo r in roomInfos)
			{
				if(r.name != _lastRoomName && r.playerCount<r.maxPlayers && r.customProperties!= null && 
				   r.customProperties.ContainsKey("location") && 
				   r.customProperties.ContainsKey("mode") &&
				   r.customProperties.ContainsKey("version"))
				{
					ADCAdManager adcolony = FindObjectOfType<ADCAdManager>();
					if(FindObjectOfType<GameScreen>() == null && (adcolony == null||!adcolony.IsShowing()))
					{
						onVacantRoomPlace((int)r.customProperties["location"], 
						                  (CombatTransfer.Mode)Enum.Parse(typeof(CombatTransfer.Mode), (string)r.customProperties["mode"]));
						_lastRoomName = r.name;
					}
				}
			}
		}
		//Debug.Log(getLocationName());
		if(Input.GetKey(KeyCode.N) && FindObjectOfType<GameScreen>() == null)
		{
			onVacantRoomPlace(5, CombatTransfer.Mode.dash);
		}
	}

	/*private List<int> _playingPlayersCounters = new List<int>();
	private List<int> _waitingPlayersCounters = new List<int>();
	private List<string> _locations = new List<string>();*/
	private TypedLobby _lobby;
	private bool _canCreateRooms;
	private string _lastRoomName;

	//private int _playersCount;//use Room.playerCount instead?
	private int _loadedLevelsCount;
	//private DateTime _startConnectTime;
	//private int timeoutMinutes = 5;
	public int connectionId = -1;

	private string _status;

	public Action<int, CombatTransfer.Mode> onVacantRoomPlace;
	public Action onAllPlayersAvailable;
	//public Func<string> getRoomName;
	public Func<string> getProfileName;
	public Func<Sprite> getProfileSprite;
	public Func<int> getLocationIndex;
	public Func<CombatTransfer.Mode> getMode;
	public Func<string> getLocationName;
	public Func<int> getMaxPlayers;
}