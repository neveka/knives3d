﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CombatView
{
	SandDrawing _drawing;
	HUD _hud;
	PlayerTimer _timer;

	public void StartInit()
	{
		_drawing = GameObject.FindObjectOfType<SandDrawing>();
		_hud = GameObject.FindObjectOfType<HUD>();
		_timer = GameObject.FindObjectOfType<PlayerTimer>();
	}

	public void Init(List<Player> players, int humanIndex, float throwTime, Action<string> onSay, bool dash)
	{
		GameObject.FindObjectOfType<LoadingScreen>().Open();
		_hud.SelectGroup(HUD.Groups.None);
		_hud.onEsc = ()=>CompleteCombat(false, "Ты сдался");
		_timer.onTimeChanged = _hud.SetTimer;
		_timer.onStoped = _hud.ClearTimer;
		_timer.time = throwTime;
		_hud.TuneChat(onSay);

		players[humanIndex].MapButtons(_hud, dash);

		_hud.currentPlayerText.text = players[humanIndex].data.profileName;
		_hud.SetPlayerImage(players[humanIndex], true);

		_hud.enemyPlayerText.text = players[1-humanIndex].data.profileName;
		_hud.SetPlayerImage(players[1-humanIndex], false);
	}

	
	public void SetFraze(string str)
	{
		_hud.SetFraze(str);	
	}
	
	public void OnLandChanged(Player player, bool updateOutline)
	{
		_drawing.DrawPoly(player.land.GetVertexes(), player.idx, player.data.color);
		if(updateOutline)
		{
			_drawing.HideOutline();
			_drawing.DrawOutline(player.land.GetVertexes(), player.knife.GetPullDistance(), false);
		}
		float area = player.land.GetArea();
		if(area==0)
			player.text.text = "Проиграл";//:"S="+((int)(area*100)).ToString();
	}

	
	public void SelectPlayer(Player player, bool human)
	{
		_hud.SetWind(player.knifeThrow.windDir);
		_hud.SetThrowResult(PlayerKnifeThrow.Result.none);
		_hud.SetStones(player.stonePositioning);
		_hud.SelectGroup(human?HUD.Groups.None:HUD.Groups.Enemy);
		if(player.stonePositioning.onNextTurn!=null)
			player.stonePositioning.onNextTurn();
	}
	
	public void SetThrowResult(PlayerKnifeThrow.Result throwResult, Vector3[] line, Player player, bool human, bool tutorial)
	{
		if(line != null)
			_drawing.AddCutLine(line, throwResult == PlayerKnifeThrow.Result.success);
		if(throwResult == PlayerKnifeThrow.Result.far && !tutorial)
			_drawing.DrawOutline(player.land.GetVertexes(), player.knife.GetPullDistance(), true);//?
		_hud.SetThrowResult(throwResult);
		_hud.SelectGroup(human?HUD.Groups.None:HUD.Groups.Enemy);
	}

	public void HideThrowResult()
	{
		_hud.throwResultText.text = "";
	}

	public void SetThrowResult(PlayerScores scores)
	{
		_hud.throwResultText.text = scores.GetResult();
	}

	public void SetScores(int index, PlayerScores scores, bool human)
	{
		if(human)
			_hud.SetThrowStrength(0, 0, true);
		if(scores.GetDiff() == 0)
			return;
		Vector2 screenPos = Camera.main.WorldToScreenPoint(scores.GetHitPos());
		screenPos.x/=Screen.width;
		screenPos.y/=Screen.height;
		_hud.SetScores(index, scores.GetDiff(), scores.GetScores(), screenPos);
	}

	public IEnumerator BeginDash(List<Player> players, int humanIndex, Action<int> onFinish, Action onTimeOut)
	{
		_hud.SelectGroup(HUD.Groups.Dash);
		_timer.StartCountDown();
		_timer.onTimeOut = onTimeOut;
		for(int i=0; i<players.Count; i++)
		{
			players[i].scores.enabled = true;
			float p = players[i].knife.data.control/100f;
			players[i].scores.turboSpeed = 3*(1-p)+1.5f*(p);
			players[i].scores.enemyScores = players[1-i].scores;
		}
		players[humanIndex].scores.Init(_hud);
		int counter = 5;
		while(counter>=0)
		{
			_hud.SetCountDown(counter);
			yield return new WaitForSeconds(0.8f);
			counter--;
		}
		_hud.SetCountDown(-1);
		for(int i=0; i<players.Count; i++)
		{
			int k=i;
			onFinish(k);
		}
		_hud.SetThrowStrength(0, 0, true);
	}

	public void BeginChoosePosAndDir(bool human, Action onTimeOut)
	{
		_timer.StartCountDown();
		_timer.onTimeOut = onTimeOut;
		_hud.SelectGroup(human?HUD.Groups.Human:HUD.Groups.Enemy);
	}
	
	public bool BeginThrow(bool human)
	{
		_timer.Stop();
		_hud.SelectGroup(human?HUD.Groups.None:HUD.Groups.Enemy);
		bool isTimeOut = !_timer.IsTimeOut();
		_timer.onTimeOut = null;
		return isTimeOut;
	}

	public static void ForceCompleteCombat()
	{
		if(GameObject.FindObjectOfType<PhotonLobby>())
			GameObject.DestroyImmediate(GameObject.FindObjectOfType<PhotonLobby>().gameObject);//?
		Application.LoadLevel("menu");
	}

	public void CompleteCombat(bool victory, string reason)
	{
		MenuTransfer.Create(victory, CombatTransfer.GetOrCreateDefault().mode, reason, CombatTransfer.GetOrCreateDefault().locationIdx);
		ForceCompleteCombat();
	}

	public void CheckDash(List<Player> players, int humanIndex)
	{
		if(players[humanIndex].scores.GetScores()>players[1-humanIndex].scores.GetScores())
		{
			CompleteCombat(true, "Набрал больше очков");
		}
		else
		{
			CompleteCombat(false, "Противник набрал больше очков");
		}
	}

	public bool CheckCombat(List<Player> players, int humanIndex, ref int currentPlayer, int harmedIndex, bool tutorial)
	{
		OnLandChanged(players[harmedIndex], false);
		OnLandChanged(players[currentPlayer], tutorial);
		if(players[harmedIndex].land.GetArea() == 0)
		{
			Player oldPlayer = players[currentPlayer];
			players.Remove(players[harmedIndex]);
			currentPlayer = players.IndexOf(oldPlayer);
			if(harmedIndex == humanIndex)
			{
				CompleteCombat(false, "Потерял всю землю");
				return true;
			}
			else if(players.Count == 1)
			{
				CompleteCombat(true, "Захватил всю землю");
				return true;
			}
		}
		return false;
	}
}