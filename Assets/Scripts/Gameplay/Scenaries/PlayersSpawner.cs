﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersSpawner
{
	public static float radius = 1.5f;
	public static float playersRadius = 2f;
	//public static int humanIndex = 0;

	public Stone Spawn(StoneData stoneData)
	{
		Stone stone = GameObject.Instantiate(Resources.Load<GameObject>(stoneData.modelName)).AddComponent<Stone>();
		stone.data = stoneData;
		return stone;
	}

	public Player Spawn(PlayerData playerData)
	{
		Debug.LogWarning(playerData.modelName);
		Player player = GameObject.Instantiate(Resources.Load<Player>(playerData.modelName));
		player.Init (playerData);
		return player;
	}

	public GameObject SpawnEffect(Transform parent, string effectName)
	{
		GameObject effectGO = GameObject.Instantiate(Resources.Load<GameObject>(effectName));
		effectGO.transform.parent = parent;
		effectGO.transform.localRotation = Quaternion.identity;
		effectGO.transform.localPosition = Vector3.forward/4f;
		return effectGO;
	}

	public Player Spawn(PlayerData playerData, KnifeData knifeData, int idx, int count, bool isDash, int locationIdx)
	{
		Player player = Spawn(playerData);

		player.knife = GameObject.Instantiate(Resources.Load<GameObject>(knifeData.modelName)).AddComponent<Knife>();
		player.knife.Init (player, knifeData, isDash, locationIdx, SpawnEffect);

		player.InitForCombat (idx, count, isDash);
		return player;
	}	

	public void LookAtHuman(List<Player> players, int humanIndex, bool dash)
	{
		Debug.Log("humanIndex = "+humanIndex);
		for(int i=0; i<players.Count; i++)
		{
			players[i].dashCamera.gameObject.SetActive(dash && i == humanIndex);
			players[i].childCamera.gameObject.SetActive(!dash && i == humanIndex);
			players[i].text.transform.parent.GetComponent<LookAt>().target = dash?players[humanIndex].dashCamera.transform:players[humanIndex].childCamera.transform;
			players[i].GetComponentInChildren<Animator>().enabled = true;
		}
	}
}
