using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(PhotonView))]
public class NetScenary : Photon.MonoBehaviour 
{
	int _currentPlayer = 0;
	List<Player> _players;
	CombatView _view = new CombatView();
	CombatTransfer _transfer;

	void Awake () 
	{
		_transfer = CombatTransfer.GetOrCreateDefault();
		IPlayersLoader loader = (IPlayersLoader)gameObject.AddComponent<NetPlayersLoader>();
		loader.Load(_transfer, OnLoad);
		_view.StartInit();
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer disconnectedPlayer)
	{
		if(PhotonNetwork.player.ID != disconnectedPlayer.ID && (_players==null||_players.Count == 2))//?
		{
			Debug.LogWarning("PhotonPlayerDisconnected "+disconnectedPlayer);
			_view.CompleteCombat(true, "Противник сдался");
		}
	}

	void OnDestroy()
	{
		if(_transfer)
			Destroy(_transfer.gameObject);//?
		PhotonNetwork.Disconnect();
	}

	void OnLoad(List<Player> players)
	{
		_players = players;
		_players[_transfer.humanIndex].movement.rotationChanged = PlayerRotate;
		_players[_transfer.humanIndex].movement.positionChanged = PlayerStep;
		_players[_transfer.humanIndex].stonePositioning.onMakeStonePositioning = PlayerMakeStonePositioning;

		for(int i=0; i<players.Count; i++)
			_view.OnLandChanged(players[i], false);
		
		_view.SelectPlayer(_players[_currentPlayer], true);
		_view.Init (_players, _transfer.humanIndex, _transfer.throwTime, OnSay, false);
		PrepairThrow();
	}

	void OnSay(string fraze)
	{
		photonView.RPC("OnSayRemote", PhotonTargets.All, fraze, _transfer.humanIndex);
	}

	[PunRPC]
	void OnSayRemote(string fraze, int speaker)
	{
		_players[speaker].text.text = fraze;
		_players[speaker].text.transform.parent.GetComponent<UnityEngine.UI.RawImage>().color = 
			(string.IsNullOrEmpty(fraze)?0:0.6f)*Color.white;
	}

	public void PlayerRotate(Quaternion rotation)
	{
		photonView.RPC("PlayerRotateRemote", PhotonTargets.Others, rotation, _transfer.humanIndex);//?
	}
	
	public void PlayerStep(Vector3 pos, bool back)
	{
		photonView.RPC("PlayerStepRemote", PhotonTargets.Others, pos, back);//?
	}

	public void PlayerMakeStonePositioning(Vector3 pos, string stoneName)
	{
		photonView.RPC("PlayerMakeStonePositioningRemote", PhotonTargets.Others, pos, stoneName);//?
	}

	public void PrepairThrow() 
	{
		Vector3 pos = Utils.FindCenterInside(_players[_currentPlayer].land.GetVertexes());
		_players[_currentPlayer].movement.GoTo(pos, BeginChoosePosAndDir);
	}
	
	void BeginChoosePosAndDir()
	{
		if(_currentPlayer == _transfer.humanIndex)
		{	
			photonView.RPC("BeginChoosePosAndDirRemote", PhotonTargets.All);
			_players[_currentPlayer].knifeThrow.ChoosePosAndDir(_currentPlayer == _transfer.humanIndex, ()=>{
				if(_view.BeginThrow(true))
				{
					photonView.RPC("MakeThrowRemote", PhotonTargets.All, _currentPlayer, _players[_currentPlayer].knifeThrow.throwPoint,
					               _players[_currentPlayer].knifeThrow.throwDir, _players[_currentPlayer].knifeThrow.throwOffset);
				}
			});
		}
	}

	void OnThrowFinish()
	{
		if(_currentPlayer == _transfer.humanIndex)
		{
			int harmedIndex = -1;
			Vector3[] line = null;
			PlayerKnifeThrow.Result throwResult = _players[_currentPlayer].knifeThrow.GetResult(_players, _currentPlayer, _transfer.minArea, out harmedIndex, out line);
			photonView.RPC("OnThrowFinishRemote", PhotonTargets.All, throwResult, harmedIndex, 
			               harmedIndex==-1?"":Utils.VertexesToString(_players[harmedIndex].land.GetVertexes()), 
			               Utils.VertexesToString(_players[_currentPlayer].land.GetVertexes()), line);
			photonView.RPC("SetWindRemote", PhotonTargets.All, _transfer.GetRandomWindDir());
		}
	}
	
	[PunRPC]
	void MakeThrowRemote(int idx, Vector3 throwPoint, Vector3 throwDir, float offset)
	{
		_players[_currentPlayer].knifeThrow.throwPoint = throwPoint;
		_players[_currentPlayer].knifeThrow.throwDir = throwDir;
		_players[_currentPlayer].knifeThrow.throwOffset = offset;
		_players[idx].knifeThrow.MakeThrow(OnThrowFinish);
	}

	[PunRPC]
	void OnThrowFinishRemote(PlayerKnifeThrow.Result throwResult, int harmedIndex, 
	                         string harmedPlayerVertexes, string currentPlayerVertexes, Vector3[] line)
	{
		StartCoroutine(ThrowFinishCoroutine(throwResult, harmedIndex, harmedPlayerVertexes, currentPlayerVertexes, line));
	}
	
	IEnumerator ThrowFinishCoroutine(PlayerKnifeThrow.Result throwResult, int harmedIndex, 
	                                 string harmedPlayerVertexes, string currentPlayerVertexes, Vector3[] line)
	{
		_view.SetThrowResult(throwResult, line, _players[_currentPlayer], _currentPlayer == _transfer.humanIndex, _transfer.tutorialStep != Tutorial.Step.none);
		//yield return new WaitForSeconds(1);
		while(_players[_currentPlayer].knifeThrow.KnifeFlies())//?
			yield return null;
		if(throwResult == PlayerKnifeThrow.Result.success)
		{
			_players[harmedIndex].land.SetVertexes(Utils.StringToVertexes(harmedPlayerVertexes));
			_players[_currentPlayer].land.SetVertexes(Utils.StringToVertexes(currentPlayerVertexes));
			if(!_view.CheckCombat(_players, _transfer.humanIndex, ref _currentPlayer, harmedIndex, false))
				BeginChoosePosAndDir();
			yield break;
		}
		Vector3 pos = _players[_currentPlayer].movement.FindOutPosition(_players);
		_players[_currentPlayer].movement.GoTo(pos, ()=>{		
			_currentPlayer++;
			_currentPlayer %= _players.Count;
			_view.SelectPlayer(_players[_currentPlayer], _currentPlayer == _transfer.humanIndex);
			PrepairThrow();
		});
	}

	[PunRPC]
	void SetWindRemote(Vector3 windDir)
	{
		_players[_currentPlayer].knifeThrow.windDir = windDir;
	}

	[PunRPC]
	public void PlayerRotateRemote(Quaternion rotation, int idx)
	{
		_players[idx].movement.SmoothRot(rotation);
	}

	[PunRPC]
	public void PlayerStepRemote(Vector3 pos, bool back)
	{
		_players[_currentPlayer].movement.SmoothPos(pos, back);
	}

	[PunRPC]
	public void PlayerMakeStonePositioningRemote(Vector3 pos, string stoneName)
	{
		_players[_currentPlayer].stonePositioning.MakeStonePositioning(pos, stoneName);//?
	}

	[PunRPC]
	public void BeginChoosePosAndDirRemote()
	{
		_view.BeginChoosePosAndDir(_currentPlayer == _transfer.humanIndex, OnThrowFinish);
	}
}
