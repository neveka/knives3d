﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

interface IPlayersLoader
{
	void Load (CombatTransfer transfer, Action<List<Player>> onFinish);
}

public class PlayersLoader : MonoBehaviour, IPlayersLoader 
{
	public void Load (CombatTransfer transfer, Action<List<Player>> onFinish) 
	{
		StartCoroutine(LoadPlayersCoroutine(transfer, onFinish));
	}

	IEnumerator LoadPlayersCoroutine(CombatTransfer transfer, Action<List<Player>> onFinish)
	{
		PlayersSpawner spawner = new PlayersSpawner();
		yield return null;
		List<Player> players = new List<Player>();
		for(int i=0; i<transfer.players.Count; i++)
		{
			players.Add(spawner.Spawn(transfer.GetPlayerData(i), transfer.GetKnifeData(i), i, transfer.count, transfer.mode == CombatTransfer.Mode.dash, transfer.locationIdx));
			yield return null;
		}
		spawner.LookAtHuman(players, transfer.humanIndex, 
		                    transfer.mode == CombatTransfer.Mode.dash||transfer.mode == CombatTransfer.Mode.netDash);//?
		yield return null;
		onFinish(players);
	}


}
