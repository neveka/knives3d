﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class Scenary : MonoBehaviour 
{
	int _currentPlayer = 0;
	List<Player> _players;
	CombatView _view = new CombatView();
	CombatTransfer _transfer;
	ChatBot _chatBot;

	void Start () 
	{
		_transfer = CombatTransfer.GetOrCreateDefault();
		_chatBot = FindObjectOfType<ChatBot>();
		_chatBot.enabled = true;
		IPlayersLoader loader = (IPlayersLoader)gameObject.AddComponent<PlayersLoader>();
		loader.Load(_transfer, OnLoad);
		_view.StartInit();
	}

	void OnDestroy()
	{
		if(_transfer)
			Destroy(_transfer.gameObject);//?
	}

	void OnLoad(List<Player> players)
	{
		_players = players;
		for(int i=0; i<players.Count; i++)
			_view.OnLandChanged(players[i], _transfer.tutorialStep != Tutorial.Step.none && i==_transfer.humanIndex);

		_players[_currentPlayer].knifeThrow.windDir = _transfer.GetRandomWindDir();
		_view.SelectPlayer(_players[_currentPlayer], true);
		_view.Init (_players, _transfer.humanIndex, _transfer.throwTime, OnSay, false);
		PrepairThrow();
	}

	void OnSay(string fraze)
	{
		OnSayRemote(fraze, _transfer.humanIndex);
		if(_chatBot.enabled)
			StartCoroutine(_chatBot.Ask(fraze, (string answer)=>{OnSayRemote(answer, 1-_transfer.humanIndex);} ));
	}

	void OnSayRemote(string fraze, int speaker)
	{
		_players[speaker].text.text = fraze;
		_players[speaker].text.transform.parent.GetComponent<UnityEngine.UI.RawImage>().color = 
			(string.IsNullOrEmpty(fraze)?0:0.6f)*Color.white;
	}

	public void PrepairThrow() 
	{
		Vector3 pos = Utils.FindCenterInside(_players[_currentPlayer].land.GetVertexes());
		_players[_currentPlayer].movement.GoTo(pos, BeginChoosePosAndDir);
	}

	void BeginChoosePosAndDir()
	{
		_view.BeginChoosePosAndDir(_currentPlayer == _transfer.humanIndex, OnThrowFinish);
		if(_transfer.tutorialStep != Tutorial.Step.none && _transfer.tutorialStep != Tutorial.Step.ok)
		{	
			StartCoroutine(new Tutorial().StartCoroutine(_players[_currentPlayer], _transfer.tutorialStep, 
			                                             (Tutorial.Step step)=>{_transfer.tutorialStep = step; BeginChoosePosAndDir();}));
			return;
		}
		if(_currentPlayer != _transfer.humanIndex)
			_players[_currentPlayer].stonePositioning.MakeAIStonePositioning();
		_players[_currentPlayer].knifeThrow.ChoosePosAndDir(_currentPlayer == _transfer.humanIndex, ()=>{
			if(_view.BeginThrow(_currentPlayer == _transfer.humanIndex))
			{
				//Debug.LogWarning("MakeThrow");
				_players[_currentPlayer].knifeThrow.MakeThrow(OnThrowFinish);
			}
		});
	}

	void OnThrowFinish()
	{

		StartCoroutine(ThrowFinishCoroutine());
	}

	IEnumerator ThrowFinishCoroutine()
	{
		//Debug.LogWarning("ThrowFinishCoroutine");
		int harmedIndex = -1;
		Vector3[] line = null;
		PlayerKnifeThrow.Result throwResult = _players[_currentPlayer].knifeThrow.GetResult(_players, _currentPlayer, _transfer.minArea, out harmedIndex, out line);
		_view.SetThrowResult(throwResult, line, _players[_currentPlayer], _currentPlayer == _transfer.humanIndex, _transfer.tutorialStep != Tutorial.Step.none);
		//yield return new WaitForSeconds(1);//?
		while(_players[_currentPlayer].knifeThrow.KnifeFlies())//?
			yield return null;
		Debug.LogWarning("throwResult="+throwResult);
		if(throwResult == PlayerKnifeThrow.Result.success)
		{
			if(!_view.CheckCombat(_players, _transfer.humanIndex, ref _currentPlayer, harmedIndex, _transfer.tutorialStep != Tutorial.Step.none))
				BeginChoosePosAndDir();
			yield break;
		}
		else if(_transfer.tutorialStep != Tutorial.Step.none)
		{
			_transfer.tutorialStep = Tutorial.Step.throwFailed + (int)throwResult;
			BeginChoosePosAndDir();
			yield break;
		}
		Vector3 pos = _players[_currentPlayer].movement.FindOutPosition(_players);
		_players[_currentPlayer].movement.GoTo(pos, ()=>{		
			_currentPlayer++;
			_currentPlayer %= _players.Count;
			//Debug.LogWarning("_currentPlayer="+_currentPlayer);
			_players[_currentPlayer].knifeThrow.windDir = _transfer.GetRandomWindDir();
			_view.SelectPlayer(_players[_currentPlayer], _currentPlayer == _transfer.humanIndex);
			PrepairThrow();
		});
	}

	void OnGUI()
	{
		if(_transfer.cheatCounter<7)
			return;
		GUIStyle style = new GUIStyle("button");
		style.fontSize = Screen.height/30;
		GUILayout.Space(Screen.height/4f);
		GUILayoutOption[] options = new GUILayoutOption[] {GUILayout.Height(Screen.height/12f), GUILayout.Width(Screen.height/5f)};
		if(GUILayout.Button("win", style, options))
			_view.CompleteCombat(true, "Ты применил чит");
		if(GUILayout.Button("loose", style, options))
			_view.CompleteCombat(false, "Ты применил чит");
	}
}
