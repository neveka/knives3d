﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(PhotonView))]
public class NetDashScenary : Photon.MonoBehaviour 
{
	List<Player> _players;
	CombatView _view = new CombatView();
	CombatTransfer _transfer;
	
	void Awake () 
	{
		_transfer = CombatTransfer.GetOrCreateDefault();
		IPlayersLoader loader = (IPlayersLoader)gameObject.AddComponent<NetPlayersLoader>();
		loader.Load(_transfer, OnLoad);
		_view.StartInit();
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer disconnectedPlayer)
	{
		if(PhotonNetwork.player.ID != disconnectedPlayer.ID && (_players==null||_players.Count == 2))//?
		{
			Debug.LogWarning("PhotonPlayerDisconnected "+disconnectedPlayer);
			_view.CompleteCombat(true, "Противник сдался");
		}
	}
	
	void OnDestroy()
	{
		if(_transfer)
			Destroy(_transfer.gameObject);//?
		PhotonNetwork.Disconnect();
	}

	void OnLoad(List<Player> players)
	{
		_players = players;
		//_players[_transfer.humanIndex].movement.rotationChanged = PlayerRotate;
		//_players[_transfer.humanIndex].movement.positionChanged = PlayerStep;
		_view.Init (_players, _transfer.humanIndex, _transfer.throwTime, null, false);
		StartCoroutine(_view.BeginDash(_players, _transfer.humanIndex, BeginChoosePosAndDir, ()=>_view.CheckDash(_players, _transfer.humanIndex)));
		//players[1-_transfer.humanIndex].scores.checksResults = false;
		players[_transfer.humanIndex].scores.onAddScores = OnAddScores;
		players[_transfer.humanIndex].scores.onAddKiss = OnAddKiss;
		players[_transfer.humanIndex].scores.onAddHearts = OnAddHearts;
		players[_transfer.humanIndex].scores.onCalcTargetTransform = OnCalcTargetTransform;

	}

	void BeginChoosePosAndDir(int index)
	{
		if(index == _transfer.humanIndex)
		{
			bool human = index == _transfer.humanIndex;
			_players[index].scores.CalcAllTargets(human);
			_players[index].knifeDash.ChooseOnlyStrength(human, ()=>{
				_players[index].scores.CalcTargetTransform(_players[index].knifeDash.GetStrength());

				photonView.RPC("MakeThrowRemote", PhotonTargets.All, index, _players[index].knifeDash.throwPoint);
			});
		};
	}

	void OnAddScores(int idx, Vector3 hitPos)
	{
		photonView.RPC("AddScoresRemote", PhotonTargets.Others, idx, hitPos);
	}

	void OnAddKiss(string parentName, Vector3 pos, Vector3 dir)
	{
		photonView.RPC("AddKissRemote", PhotonTargets.Others, parentName, pos, dir);
	}

	void OnAddHearts(string parentName, Vector3 pos, Vector3 dir)
	{
		photonView.RPC("AddHeartsRemote", PhotonTargets.Others, parentName, pos, dir);
	}

	void OnCalcTargetTransform(string transformName, Vector3 center)
	{
		photonView.RPC("CalcTargetTransformRemote", PhotonTargets.Others, transformName, center);
	}

	[PunRPC]
	void AddScoresRemote(int idx, Vector3 hitPos)
	{
		_players[1-_transfer.humanIndex].scores.AddScores(idx, hitPos);
		_view.SetScores(1, _players[1-_transfer.humanIndex].scores, false);
	}

	[PunRPC]
	void AddKissRemote(string parentName, Vector3 pos, Vector3 dir)
	{
		Collider [] colliders = _players[_transfer.humanIndex].GetComponentsInChildren<Collider>();
		_players[1-_transfer.humanIndex].scores.AddKiss(Array.Find(colliders, c=>c.name == parentName).transform, pos, dir);
	}

	[PunRPC]
	void AddHeartsRemote(string parentName, Vector3 pos, Vector3 dir)
	{
		Collider [] colliders = _players[_transfer.humanIndex].GetComponentsInChildren<Collider>();
		_players[1-_transfer.humanIndex].scores.AddHearts(Array.Find(colliders, c=>c.name == parentName).transform, pos, dir);
	}

	[PunRPC]
	void CalcTargetTransformRemote(string targetName, Vector3 center)
	{
		Collider [] colliders = _players[_transfer.humanIndex].GetComponentsInChildren<Collider>();
		_players[1-_transfer.humanIndex].scores.SetTargetTransform(Array.Find(colliders, c=>c.name == targetName).transform, center);
	}

	[PunRPC]
	void MakeThrowRemote(int idx, Vector3 throwPoint)
	{
		_players[idx].knifeDash.throwPoint = throwPoint;

		_players[idx].knifeDash.MakeThrow(
			()=>StartCoroutine(ThrowFinishCoroutine(idx)),
			()=>NeedFinish(idx));
	}
	
	bool NeedFinish(int index)
	{
		bool result = false;
		if(index == _transfer.humanIndex)
		{
			result = _players[index].scores.FinishDash(_players[index].knife.transform);
			_view.SetThrowResult(_players[index].scores);
		}
		else
		{
			result = _players[index].scores.FinishDashLite(_players[index].knife.transform);//????
		}
		return result;
	}
	
	IEnumerator ThrowFinishCoroutine(int index)
	{
		if(index == _transfer.humanIndex)
		{
			while(_players[index].knifeDash.KnifeFlies())//?
				yield return null;
			_view.SetScores(0, _players[index].scores, true);
			_view.HideThrowResult();
			BeginChoosePosAndDir(index);
		}
	}
}
