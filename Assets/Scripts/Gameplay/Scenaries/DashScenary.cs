﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DashScenary : MonoBehaviour 
{
	List<Player> _players;
	CombatView _view = new CombatView();
	CombatTransfer _transfer;

	void Start () 
	{
		_transfer = CombatTransfer.GetOrCreateDefault();
		IPlayersLoader loader = (IPlayersLoader)gameObject.AddComponent<PlayersLoader>();
		loader.Load(_transfer, OnLoad);
		_view.StartInit();
	}
	
	void OnDestroy()
	{
		if(_transfer)
			Destroy(_transfer.gameObject);//?
	}
	
	void OnLoad(List<Player> players)
	{
		_players = players;
		_view.Init (_players, _transfer.humanIndex, _transfer.throwTime, null, true);
		StartCoroutine(_view.BeginDash(_players, _transfer.humanIndex, BeginChoosePosAndDir, ()=>_view.CheckDash(_players, _transfer.humanIndex)));
	}

	void BeginChoosePosAndDir(int index)
	{
		bool human = index == _transfer.humanIndex;
		_players[index].scores.CalcAllTargets(human);
		_players[index].knifeDash.ChooseOnlyStrength(human, ()=>{
			_players[index].scores.CalcTargetTransform(_players[index].knifeDash.GetStrength());
			_players[index].knifeDash.MakeThrow(
					()=>StartCoroutine(ThrowFinishCoroutine(index)),
					()=>NeedFinish(index));
		});
	}

	bool NeedFinish(int index)
	{
		bool result = _players[index].scores.FinishDash(_players[index].knife.transform);
		if(index == _transfer.humanIndex)
			_view.SetThrowResult(_players[index].scores);
		return result;
	}
	
	IEnumerator ThrowFinishCoroutine(int index)
	{
		while(_players[index].knifeDash.KnifeFlies())//?
			yield return null;
		_view.SetScores(index, _players[index].scores, index == _transfer.humanIndex);
		if(index == _transfer.humanIndex)
			_view.HideThrowResult();
		else
			yield return new WaitForSeconds( UnityEngine.Random.Range(0.25f, 5f));
		BeginChoosePosAndDir(index);
	}
}
