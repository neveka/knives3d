﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(PhotonView))]
public class NetPlayersLoader : Photon.MonoBehaviour, IPlayersLoader
{
	PlayersSpawner _spawner = new PlayersSpawner();
	CombatTransfer _transfer;
	LoadingScreen _screen;

	void Awake()
	{
		_screen = FindObjectOfType<LoadingScreen>();
	}

	[PunRPC]
	void PatchOtherPlayerDatasRemote(int prototypeId, 
	                                 int otherPlayerConnectionId, 
	                                 string otherPlayerName, 
	                                 string otherPlayerSpriteStr, 
	                                 string otherPlayerProfileString)
	{
		try
		{
			_transfer.PatchOtherPlayerDatas(prototypeId, otherPlayerConnectionId, otherPlayerName, Utils.StringToSprite(otherPlayerSpriteStr), otherPlayerProfileString);
		}
		catch(Exception ex)
		{
			_screen.SetText("Ошибка загрузки: "+ex.ToString());
		}
	}

	int _createdLoaders = 0;
	[PunRPC]
	protected void NotifyLoaderCreatedRemote()
	{
		_createdLoaders++;
	}

	[PunRPC]
	protected void AllLoadersCreatedRemote()
	{
		_createdLoaders = _transfer.count;
	}

	[PunRPC]
	protected void LoadPlayerRemote(string playerName, string knifeName, int idx, int count/*, int viewId*/)
	{
		/*Player player = */_spawner.Spawn(_transfer.playerDatas.Find(pd=>pd.name == playerName), 
			_transfer.knifeDatas.Find(pd=>pd.name == knifeName), idx, count, _transfer.mode != CombatTransfer.Mode.dash, _transfer.locationIdx);
		//PhotonView view = player.gameObject.AddComponent<PhotonView>();
		//view.viewID = viewId;
	}

	public void Load (CombatTransfer transfer, Action<List<Player>> onFinish) 
	{
		StartCoroutine(LoadPlayersCoroutine(transfer, onFinish));
	}
	
	IEnumerator LoadPlayersCoroutine(CombatTransfer transfer, Action<List<Player>> onFinish)
	{
		_transfer = transfer;
		_screen.SetText("Загрузка сцены");

		photonView.RPC("NotifyLoaderCreatedRemote", PhotonTargets.All);

		while(_createdLoaders<_transfer.count)
		{
			yield return null;
		}
		if(PhotonNetwork.isMasterClient)
			photonView.RPC("AllLoadersCreatedRemote", PhotonTargets.All);

		/*while(FindObjectOfType<PhotonLobby>())
		{
			yield return null;
		}*/

		int oldDatas = _transfer.playerDatas.Count-1;
		PlayerData playerData = _transfer.playerDatas[_transfer.playerDatas.Count-1];
		photonView.RPC("PatchOtherPlayerDatasRemote", 
		               PhotonTargets.Others, 

		               playerData.prototypeId, 
		               _transfer.humanIndex,
			           playerData.profileName, 
		               Utils.SpriteToString(playerData.profileSprite), 
		               playerData.profileData.GetString() );
		_screen.SetText("Загрузка данных");
		while(_transfer.playerDatas.Count-oldDatas<_transfer.count)
		{
			//Debug.LogWarning("_transfer.playerDatas.Count-oldDatas<_transfer.count");
			yield return null;
		}
		
		for(int i=0; i<transfer.players.Count; i++)
		{
			photonView.RPC("LoadPlayerRemote", PhotonTargets.All, transfer.players[i], transfer.knives[i], transfer.humanIndex, transfer.count/*, view.viewID*/);
			yield return null;
		}
		_screen.SetText("Загрузка игроков");
		List<Player> players = new List<Player>(GameObject.FindObjectsOfType<Player>());
		while(players.Count<transfer.count)
		{
			//Debug.LogWarning("players.Count<transfer.count");
			players = new List<Player>(GameObject.FindObjectsOfType<Player>());
			yield return null;
		}
		players.Sort((x,y)=>{return x.idx>y.idx?1:(x.idx==y.idx?0:-1);});
		_spawner.LookAtHuman(players, transfer.humanIndex, 
		                     transfer.mode == CombatTransfer.Mode.dash||transfer.mode == CombatTransfer.Mode.netDash);//?
		yield return null;
		onFinish(players);
	}
}
