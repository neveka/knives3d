﻿using UnityEngine;
using System.Collections;

public class ScenaryCreator : MonoBehaviour 
{
	CombatTransfer _transfer;
	// Use this for initialization
	void Awake () 
	{
		GameScreen.CreateScreen<LoadingScreen>().Show(()=>Application.LoadLevel("menu"));
		_transfer = CombatTransfer.GetOrCreateDefault();
		if(_transfer.mode == CombatTransfer.Mode.dash)
			gameObject.AddComponent<DashScenary>();
		else if(_transfer.mode == CombatTransfer.Mode.training||_transfer.mode == CombatTransfer.Mode.championship)
			gameObject.AddComponent<Scenary>();
	}

	public void AwakeAfterLoadAllLevels()
	{
		if(_transfer.mode == CombatTransfer.Mode.net)
		{
			if (PhotonNetwork.isMasterClient)
				PhotonNetwork.Instantiate("NetScenary", Vector3.zero, Quaternion.identity, 0);
		}
		if(_transfer.mode == CombatTransfer.Mode.netDash)
		{
			if (PhotonNetwork.isMasterClient)
				PhotonNetwork.Instantiate("NetDashScenary", Vector3.zero, Quaternion.identity, 0);
		}
	}
}
