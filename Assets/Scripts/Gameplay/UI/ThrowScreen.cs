﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ThrowScreen : GameScreen 
{
	public CurrencyControll desc;
	public void Show (Action onClose) 
	{
		_onClose = onClose;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
#if UNITY_WEBPLAYER
		desc.SetText("Проведи мышкой вверх по экрану, чтобы бросить нож. Следи за шкалой слева и когда она заполнится до черной засечки, отпусти кнопку мыши.");
#endif
	}
}
