﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ScoresControll : MonoBehaviour 
{
	public Text main;
	public Text countdown;
	public Text[] flyers = new Text[2];
	public Image turboFiller;
	public RectTransform[] icons;
	public Func<float> getTurboPart;
	public Func<bool> isTurboMode;
	public RectTransform[] turboIcons;

	void Awake()
	{
		flyers[0].gameObject.SetActive(false);
		flyers[1].gameObject.SetActive(false);
		countdown.gameObject.SetActive(false);
	}

	public IEnumerator ScoresFly(int index, int diff, int scores, Vector2 screenPart)
	{
		flyers[index].gameObject.SetActive(true);
		flyers[index].text = diff.ToString();
		RectTransform flyer = flyers[index].GetComponent<RectTransform>();
		flyer.anchorMax = screenPart;
		flyer.anchorMin = screenPart;
		flyer.anchoredPosition = Vector2.zero;
		Vector2 targetPos = new Vector2(index == 0? 0.47f:0.53f, 1);
		float time = 1;
		float p = 0;
		while(time>0)
		{
			p = 1-time;
			flyer.anchorMax = screenPart*(1-p)+targetPos*p;
			flyer.anchorMin = screenPart*(1-p)+targetPos*p;
			flyer.anchoredPosition = Vector2.zero;
			time-=Time.deltaTime;
			yield return null;
		}			
		flyers[index].gameObject.SetActive(false);
		String[] strs = main.text.Split(':');
		strs[index] = scores.ToString();
		main.text = string.Join(":", strs);
	}

	public void SetCountDown(int counter)
	{
		countdown.text = counter.ToString();
		countdown.transform.localScale = Vector3.one;
		countdown.gameObject.SetActive(counter>=0);
		StopAllCoroutines();
		StartCoroutine(CountDownScale());
	}

	IEnumerator CountDownScale()
	{
		float scale = 1;
		while(scale<5)
		{
			scale+=Time.deltaTime*2;
			countdown.transform.localScale = Vector3.one*scale;
			yield return null;
		}
	}

	void Update()
	{
		if(getTurboPart != null)
		{
			turboFiller.fillAmount = getTurboPart();
		}
		if(isTurboMode != null)
		{
			bool show = !isTurboMode() || ((int)(Time.time*5))%2==0;
			Array.ForEach(turboIcons, i=>i.gameObject.SetActive(show));
		}
	}
}
