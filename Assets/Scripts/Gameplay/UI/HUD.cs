using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HUD : MonoBehaviour 
{
	public enum Groups
	{
		Human,
		Enemy,
		Wind,
		Timer,
		Throw,
		List,
		Dash,
		Scores,
		Number,
		None
	}

	public enum Buttons
	{
		Back,
		Forward,
		Left,
		Right,
		EnemyLeft,
		EnemyRight,
		Throw,
		Exit,
		Send,
		Mail,
		Number
	}

	public Text currentPlayerText;
	public Image currentPlayerImage;

	public Text enemyPlayerText;
	public Image enemyPlayerImage;

	public Text throwResultText;
	public Image compass;
	public Action onEsc;
	public bool highlightLeftAndRightArrow;
	public bool highlightUpAndDownArrow;
	public bool showTurns;

	List<GameObject> groups = new List<GameObject>();
	List<Button> buttons = new List<Button>();
	Player player;
	Sprite _guestSprite;

	// Use this for initialization
	void Start () 
	{
		_guestSprite = currentPlayerImage.sprite;
		for(int i=0; i<(int)Groups.Number; i++)
		{
			GameObject go = GameObject.Find(((Groups)i).ToString()+"Group");
			groups.Add(go);
		}
		for(int i=0; i<(int)Buttons.Number; i++)
		{
			GameObject go = GameObject.Find(((Buttons)i).ToString()+"Button");
			buttons.Add(go?go.GetComponent<Button>():null);
		}
		GetButton(Buttons.Exit).GetComponent<PointerListener>().onPressedUp = ()=>Application.LoadLevel("menu");
		GetButton(Buttons.Send).gameObject.SetActive(false);
		ClearTimer();
		HideThrowStrength();
	}

	public void SetThrowResult(PlayerKnifeThrow.Result result)
	{
		switch(result)
		{
		case PlayerKnifeThrow.Result.none:
			throwResultText.text = "";
			break;
		case PlayerKnifeThrow.Result.cannotAddLand:
			throwResultText.text = "нельзя присоединить";
			break;
		case PlayerKnifeThrow.Result.notTrusted:
			throwResultText.text = "не воткнулся";
			break;
		case PlayerKnifeThrow.Result.outofLand:
			throwResultText.text = "не попал в землю врага";
			break;
		case PlayerKnifeThrow.Result.timeOut:
			throwResultText.text = "не успел";
			break;
		case PlayerKnifeThrow.Result.far:
			throwResultText.text = "не дотянулся провести границу";
			break;
		case PlayerKnifeThrow.Result.stoneCollided:
			throwResultText.text = "нож попал в камень";
			break;
		case PlayerKnifeThrow.Result.success:
			throwResultText.text = "успешно";
			break;
		}
	}

	public void ShowChat()
	{
		GetButton(Buttons.Send).gameObject.SetActive(!GetButton(Buttons.Send).gameObject.activeSelf);
	}

	public void SetPlayerImage(Player player, bool human)
	{
		if(player.data.profileSprite==null)
			player.data.profileSprite = _guestSprite;//
		if(human)
		{
			currentPlayerImage.GetComponent<PointerListener>().onPressedDown = () => GameScreen.CreateScreen<PlayerScreen>().Init(player);
			currentPlayerImage.sprite = player.data.profileSprite;
		}
		else
		{
			enemyPlayerImage.GetComponent<PointerListener>().onPressedDown = () => GameScreen.CreateScreen<PlayerScreen>().Init(player);
			enemyPlayerImage.sprite = player.data.profileSprite;
		}
	}//

	public Button GetButton(Buttons button)
	{
		return buttons[(int)button];
	}
	

	public void SelectGroup(Groups group) 
	{
		PointerEventData e = new PointerEventData(FindObjectOfType<EventSystem>());
		buttons[(int)Buttons.Left].GetComponent<PointerListener>().OnPointerUp(e);
		buttons[(int)Buttons.Right].GetComponent<PointerListener>().OnPointerUp(e);
		buttons[(int)Buttons.EnemyLeft].GetComponent<PointerListener>().OnPointerUp(e);
		buttons[(int)Buttons.EnemyRight].GetComponent<PointerListener>().OnPointerUp(e);
		buttons[(int)Buttons.Forward].GetComponent<PointerListener>().OnPointerUp(e);
		buttons[(int)Buttons.Back].GetComponent<PointerListener>().OnPointerUp(e);
		groups[(int)Groups.Human].SetActive(Groups.Human==group);
		groups[(int)Groups.Enemy].SetActive(Groups.Enemy==group);
		GetComponent<BusinessListCanvas>().SetInteractive(Groups.Human==group);
		GetComponent<SwipeControll>().enabled = Groups.Human==group||Groups.Dash==group;
		groups[(int)Groups.Scores].SetActive(Groups.Dash==group);
	}

	public void SetGetTurboPart(Func<float> getTurboPart, Func<bool> isTurboMode)
	{
		groups[(int)Groups.Scores].GetComponent<ScoresControll>().getTurboPart = getTurboPart;
		groups[(int)Groups.Scores].GetComponent<ScoresControll>().isTurboMode = isTurboMode;
	}

	public void SetScores(int index, int diff, int scores, Vector2 screenPart)
	{
		StartCoroutine(groups[(int)Groups.Scores].GetComponent<ScoresControll>().ScoresFly(index, diff, scores, screenPart));
	}

	public void SetCountDown(int counter)
	{
		groups[(int)Groups.Scores].GetComponent<ScoresControll>().SetCountDown(counter);
	}
	
	public void ClearTimer()
	{
		groups[(int)Groups.Timer].GetComponentInChildren<Text>().text = "";
	}

	public void SetStones(PlayerStonePositioning stonePositioning)
	{
		BusinessListCanvas businesList = GetComponent<BusinessListCanvas>();
		businesList.SetStones(stonePositioning.stoneDatas);
		stonePositioning.onFinish = ()=>{ 
			GetComponent<SwipeControll>().enableSwipes = true;
			businesList.SetStones(stonePositioning.stoneDatas);
		};
		businesList.onSelect = (object item)=>{
			if(item is StoneData)
			{
				GetComponent<SwipeControll>().enableSwipes = false;
				stonePositioning.SetCurrentStoneName((item as StoneData).name);
			}
		};
		businesList.onDeselect = (object item)=>{
			if(item is StoneData)
			{
				GetComponent<SwipeControll>().enableSwipes = true;
				stonePositioning.SetCurrentStoneName(null);
			}
		};
	}

	public void SetTimer(float time)
	{
		int secs = (int)time;
		int milisecs = (int)((time-(int)time)*100);
		groups[(int)Groups.Timer].GetComponentInChildren<Text>().text = 
			(secs<10?"0":"")+secs+":"+(milisecs<10?"0":"")+milisecs;
	}

	public void SetWind(Vector3 dir)
	{
		groups[(int)Groups.Wind].SetActive(dir != Vector3.zero);
		if(dir != Vector3.zero)
		{
			groups[(int)Groups.Wind].GetComponentInChildren<Text>().text = "ветер: "+(int)dir.magnitude;
			compass.transform.GetChild(0).transform.localRotation = Quaternion.AngleAxis(Mathf.Atan(dir.x/dir.z), Vector3.up);
		}
	}

	public void SetThrowStrength(float part, int turns, bool dash)
	{
		groups[(int)Groups.Throw].SetActive(true);
		//Debug.Log("show filler");
		groups[(int)Groups.Throw].transform.GetChild(1).gameObject.SetActive(dash);
		groups[(int)Groups.Throw].transform.GetChild(2).GetComponent<Image>().fillAmount = part;
		if(showTurns)
		{
			throwResultText.text = turns==-1?"":(turns+" оборот");
			if(turns == 2||turns == 3)
				throwResultText.text += "а";
		}
	}

	public void HideThrowStrength()
	{
		//groups[(int)Groups.Throw].GetComponentInChildren<Image>();
		groups[(int)Groups.Throw].SetActive(false);
		//Debug.Log("hide filler");
	}

	public void TuneChat(Action<string> onSay)
	{
		//GetButton(Buttons.Send).gameObject.SetActive(onSay!=null);
		GetButton(Buttons.Mail).gameObject.SetActive(onSay!=null);
		GetButton(Buttons.Mail).GetComponent<PointerListener>().onPressedUp = ()=>{
			if(GetButton(Buttons.Send).gameObject.activeSelf)
				onSay("");
			ShowChat();
		};
		if(onSay!=null)
		{
			GetButton(Buttons.Send).GetComponent<PointerListener>().onPressedUp = ()=>{
				onSay(GetComponentInChildren<InputField>().text);
				GetComponentInChildren<InputField>().text = "";
			};
		}
	}

	public void SetFraze(string str)
	{
		GetComponentInChildren<InputField>().text = str;
	}

	void Update()
	{
		/*if(Camera.main)
		{
			//if(!player)
				player = Camera.main.GetComponentInParent<Player>();
			compass.transform.localRotation = 
				Quaternion.AngleAxis(60, Vector3.right)*
				Quaternion.AngleAxis(player.transform.rotation.eulerAngles.y, Vector3.forward);
		}*/
		if (Input.GetKeyDown(KeyCode.Escape) && FindObjectOfType<GameScreen>() == null) 
		{
			onEsc();
		}
		float scale = 1+Mathf.Sin(Time.time*10)*0.1f;
		if(highlightLeftAndRightArrow)
		{
			buttons[(int)Buttons.Left].transform.localScale = Vector3.one*scale;
			buttons[(int)Buttons.Right].transform.localScale = Vector3.one*scale;
		}
		if(highlightUpAndDownArrow)
		{
			buttons[(int)Buttons.Forward].transform.localScale = Vector3.one*scale;
			buttons[(int)Buttons.Back].transform.localScale = Vector3.one*scale;
		}
	}

	public void ResetArrows()
	{
		buttons[(int)Buttons.Left].transform.localScale = Vector3.one;
		buttons[(int)Buttons.Right].transform.localScale = Vector3.one;
	}

}
