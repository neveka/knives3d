using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LocalizedText: Text
{
	string localeKey = string.Empty;
	// Use this for initialization
	protected override void Start( )
	{
		localeKey = text;
		//  Debug.LogWarning( text + "->" + Localization.GetString( text ) );
		OnLocalize( );
	}
	
	public void OnLocalize( )
	{
		text = Strings.Get( localeKey );
	}
}
