﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScreen : GameScreen 
{
	public Image image;
	public Text text;
	public CurrencyControll combats;
	public ExperienceControll experience;
	public BusinessItemPanel panel;
	//public BusinessItemPanel panel2;
	public void Init (Player player) 
	{
		panel.SetKnife(player.knife.data, null, 100500, null);
		panel.RotateModel();
		panel.allwaysShow = true;

		/*panel2.SetStone(player.stone.data, null, null);
		panel2.RotateModel();
		panel2.allwaysShow = true;*/

		text.text = player.data.profileName;
		image.sprite = player.data.profileSprite;
		closeButton.onClick.RemoveAllListeners();
		combats.SetText("Сыграл битв: "+player.data.profileData.combats);
		experience.SetExperience(player.data.profileData.newExp, player.data.profileData.nextExp, player.data.profileData.level);
		closeButton.onClick.AddListener(()=>Close());
	}
}
