﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ArrowsScreen : GameScreen 
{
	public void Show (Action onClose) 
	{
		_onClose = onClose;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close ());
	}
}
