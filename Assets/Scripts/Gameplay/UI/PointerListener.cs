﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PointerListener : MonoBehaviour, IPointerUpHandler, IPointerDownHandler 
{
	public Action onPressed = null; 
	public Action onPressedDown = null;
	public Action onPressedUp = null;
	public bool interactable = true;
	bool _pressed = false;

	public void OnPointerDown(PointerEventData eventData)
	{
		_pressed = true;
		if(onPressedDown != null && interactable)
			onPressedDown();
	}
	
	public void OnPointerUp(PointerEventData eventData)
	{
		_pressed = false;
		if(onPressedUp != null && interactable)
			onPressedUp();
	}

	// Update is called once per frame
	void Update () 
	{
		if(_pressed && onPressed!=null && interactable)
			onPressed();
	}
}
