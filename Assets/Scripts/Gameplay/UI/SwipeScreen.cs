﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SwipeScreen : MonoBehaviour 
{
	public Transform root;
	public RawImage finger;
	public RawImage cursor;
	public Image filler;
	public Text hint;
	public RawImage spark;
	private Vector2 _pos;
	private float _startTime;
	//private Action _onClose;

	void Awake()
	{
		bool showFinger = true;
#if UNITY_WEBPLAYER
		showFinger = false;
#endif
		finger.gameObject.SetActive(showFinger);
		cursor.gameObject.SetActive(!showFinger);
		_pos = root.GetComponent<RectTransform>().anchoredPosition;
		FindObjectOfType<SwipeControll>().tutorialScreen = this;
		_startTime = Time.time;
		Update();
		//GetComponent<PointerListener>().onPressed = ()=>{Destroy(gameObject);_onClose();};
	}

	void Update()
	{
		float part = Mathf.Sin(Time.time-_startTime);
		if(Mathf.Cos(Time.time-_startTime)<0)
			part = -part;
		part += 1;
		part /= 2f;

		float place = 0.63f;

		hint.gameObject.SetActive(part>place);
		spark.gameObject.SetActive(part<place);

		filler.fillAmount = part>place?place:part;
		if(part>place)
		{
			part = (1-(part-place)/(1-place))*place;
		}
		_pos.y = -180*(1-part)+0*(part);
		root.GetComponent<RectTransform>().anchoredPosition = _pos;//+Vector2.right*shift*300;
	}
}
