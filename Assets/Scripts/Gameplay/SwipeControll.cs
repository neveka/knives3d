﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SwipeControll : MonoBehaviour 
{
	public enum Type
	{
		None,
		Horizontal,
		//Vertical,
		Diagonal
	};

	public GameObject swipeFlare;
	public Action<Type, Vector3, Vector3, Vector3> onSwipe = null; 
	public Action onSwipeStarted = null;
	public Action<Type, Vector3, Vector3> onSwipeFinished = null;
	public bool enableSwipes = true;
	public bool hasMaxDuration = false;
	public SwipeScreen tutorialScreen;

	Vector3 _swipeStart;
	Vector3 _lastMousePoint;
	Type _type;

	float _swipeStartTime;
	float _maxSwipeDuration = 1;
	int _lastFlareIdx;
	int _flaresCount = 20;
	float _flaresDist = 0.05f;
	int _dirIndex = 4;
	List<GameObject> _swipeFlares = new List<GameObject>();

	void Start () 
	{
		_swipeFlares.Add(swipeFlare);
		for(int i=0; i<_flaresCount-1; i++)
		{
			GameObject f = Instantiate(swipeFlare);
			f.transform.localScale = Vector3.one*(1-(float)i/_flaresCount);
			f.transform.SetParent(swipeFlare.transform.parent);
			_swipeFlares.Insert(0,f);
		}
		_swipeFlares.ForEach(f=>f.SetActive(false));
	}

	void CheckSwipeType()
	{
		if(_type == Type.Horizontal)
		{
			float offset = Screen.height/10f;//
			Vector3 from = _swipeStart;
			Vector3 to = _swipeFlares[Math.Min(_dirIndex, _lastFlareIdx)].transform.position;
			if(to.y-from.y>offset)
			{
				_type = Type.Diagonal;
				_swipeStart = Input.mousePosition;
			}
		}
	}

	void Update()
	{
		if(!enableSwipes)
			return;
		if(Input.GetMouseButtonDown(0) && (tutorialScreen ||!EventSystem.current.IsPointerOverGameObject()))
		{
			_swipeStart = Input.mousePosition;
			_type = Type.Horizontal;
			_swipeStartTime = Time.time;
			_lastFlareIdx = -1;
			if(onSwipeStarted != null)
				onSwipeStarted();
			if(tutorialScreen)
				Destroy(tutorialScreen.gameObject);
		}
		if(_type != Type.None && Input.GetMouseButton(0))
		{
			if(_lastFlareIdx == -1 || (_swipeFlares[_lastFlareIdx].transform.position-Input.mousePosition).magnitude>_flaresDist*Screen.width)
			{
				if(_lastFlareIdx<_flaresCount-1)
				{
					Vector3 pos = _lastFlareIdx == -1?Input.mousePosition:
						(_swipeFlares[_lastFlareIdx].transform.position-(_swipeFlares[_lastFlareIdx].transform.position-Input.mousePosition).normalized*_flaresDist*Screen.width);
					_lastFlareIdx++;
					_swipeFlares[_lastFlareIdx].transform.position = pos;
					_swipeFlares[_lastFlareIdx].SetActive(true);
				}
			}
			CheckSwipeType();
			if(onSwipe!=null)
				onSwipe(_type, _swipeStart, _lastMousePoint, Input.mousePosition);
		}
		if(_type != Type.None && (Input.GetMouseButtonUp(0) || _lastFlareIdx==_flaresCount || (hasMaxDuration && Time.time-_swipeStartTime>_maxSwipeDuration)))
		{
			if(onSwipeFinished!=null)
				onSwipeFinished(_type, _swipeStart, Input.mousePosition);
			_swipeFlares.ForEach(f=>f.SetActive(false));
			_type = Type.None;
		}
		_lastMousePoint = Input.mousePosition;
	}

	void OnDisable()
	{
		_swipeFlares.ForEach(f=>f.SetActive(false));
	}
}
