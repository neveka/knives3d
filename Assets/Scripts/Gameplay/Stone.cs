﻿using UnityEngine;
using System.Collections;

public class Stone : MonoBehaviour 
{
	public StoneData data;
	int _leftTurns;

	void Start()
	{
		_leftTurns = data.totalTurns;
	}

	public void OnNextTurn()
	{
		if(_leftTurns == 0)
		{
			Destroy(gameObject);
		}
		_leftTurns--;
	}
}
