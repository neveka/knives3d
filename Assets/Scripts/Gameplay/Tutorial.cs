﻿using UnityEngine;
using System.Collections;
using System;

public class Tutorial
{
	public enum Step
	{
		none = -1,
		ok = 0,
		arrowsLesson = 1,
		throwLesson = 2,
		throwFailed = 3,
		throwFailed_notTrusted,
		throwFailed_outofLand,
		throwFailed_cannotAddLand,
		throwFailed_timeOut,
		throwFailed_far,
		throwFailed_stoneCollided,
	}

	public IEnumerator StartCoroutine (Player player, Step step, Action<Step> onFinish) 
	{
		GameObject.FindObjectOfType<SwipeControll>().enableSwipes = false;
		PlayerTimer timer = GameObject.FindObjectOfType<PlayerTimer>();
		timer.enabled = false;
		bool wait = true;
		if(step == Step.arrowsLesson)
		{
			yield return new WaitForSeconds(1);

			wait = true;
			string text = "Привет, Земляк! Сейчас будет тренировка твоих навыков с ножом. Для ее завершения нужно захватить всю землю.";
			GameScreen.CreateScreen<TutorialScreen>().ShowInfo("Обучение", text, ()=>{wait = false;});
			while(wait)
				yield return null;
			yield return new WaitForSeconds(0.5f);

			bool wasRotation = false;
			bool wasStep = false;
			HUD hud = GameObject.FindObjectOfType<HUD>();
			hud.highlightLeftAndRightArrow = true;
			hud.highlightUpAndDownArrow = true;

			player.movement.rotationChanged = (Quaternion)=>{wasRotation = true; hud.highlightLeftAndRightArrow = false; hud.ResetArrows();}; 
			player.movement.positionChanged = (Vector3 pos, bool back)=>{wasStep = true; hud.highlightUpAndDownArrow = false; hud.ResetArrows();};
			GameScreen.CreateScreen<ArrowsScreen>().Show(()=>{wait = false;});
			while(wait)
				yield return null;
			while(!wasRotation||!wasStep||Input.GetMouseButton(0))
				yield return null;
			yield return new WaitForSeconds(1);
			step = Step.throwLesson;
		}
		if(step == Step.throwLesson)
		{
			wait = true;
			string text = "Белым мелом нарисована граница до которой ты можешь дотянуться со своей земли.";
			GameScreen.CreateScreen<TutorialScreen>().ShowInfo("Обучение", text, ()=>{wait = false;});
			while(wait)
				yield return null;
			yield return new WaitForSeconds(0.5f);

			wait = true;
			GameScreen.CreateScreen<ThrowScreen>().Show(()=>{wait = false;});
			while(wait)
				yield return null;

			GameScreen.CreateScreen<SwipeScreen>();

			step = Step.ok;
		}
		if(step >= Step.throwFailed)
		{
			wait = true;
			yield return new WaitForSeconds(0.5f);
			GameScreen.CreateScreen<TutorialScreen>().ShowInfo("Обучение", GetFailText(step), ()=>{wait = false;});
			while(wait)
				yield return null;
			step = Step.ok;
		}
		timer.enabled = true;
		GameObject.FindObjectOfType<SwipeControll>().enableSwipes = true;
		onFinish(step);
	}

	string GetFailText(Step step)
	{
		switch(step)
		{
		case Step.throwFailed_cannotAddLand:
			return "Земля не может быть присоединена. Старайся отрезать землю, граничущую с твоей землей";
		case Step.throwFailed_far:
			return "Ты не дотянулся провести границу. Старайся отрезать новую землю так, чтобы до нее можно было дотянуться рукой с ножом";
		case Step.throwFailed_notTrusted:
			return "Нож не воткнулся. Следи, чтобы нож делал полное кол-во оборотов по надписи на экране";
		case Step.throwFailed_outofLand:
			return "Нож не попал в землю врага. Старайся быть точнее";
		case Step.throwFailed_timeOut:
			return "Время броска вышло. Не тормози!";
		case Step.throwFailed_stoneCollided:
			return "Нож попал в камень. Старайся кидать в песок";
		}
		return "Попробуй кинуть нож еще раз";
	}
}
