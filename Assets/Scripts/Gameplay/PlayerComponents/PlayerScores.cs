﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class CollidersGroup
{
	public Collider[] colliders;
}

public class PlayerScores : MonoBehaviour 
{
	public Action<int, Vector3> onAddScores;
	public Action<string, Vector3, Vector3> onAddHearts;
	public Action<string, Vector3, Vector3> onAddKiss;
	public Action<string, Vector3> onCalcTargetTransform;
	public Action<bool> onTurboMode;

	public string[] colliderNames = {"leg", "arm", "spine", "spine2", "neck", "head", "sphere"};
	public int[] scores = {3, 3, 4, 6, 8, 10, 10};
	public List<CollidersGroup> groups = new List<CollidersGroup>();
	public string[] results = {"в ногу", "в руку", "в живот", "в грудь", "в шею", "в голову", "в яблочко"}; 
	public int scoresForTurbo = 30;
	public float turboSpeed = 1;
	public PlayerScores enemyScores;
	
	bool _turboMode;
	int _collected;
	float _turbo;
	int _diff;
	Vector3 _hitPos;
	string _result;

	void Start()
	{
		if(onTurboMode!=null)
			onTurboMode(false);
	}

	public void Hide()
	{
		groups[groups.Count-1].colliders[0].transform.parent.gameObject.SetActive(false);
	}

	public void Init(HUD hud)
	{
		hud.SetGetTurboPart(()=>{ return (float)_turbo/scoresForTurbo;}, ()=> {return _turboMode;});
	}

	public void Miss()
	{
		_turbo = 0;
		_turboMode = false;
		if(onTurboMode!=null)
			onTurboMode(false);
		_result = "промазал";
		_hitPos = Vector3.zero;
		_diff = 0;
	}

	public bool FinishDashLite(Transform knifeTransform)
	{
		knifeTransform.parent = _targetTransform;
		return true;
	}

	public bool FinishDash(Transform knifeTransform)
	{
		if(AddEffects(knifeTransform))
		{
			knifeTransform.parent = _targetTransform;
			AddScores(_targetTransform.name, knifeTransform.position);
			return true;
		}
		else
		{
			Miss();
			return false;
		}
	}
	
	public bool AddEffects(Transform knifeTransform)
	{
		Player player = enemyScores.gameObject.GetComponentInParent<Player>();
		MeshCollider playerCollider = new SkinnedCollisionHelper().AddCollisionMesh(player.mainSkin.gameObject);
		RaycastHit[] hits1 = Physics.RaycastAll(knifeTransform.position-knifeTransform.forward*2, knifeTransform.forward, 4);

		List<RaycastHit> hits = new List<RaycastHit>(hits1);
		hits.RemoveAll(h=>h.collider == playerCollider);
		if(hits.Count>0)
		{
			hits.Sort((h1, h2)=>(int)Mathf.Sign(h1.distance-h2.distance));
			if(_targetcollider != hits[0].collider)
			{
				Debug.LogWarning("Change collider "+_targetcollider+"->"+hits[0].collider);
				_targetcollider = hits[0].collider;
				_targetTransform = hits[0].collider.transform;
				Vector3 pos = _targetTransform.TransformPoint(_center);
				_center = _targetTransform.InverseTransformPoint(pos);
			}
		}
		else
		{
			Debug.LogError("no collider");
			Destroy(playerCollider);
			return false;
		}

		RaycastHit[] hits2 = Physics.RaycastAll(knifeTransform.position+knifeTransform.forward, -knifeTransform.forward, 2);
		hits = new List<RaycastHit>(hits1);
		hits.AddRange(hits2);

		foreach(RaycastHit hit in hits)
		if(hit.collider.name.Contains("Sphere"))
		{
			Destroy(playerCollider);
			return true;
		}

		bool result = false;
		foreach(RaycastHit hit in hits)
		{
			if(hit.collider == playerCollider)
			{
				Vector3 localHitPoint = _targetTransform.InverseTransformPoint(hit.point);
				Vector3 localLookPoint = _targetTransform.InverseTransformPoint(hit.point+hit.normal);
				AddHearts(_targetTransform, localHitPoint, localLookPoint);
				AddKiss(_targetTransform, localHitPoint, localLookPoint);

				if(onAddHearts!=null)
					onAddHearts(_targetTransform.name, localHitPoint, localLookPoint);
				if(onAddKiss!=null)
					onAddKiss(_targetTransform.name, localHitPoint, localLookPoint);
				result = true;
			}
		}
		Destroy(playerCollider);
		return result;
	}

	public bool AddScores(string colliderName, Vector3 hitPos) 
	{
		int idx = -1;
		for(int i=0; i<colliderNames.Length; i++)
			if(colliderName.ToLower().Contains(colliderNames[i]))
				idx = i;
		AddScores(idx, hitPos);
		if(onAddScores!=null)
			onAddScores(idx, hitPos);
		return idx != -1;
	}

	public void AddScores(int idx, Vector3 hitPos)
	{
		if(idx != -1)
		{
			if(!_turboMode)
			{
				_turbo += scores[idx];
				if(_turbo>=scoresForTurbo)
				{
					_turboMode = true;
					if(onTurboMode!=null)
						onTurboMode(true);
				}
			}
			_diff = scores[idx]*(_turboMode?2:1);
			_collected += _diff;
			_result = results[idx];
			_hitPos = hitPos;
		}
		else
		{
			_turbo = 0;
			_turboMode = false;
			if(onTurboMode!=null)
				onTurboMode(false);
			_result = "промазал";
			_hitPos = Vector3.zero;
			_diff = 0;
		}
	}

	void Update()
	{
		if(_turboMode)
		{
			_turbo -= Time.deltaTime*turboSpeed;
			if(_turbo<=0)
			{
				_turboMode = false;
				if(onTurboMode!=null)
					onTurboMode(false);
			}
		}
	}

	public Vector3 GetHitPos()
	{
		return _hitPos;
	}

	public string GetResult()
	{
		return _result;
	}

	public int GetScores()
	{
		return _collected;
	}

	public int GetDiff()
	{
		return _diff;
	}

	Transform _targetTransform;
	Vector3 _center;
	Collider _targetcollider;

	List<int> _indexes = new List<int>();
	public void CalcAllTargets(bool human)
	{
		//Debug.Log("CalcAllTargets");
		ScoresControll controll = FindObjectOfType<ScoresControll>();
		for(int i=0; i<controll.icons.Length; i++)
		{
			_indexes.Insert(UnityEngine.Random.Range(0,i+1), i);
		}
		if(human)
		for(int i=0; i<controll.icons.Length; i++)
		{
			//Debug.Log(_indexes[i]);
			Vector2 p = new Vector2(0.5f, (i+0.5f)/controll.icons.Length);
			controll.icons[_indexes[i]].anchorMax = p;
			controll.icons[_indexes[i]].anchorMin = p;
		}
	}

	public void CalcTargetTransform(float part)
	{
		int idx = Mathf.RoundToInt(part*groups.Count-0.5f);

		if(idx>=groups.Count)
			idx = groups.Count-1;
		//Debug.LogWarning("CalcTargetTransform "+idx+" "+_indexes[idx]);
		idx = _indexes[idx];
		int r = UnityEngine.Random.Range(0, groups[idx].colliders.Length);
		//Debug.Log(part+" "+(part*groups.Count-0.5f)+" "+groups[idx].colliders[0]);
		_targetTransform = enemyScores.groups[idx].colliders[r].transform;//verticalTargetPosition-transform.forward*0.2f+Vector3.up*(throwOffset-_minThrowOffset);
		_targetcollider = _targetTransform.GetComponent<Collider>();
		float radius = (_targetcollider as CapsuleCollider?(_targetcollider as CapsuleCollider).radius:(_targetcollider as SphereCollider).radius)*0.8f;//*(1+range);
		float height = (_targetcollider as CapsuleCollider?(_targetcollider as CapsuleCollider).height/2:0)+radius;

		Vector3 add = new Vector3(UnityEngine.Random.Range(-radius,radius),UnityEngine.Random.Range(-height,height),0);
		if(_targetcollider.name.ToLower().Contains("arm"))
		{
			float swap = add.x;
			add.x = add.y;
			add.y = swap;
		}
		_center = (_targetcollider as CapsuleCollider?(_targetcollider as CapsuleCollider).center:(_targetcollider as SphereCollider).center)+add;
		if(onCalcTargetTransform != null)
			onCalcTargetTransform(_targetTransform.name, _center);
	}

	public void SetTargetTransform(Transform trans, Vector3 center)
	{
		_targetTransform = trans;
		_targetcollider = _targetTransform.GetComponent<Collider>();
		_center = center;
	}

	public Vector3 GetTargetPoint(Vector3 startPos)
	{
		Vector3 pos = _targetTransform.TransformPoint(_center);
		RaycastHit[] hits = Physics.RaycastAll(startPos, -startPos+pos, 10);
		foreach(RaycastHit hit in hits)
		{
			if(hit.collider == _targetcollider)
				return hit.point;
		}
		return pos;
	}

	public Transform GetTarget()
	{
		return _targetTransform;
	}

	static GameObject heartsFX;
	static GameObject kissSprite;
	public void AddHearts(Transform parent, Vector3 hitPoint, Vector3 lookPoint)
	{
		if(heartsFX == null)
			heartsFX = Resources.Load<GameObject>("Hearts2");
		GameObject go = Instantiate<GameObject>(heartsFX);
		go.transform.parent = parent;
		go.transform.localPosition = hitPoint;
		go.transform.LookAt(parent.TransformPoint(lookPoint));
		StartCoroutine(hideHeartFX(go));
	}

	public void AddKiss(Transform parent, Vector3 hitPoint, Vector3 lookPoint)
	{
		//Debug.Log(hit.collider+" "+hit.normal+" "+hit.point);
		if(kissSprite == null)
			kissSprite = Resources.Load<GameObject>("Kiss");
		GameObject go = Instantiate<GameObject>(kissSprite);
		go.transform.parent = parent;
		go.transform.localPosition = hitPoint;
		go.transform.LookAt(parent.TransformPoint(lookPoint));
	}
	
	IEnumerator hideHeartFX(GameObject go)
	{
		yield return new WaitForSeconds(5);
		Destroy(go);
	}
}
