﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KnifeThrow
{
	class IntersecInfo
	{
		public Vector3 point1, point2;
		public int polyIndex;
		public int edgeIndex1, edgeIndex2;

		public static int CompareIntersecs(IntersecInfo a, IntersecInfo b, Vector3 throwPoint)
		{
			float adist = ((a.point1+a.point2)/2-throwPoint).sqrMagnitude;
			float bdist = ((b.point1+b.point2)/2-throwPoint).sqrMagnitude;
			if(adist == bdist)
				return 0;
			return adist<bdist?-1:1;
		}
	}

	List<IntersecInfo> _intersections = new List<IntersecInfo>();
	//List<int> underIndexes = new List<int>();
	//List<int> aboveIndexes = new List<int>();
	int _insideIndex = -1;
	
	public bool GetDamaged(Vector3 throwPoint, Vector3 throwDir, ref List<List<Vector3>> vertexes, out List<List<Vector3>> result1, out List<List<Vector3>> result2, out Vector3[] line)
	{
		result1 = null;
		result2 = null;
		line = null;
		FindIntersecs(throwPoint, throwDir, vertexes);
		//Debug.LogWarning(throwPoint+" "+throwDir+" "+_intersections.Count+" "+_insideIndex);
		if(_insideIndex!=-1)
		{
			Vector3 throwOut = throwPoint+throwDir*100;
			_intersections.Sort((a,b)=>IntersecInfo.CompareIntersecs(a, b, throwOut));
			line = new Vector3[2];

			DebugIntersecs();
			result1 = new List<List<Vector3>>();
			result2 = new List<List<Vector3>>();
			List<List<Vector3>> oldVertexes = new List<List<Vector3>>(vertexes);

			int idx = _intersections.IndexOf(_intersections.Find(k=>k.polyIndex==_insideIndex));
			//Debug.Log("idx="+idx);

			int i=idx;
			while(i<_intersections.Count && (i==0||_intersections[i-1].point2==_intersections[i].point1))
			{
				/*newDist = (_intersections[i].point1-playerPos).magnitude;
				if(newDist>maxDist)
					maxDist = newDist;
				newDist = (_intersections[i].point2-playerPos).magnitude;
				if(newDist>maxDist)
					maxDist = newDist;*/

				List<Vector3> half1, half2;
				DevidePoly(vertexes[_intersections[i].polyIndex], _intersections[i], out half1, out half2);
				result1.Add(half1);
				result2.Add(half2);
				oldVertexes.Remove(vertexes[_intersections[i].polyIndex]);
				i++;
			}
			line[1] = _intersections[i-1].point2;
			i=idx-1;
			while(i>=0 && (i==_intersections.Count-1||_intersections[i+1].point1==_intersections[i].point2))
			{
				/*newDist = (_intersections[i].point1-playerPos).magnitude;
				if(newDist>maxDist)
					maxDist = newDist;
				newDist = (_intersections[i].point2-playerPos).magnitude;
				if(newDist>maxDist)
					maxDist = newDist;*/

				List<Vector3> half1, half2;
				DevidePoly(vertexes[_intersections[i].polyIndex], _intersections[i], out half1, out half2);
				result1.Add(half1);
				result2.Add(half2);
				oldVertexes.Remove(vertexes[_intersections[i].polyIndex]);
				i--;
			}
			line[0] = _intersections[i+1].point1;
			/*for(int i=0; i<aboveIndexes.Count; i++)
			{
				List<Vector3> poly = vertexes[aboveIndexes[i]];
				result1.Add(poly);
			}
			for(int i=0; i<underIndexes.Count; i++)
			{
				List<Vector3> poly = vertexes[underIndexes[i]];
				result2.Add(poly);
			}*/
			AddNeighors(result1, oldVertexes);
			AddNeighors(result2, oldVertexes);
			return true;
		}
		return false;
	}

	void AddNeighors(List<List<Vector3>> result, List<List<Vector3>> vertexes)
	{
		bool found = true;
		while(found)
		{
			found = false;
			for(int i=0; i<result.Count; i++)
			{
				for(int j=0; j<vertexes.Count; j++)
				{
					if(Utils.AreNeighors(result[i], vertexes[j]))
					{
						result.Add(vertexes[j]);
						vertexes.RemoveAt(j);
						found = true;
					}
				}
			}
		}
	}

	void DevidePoly(List<Vector3> currentPoly, IntersecInfo currentIntersec, out List<Vector3> half1, out List<Vector3> half2)
	{
		half1 = new List<Vector3>();
		half2 = new List<Vector3>();
		//Debug.LogWarning("find "+currentIntersec.j1+" "+currentIntersec.j2+" "+currentIntersec.point1+" "+currentIntersec.point2);
		
		half1.Add(currentIntersec.point2);
		//Debug.Log(currentIntersec.j2);
		half1.Add(currentIntersec.point1);
		//Debug.Log(currentIntersec.j1);
		for(int i=currentIntersec.edgeIndex1+1; i<=currentIntersec.edgeIndex2+(currentIntersec.edgeIndex2>currentIntersec.edgeIndex1?0:currentPoly.Count); i++)
		{
			//Debug.Log(i%currentPoly.Count);
			half1.Add(currentPoly[i%currentPoly.Count]);
		}
		//Debug.Log("---------");
		half2.Add(currentIntersec.point1);
		//Debug.Log(currentIntersec.j1);
		half2.Add(currentIntersec.point2);
		//Debug.Log(currentIntersec.j2);
		for(int i=currentIntersec.edgeIndex2+1; i<=currentIntersec.edgeIndex1+(currentIntersec.edgeIndex1>currentIntersec.edgeIndex2?0:currentPoly.Count); i++)
		{
			//Debug.Log(i%currentPoly.Count);
			half2.Add(currentPoly[i%currentPoly.Count]);
		}
		//Debug.LogWarning(currentPoly.Count+"=>"+half1.Count+" & "+half2.Count);
	}

	void FindIntersecs(Vector3 throwPoint, Vector3 throwDir, List<List<Vector3>> vertexes)
	{
		throwPoint.y = 0.005f;
		Vector3 throwOut1 = throwPoint+throwDir*100;
		Vector3 throwOut2 = throwPoint-throwDir*100;
		Vector3 intersecPoint = Vector3.zero;
		for(int i=0; i<vertexes.Count; i++)
		{
			int counter = 0;
			//bool above = true;
			//bool under = true;
			for(int j=0; j<vertexes[i].Count; j++)
			{
				float res = Utils.IsIntersection(throwPoint, throwOut1, vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], ref intersecPoint);
				//Debug.Log("res1="+res+" i="+i);
				if(res == 0)
				{
					counter++;
					AddIntersec(i, j, intersecPoint, throwDir);
					//above = false;
					//under = false;
				}
				/*else 
				{
					if(res < 0)
						above = false;
					else
						under = false;
				}*/
			
				res = Utils.IsIntersection(throwPoint, throwOut2, vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], ref intersecPoint);
				//Debug.Log("res2="+res+" i="+i);
				if(res == 0)
				{
					AddIntersec(i, j, intersecPoint, throwDir);
					//above = false;
					//under = false;
				}
				/*else
				{
					if(res > 0)
						above = false;
					else
						under = false;
				}*/
			}
			if(counter == 1)
				_insideIndex = i;
			//else
			//	Debug.LogWarning("counter="+counter+" "+throwPoint+" "+throwDir);
			/*if(under)
				underIndexes.Add(i);
			if(above)
				aboveIndexes.Add(i);*/
		}
	}

	void AddIntersec(int newPolyIndex, int newEdgeIndex, Vector3 intersecPoint, Vector3 sortDir)
	{
		if(_intersections.Count==0 || _intersections[_intersections.Count-1].polyIndex != newPolyIndex)
		{
			_intersections.Add(new IntersecInfo(){point1 = intersecPoint, polyIndex=newPolyIndex, edgeIndex1=newEdgeIndex});
		}
		else
		{
			IntersecInfo lastIntersec = _intersections[_intersections.Count-1];
			//Debug.Log("second intersec "+intersecPoint);
			if(intersecPoint.x*sortDir.x+intersecPoint.z*sortDir.z<lastIntersec.point1.x*sortDir.x+lastIntersec.point1.z*sortDir.z)//only x?
			{
				lastIntersec.point2 = intersecPoint;
				lastIntersec.edgeIndex2 = newEdgeIndex;
			}
			else
			{
				lastIntersec.point2 = lastIntersec.point1;
				lastIntersec.edgeIndex2 = lastIntersec.edgeIndex1;
				lastIntersec.point1 = intersecPoint;
				lastIntersec.edgeIndex1 = newEdgeIndex;
			}
		}
	}
	
	void DebugIntersecs()
	{
		//string s="";
		//_intersections.ForEach(i=>s+=i.polyIndex+"("+i.point1+"+"+i.point2+")");
		//Debug.Log(s);
	}
}
