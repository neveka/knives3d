﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerLand 
{
	List<List<Vector3>> _vertexes = new List<List<Vector3>>();

	public float GetArea()
	{
		return Utils.PolyArea(_vertexes);
	}

	public List<List<Vector3>> GetVertexes()
	{
		return _vertexes;
	}

	public void SetVertexes(List<List<Vector3>> vertexes)
	{
		_vertexes = vertexes;
	}

	public void CreateInitVertexes(Vector3 center, float radius, int idx, int playersNumber)
	{
		_vertexes.Clear();
		_vertexes.Add(new List<Vector3>());
		_vertexes[0].Add(center);
		
		int number = 25;
		int perPlayer = number/playersNumber;
		number = perPlayer*playersNumber;
		Vector3 offset = Vector3.zero;
		for(int i=idx*number/playersNumber; i<=(idx+1)*number/playersNumber; i++)
		{
			float rad = Mathf.PI*2*i/number;
			offset.x = radius*Mathf.Cos(rad);
			offset.z = radius*Mathf.Sin(rad);
			_vertexes[0].Add(offset + center);
		}
	}

	public PlayerKnifeThrow.Result DamageWithThrow(Player damager, float dist, out Vector3 [] line)
	{
		List<List<Vector3>> result1 = null;
		List<List<Vector3>> result2 = null;
		//float maxDist = 0;
		if(new KnifeThrow().GetDamaged(damager.knifeThrow.throwPoint, damager.knifeThrow.throwDir, ref _vertexes, out result1, out result2, out line))
		{
			if(line != null && (Utils.GetDistance(damager.land._vertexes, line[0])>dist || Utils.GetDistance(damager.land._vertexes, line[1])>dist))
			{
				//Debug.Log(Utils.GetDistance(damager.land._vertexes, line[0])+" "+Utils.GetDistance(damager.land._vertexes, line[1])+" "+dist);
				return PlayerKnifeThrow.Result.far;
			}

			bool isNeighor1 = Utils.AreNeighors(damager.land.GetVertexes(), result1);
			bool isNeighor2 = Utils.AreNeighors(damager.land.GetVertexes(), result2);
			if(!isNeighor1||!isNeighor2)
				return PlayerKnifeThrow.Result.cannotAddLand;
			float scale1 = Utils.PolyArea(result1);
			float scale2 = Utils.PolyArea(result2);
			if(scale1<scale2)
			{
				_vertexes = result2;
				damager.land.GetVertexes().AddRange(result1);
			}
			else
			{
				_vertexes = result1;
				damager.land.GetVertexes().AddRange(result2);
			}
			//if(maxDist*100>250+knifeLength)//?
			//	return PlayerKnifeThrow.Result.far;
			return PlayerKnifeThrow.Result.success;
		}
		return PlayerKnifeThrow.Result.outofLand;
	}

	public void GiveRoundedLand(PlayerLand harmedPlayerLand)
	{
		if(Utils.IsArounded(PlayersSpawner.radius, harmedPlayerLand.GetVertexes()))
		{
			GiveLand(harmedPlayerLand);
		}
	}

	public void GiveMinLand(PlayerLand harmedPlayerLand, float minArea)
	{
		if(harmedPlayerLand.GetArea()<minArea)
		{
			GiveLand(harmedPlayerLand);
		}
	}

	void GiveLand(PlayerLand harmedPlayerLand)
	{
		List<List<Vector3>> vertexes = GetVertexes();
		vertexes.AddRange(harmedPlayerLand.GetVertexes());
		SetVertexes(vertexes);
		harmedPlayerLand.SetVertexes(new List<List<Vector3>>());
	}
}
