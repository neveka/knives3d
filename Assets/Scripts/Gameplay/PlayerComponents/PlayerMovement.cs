﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerMovement: MonoBehaviour
{
	public float moveSpeed = 1*1.5f;
	public float rotSpeed = 60f*2;//in degrees

	public float humanMoveSpeed = 1;
	public float humanRotSpeed = 60f;//in degrees

	public Action<Vector3, bool> positionChanged;
	public Action<Quaternion> rotationChanged;

	public float lagDuration = 1f;

	PlayerAnimation _animation;
	Vector3 _correctPlayerPos;
	float _needCorrectPosTime;
	Quaternion _correctPlayerRot;
	float _needCorrectRotTime;

	void Start()
	{
		_animation = GetComponent<PlayerAnimation>();
	}

	public void Step(float speed, PlayerLand land)
	{
		_animation.StartWalk(1, speed<0);
		Vector3 pos = transform.position + transform.forward*speed*humanMoveSpeed*Time.deltaTime;
		if(Utils.IsInside(pos, land.GetVertexes()))
		{
			transform.position = pos;
			if(positionChanged != null)
				positionChanged(transform.position, speed<0);
		}
	}

	public void Stop()
	{
		_animation.StopWalk();
	}
	
	public void Rotate(float speed)
	{
		_animation.StartWalk(1, true);//StartRotate(1, speed<0);
		transform.Rotate(Vector3.up*speed*humanRotSpeed*Time.deltaTime);
		if(rotationChanged != null)
			rotationChanged(transform.rotation);
	}

	public Vector3 FindOutPosition(List<Player> players)
	{
		bool ok = false;
		float angle = 0;
		Vector3 newPos = Vector3.zero;
		while(!ok)
		{
			ok = true;
			newPos = Quaternion.Euler(0, angle, 0)*transform.position.normalized*PlayersSpawner.playersRadius;
			for(int i = 0; i<players.Count; i++)
			{
				if((players[i].transform.position-newPos).magnitude<0.5f)
				{
					ok = false;
					angle+=5;
				}
			}

		}
		return newPos;
	}

	public void GoTo(Vector3 pos, Action onFinish)
	{
		StartCoroutine(GoToCoroutine(pos, onFinish));
	}
	
	IEnumerator GoToCoroutine(Vector3 pos, Action onFinish)
	{
		Vector3 fromPos = transform.position;
		Vector3 toPos = pos;
		
		Quaternion fromRot = transform.rotation;
		Quaternion goRot = Quaternion.LookRotation(pos-transform.position);
		Quaternion toRot = Quaternion.LookRotation(Vector3.zero-pos);

		_animation.StopWalk();
		_animation.StartWalk(1, false);//
		//_animation.StopWalk();
		//_animation.StartRotate(2, Quaternion.Angle(goRot,fromRot)<0);

		float part = 0;
		float alpha = Mathf.Abs(Quaternion.Angle(goRot,fromRot));
		//Debug.LogWarning(fromRot.y+" "+goRot.y+" "+alpha);
		while(part<1 && alpha>0)
		{
			transform.rotation = Quaternion.Lerp(fromRot, goRot, part);
			//Debug.Log("1 part="+part);
			part += Time.deltaTime*rotSpeed/alpha;
			yield return null;
		}
		transform.rotation = goRot;

		//_animation.StopWalk();
		//_animation.StartWalk(2, false);

		part = 0;
		float dist = (toPos-fromPos).magnitude;
		while(part<1 && dist>0)
		{
			transform.position = Vector3.Lerp(fromPos, toPos, part);
			//transform.rotation = Quaternion.Lerp(fromRot, toRot, part);
			//Debug.Log("2 part="+part);
			part += Time.deltaTime*moveSpeed/dist;
			yield return null;
		}
		transform.position = toPos;

		//_animation.StopWalk();
		//_animation.StartRotate(2, Quaternion.Angle(toRot, goRot)<0);

		part = 0;
		alpha = Mathf.Abs(Quaternion.Angle(toRot, goRot));
		//Debug.LogWarning(toRot.y+" "+goRot.y+" "+alpha);
		while(part<1 && alpha>0)
		{
			transform.rotation = Quaternion.Lerp(goRot, toRot, part);
			//Debug.Log("3 part="+part);
			part += Time.deltaTime*rotSpeed/alpha;
			//Debug.Log("rotate "+(rotSpeed)+"*"+Time.deltaTime+" "+alpha);
			yield return null;
		}
		transform.rotation = toRot;
		_animation.StopWalk();
		onFinish();
	}

	public void SmoothPos(Vector3 pos, bool back)
	{
		//Debug.LogWarning("SmoothPos "+transform.position+" => "+ pos);
		//StopAllCoroutines();
		//StartCoroutine(StepToCoroutine(pos));
		_animation.StartWalk(1, back);
		_needCorrectPosTime = lagDuration;
		_correctPlayerPos = pos;
	}

	public void SmoothRot(Quaternion rot)
	{
		//Debug.LogWarning("SmoothRot "+transform.rotation+" => "+ rot);
		//StopAllCoroutines();
		//StartCoroutine(RotateToCoroutine(rot));
		_animation.StartWalk(1, false);
		_needCorrectRotTime = lagDuration;
		_correctPlayerRot = rot;
	}

	public bool IsSmoothRotating()
	{
		return _needCorrectRotTime>0;
	}

	void Update()
	{
		if (_needCorrectPosTime>0)
		{
			float dist = (_correctPlayerPos-transform.position).magnitude;
			float d = humanMoveSpeed*Time.deltaTime;
			if(d<dist)
			{	
				transform.position+=(_correctPlayerPos-transform.position).normalized*d;
			}
			else
			{
				transform.position = _correctPlayerPos;
				_needCorrectPosTime -= Time.deltaTime;
				if(_needCorrectRotTime<=0)
					_animation.StopWalk();
			}
		}
		if(_needCorrectRotTime>0)
		{
			float alpha = Mathf.Abs(Quaternion.Angle(_correctPlayerRot, transform.rotation));
			float p = Time.deltaTime*humanRotSpeed/alpha;
			if(p<1)
			{
				transform.rotation = Quaternion.Lerp(transform.rotation, _correctPlayerRot, p);
			}
			else
			{	
				transform.rotation = _correctPlayerRot;
				_needCorrectRotTime -= Time.deltaTime;
				if(_needCorrectPosTime<=0)
					_animation.StopWalk();
			}
		}
	}
}
