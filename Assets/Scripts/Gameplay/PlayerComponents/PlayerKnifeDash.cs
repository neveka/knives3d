﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerKnifeDash : MonoBehaviour, IPlayerKnifeTactic
{
	Knife _knife;
	PlayerScores _scores;

	Action _onFinishChooseHumanStrength;
	public Action<float, int, bool> setStrength { get; set; }
	public Action hideStrehgth { get; set; }

	//partams
	float _strength;
	bool _stop;

	public Vector3 throwPoint;

	void Start()
	{
		_knife = transform.GetComponentInChildren<Knife> ();
		_scores = GetComponent<Player> ().scores;
	}

	public float GetStrength()
	{
		return _strength;
	}

	public void ChooseOnlyStrength(bool human, Action onFinishChooseHumanPosAndDir)
	{
		if(human)
		{
			_strength = -1;
			_onFinishChooseHumanStrength = onFinishChooseHumanPosAndDir;
		}
		else
		{
			_strength = 1;//?
			onFinishChooseHumanPosAndDir();
		}
	}

	public void UpdateChoosingStrength(float speed)
	{
		if(_strength == -1)
		{
			_strength = 0;
			StartCoroutine(ChooseHumanStrengthCoroutine());
		}
		_strength += speed*1.5f*Time.deltaTime;
	}

	public void StartChoosingStrength()
	{
	}

	public void FinishChoosingStrength()
	{
		_stop = true;
	}

	IEnumerator ChooseHumanStrengthCoroutine()
	{
		setStrength(0, -1, true);
		while(_strength < 1 && !_stop)
		{
			setStrength(_strength, -1, true);
			//_drawing.AddRangeFlare(throwPoint, Vector3.one*(1-_knife.data.range/100f)*0.3f);
			//_drawing.AddWindFlare(throwPoint, Mathf.Atan(windDir.x/windDir.z), Vector3.one*windDir.magnitude);
			yield return null;
		}
		_stop = false;
		//_drawing.RemoveRangeFlare();
		//_drawing.RemoveWindFlare();
		hideStrehgth();

		_onFinishChooseHumanStrength();
	}

	public void MakeThrow(Action onFinish, Func<bool> needFinish=null) 
	{
		StartCoroutine(_knife.MakeThrowCoroutine(onFinish, needFinish, ()=>_scores.GetTargetPoint(transform.position+Vector3.up*2), ()=>{return 1.6f;}));
	}

	public bool KnifeFlies()
	{
		return _knife.flies;
	}
}
