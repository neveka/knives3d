﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour 
{
	Animation _legacyAnimation;
	Animator _animator;
	Transform _animRoot;
	void Awake()
	{
		_legacyAnimation = GetComponent<Animation>();
		_animator = GetComponent<Animator>();
		_animRoot = transform.Find("AnimRoot");
	}

	public void StartRotate(float speed, bool left)
	{
		if(_legacyAnimation!=null)
			StartWalk(speed, false);
		if(_animator!=null)
			_animator.SetBool(left?"IsRotatingLeft":"IsRotatingRight", true);
	}

	public void StartWalk(float speed, bool back)
	{
		if(_legacyAnimation!=null && !_legacyAnimation.IsPlaying("walk"))
		{
			_legacyAnimation.CrossFade("walk");
			_legacyAnimation["walk"].speed = speed;
		}
		if(_animator!=null)
			_animator.SetBool(back?"IsMovingBack":"IsMoving", true);
	}
	
	public void StopWalk()
	{
		if(_legacyAnimation!=null && _legacyAnimation.IsPlaying("walk"))
		{
			_legacyAnimation.Stop("walk");
			if(!_legacyAnimation.IsPlaying("celebration2"))
				_legacyAnimation.CrossFade("idle");
		}
		if(_animator!=null)
		{
			_animator.SetBool("IsMoving", false);
			_animator.SetBool("IsMovingBack", false);
			_animator.SetBool("IsRotatingLeft", false);
			_animator.SetBool("IsRotatingRight", false);
			_animRoot.transform.localRotation = Quaternion.identity;
		}
	}

	public void StartThrow()
	{
		if(_legacyAnimation != null)
			_legacyAnimation.CrossFade("celebration2");
		if(_animator != null)
			_animator.SetBool("IsThrowing", true);
			//PlayOnce("IsTrowing");
	}

	public void StopThrow()
	{
		if(_animator != null)
			_animator.SetBool("IsThrowing", false);
	}

	public float GetThrowDuration()
	{
		if(_legacyAnimation != null)
			return _legacyAnimation["celebration2"].length;
		if(_animator!=null)
			return (_animator.GetCurrentAnimatorClipInfo(0).Length*2);
		return 1;
	}

	public float GetThrowPart(float time)
	{
		return time/GetThrowDuration();
	}

	void PlayOnce(string paramName)
	{
		StartCoroutine(PlayOnceCoroutine(paramName));
	}

	IEnumerator PlayOnceCoroutine (string paramName)
	{
		_animator.SetBool( paramName, true );
		yield return null;
		_animator.SetBool( paramName, false );
	}

	void LateUpdate()
	{
		if(_animator.GetBool("IsRotatingLeft")||_animator.GetBool("IsRotatingRight"))
		{
			AnimatorStateInfo state = _animator.GetCurrentAnimatorStateInfo(0);
			_animRoot.transform.localRotation = 
				Quaternion.Euler(0, (state.normalizedTime%1)*90*(_animator.GetBool("IsRotatingLeft")?1:-1), 0);
		}
	}
}
