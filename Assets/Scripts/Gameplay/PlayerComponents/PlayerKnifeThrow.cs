﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public interface IPlayerKnifeTactic
{
	Action<float, int, bool> setStrength { get; set; }
	Action hideStrehgth { get; set; }

	bool KnifeFlies ();

	void StartChoosingStrength ();
	void UpdateChoosingStrength (float speed);
	void FinishChoosingStrength ();
}

public class PlayerKnifeThrow : MonoBehaviour, IPlayerKnifeTactic
{
	public enum Result
	{
		none,
		notTrusted,
		outofLand,
		cannotAddLand,
		timeOut,
		far,
		stoneCollided,
		success,
	};

	//result
	public Vector3 throwPoint;
	public Vector3 throwDir;
	public Vector3 windDir;
	public float throwOffset;
	Vector3 _startPoint;

	//state
	Action _onFinishChooseHumanPosAndDir;
	public Action<float, int, bool> setStrength { get; set; }
	public Action hideStrehgth { get; set; }

	float _strength;
	float _timeTillFinishTrow;

	//partams
	float _throwDuration = 2f;
	float _maxStrength = 2f;
	float _choosingStrengthSpeed = 0.3f;
	float _minThrowOffset = 0.5f;
	float _maxThrowOffset = 2.5f;

	//components
	Knife _knife; 
	SandDrawing _drawing;

	void Start()
	{
		_knife = transform.GetComponentInChildren<Knife>();
		_drawing = GameObject.FindObjectOfType<SandDrawing>();
	}

	public void UpdateChoosingStrength(float speed)
	{
		if(_strength == -1)
		{
			FindObjectOfType<PlayerTimer>().Stop();
			_strength = 0;//?
			_timeTillFinishTrow = _throwDuration + _knife.data.control/100f;//?
			StartCoroutine(ChooseHumanPosAndDirCoroutine());
		}
		if (speed < 0)
			speed = 0;
		_choosingStrengthSpeed = speed * 2;
		//Debug.LogError (_choosingStrengthSpeed);
	}
	
	public void StartChoosingStrength()
	{
		_startPoint = _knife.transform.position;
	}

	public void FinishChoosingStrength()
	{
		_timeTillFinishTrow = 0;
	}

	public void ChoosePosAndDir(bool human, Action onFinishChooseHumanPosAndDir)
	{
		if(human)
		{
			_strength = -1;
			_onFinishChooseHumanPosAndDir = onFinishChooseHumanPosAndDir;
		}
		else
		{
			StartCoroutine(AIChoosePosAndDirCoroutine(onFinishChooseHumanPosAndDir));
		}
	}

	IEnumerator AIChoosePosAndDirCoroutine(Action onFinishChooseHumanPosAndDir)
	{
		float angle = UnityEngine.Random.Range(-70, 70);
		Debug.Log(angle);
		Quaternion centerRot = Quaternion.LookRotation(Vector3.zero-transform.position);

		PlayerMovement movement = GetComponent<PlayerMovement>();
		movement.SmoothRot(centerRot*Quaternion.Euler(0, angle, 0));
		while(movement.IsSmoothRotating())
		{
			yield return null;
		}
		CalcPosAndDir(UnityEngine.Random.Range(_minThrowOffset, _maxThrowOffset));
		onFinishChooseHumanPosAndDir();
	}
	
	void CalcPosAndDir(float offset)
	{
		throwOffset = offset;
		Vector3 startPos = _startPoint;//_knife.transform.position;
		throwPoint = startPos + _knife.throwCamera.transform.forward * PlayersSpawner.radius * offset;
		throwPoint.y = Mathf.Max(0, 0.2f*Mathf.Sin((90+_knife.GetAngleByOffset(offset))*Mathf.PI/180));
		//Debug.LogError (throwPoint+" "+_knife.GetAngleByOffset(offset));
		//Debug.LogError (startPos+" "+throwDir);
		throwDir = new Vector3(throwPoint.x-startPos.x, 0, throwPoint.z-startPos.z);
	}

	void AddRange()
	{
		Vector3 startPos = /*_startPoint;*/_knife.transform.position;
		throwPoint += Quaternion.Euler(0, UnityEngine.Random.Range(0, 360f), 0)*Vector3.left*(1-_knife.data.range/100f)*0.3f;//?
		throwPoint += windDir * 0.1f * (1-_knife.data.weight/100f);//?
		throwDir = new Vector3(throwPoint.x-startPos.x, 0, throwPoint.z-startPos.z);
	}
	
	IEnumerator ChooseHumanPosAndDirCoroutine()
	{
		float p = _minThrowOffset;
		setStrength(0, -1, false);
		while(_strength<=_maxStrength && _timeTillFinishTrow>0)
		{
			_timeTillFinishTrow -= Time.deltaTime;
			_strength += Time.deltaTime*_choosingStrengthSpeed;
			float part = _strength/_maxStrength;
			p = _minThrowOffset*(1-part)+_maxThrowOffset*part;
			CalcPosAndDir(p);

			setStrength(part, _knife.DidKnifeThrust(throwOffset)?_knife.GetTurns(throwOffset):-1, false);
			_drawing.AddRangeFlare(throwPoint, Vector3.one*(1-_knife.data.range/100f)*0.3f);
			_drawing.AddWindFlare(throwPoint, Mathf.Atan(windDir.x/windDir.z), Vector3.one*windDir.magnitude);
			yield return null;
		}
		_drawing.RemoveRangeFlare();
		_drawing.RemoveWindFlare();
		hideStrehgth();

		CalcPosAndDir (p);
		AddRange();
		_onFinishChooseHumanPosAndDir();
	}

	public void MakeThrow(Action onFinish, Func<bool> needFinish=null) 
	{
		StartCoroutine(_knife.MakeThrowCoroutine(onFinish, needFinish, ()=>throwPoint, ()=>throwOffset));
	}

	public PlayerKnifeThrow.Result GetResult(List<Player> players, int currentPlayer, float minArea, out int harmedIndex, out Vector3[] line)
	{
		harmedIndex = -1;
		line = null;
		Result throwResult = Result.none;
		if(GameObject.FindObjectOfType<PlayerTimer>().IsTimeOut())
		{
			throwResult = Result.timeOut;
		}
		else if(PlayerStonePositioning.CollidedWithStones(players[currentPlayer].knifeThrow.throwPoint))
		{
			throwResult = Result.stoneCollided;
		}
		else if(!_knife.DidKnifeThrust(throwOffset))
		{
			throwResult = Result.notTrusted;
		}
		else
		{
			for(int i=0; i<players.Count; i++)
			{
				if(players[i].idx != currentPlayer)
				{
					throwResult = players[i].land.DamageWithThrow( players[currentPlayer], _knife.GetPullDistance(), out line);
					if(throwResult == Result.cannotAddLand||throwResult == Result.far)
					{
						break;
					}
					if(line!=null)
					{
						_knife.PlaySound(Knife.Sound.line);
					}
					if(throwResult == Result.success)
					{
						harmedIndex = i;
						players[currentPlayer].land.GiveMinLand(players[harmedIndex].land, minArea);
						players[currentPlayer].land.GiveRoundedLand(players[harmedIndex].land);
						return throwResult;
					}
				}
			}
		}
		return throwResult;
	}

	public bool KnifeFlies()
	{
		return _knife.transform.parent == transform;
	}
}
