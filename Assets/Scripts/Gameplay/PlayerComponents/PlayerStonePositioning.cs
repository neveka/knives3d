﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerStonePositioning : MonoBehaviour 
{
	public Camera playerCamera;
	public Action onFinish;
	public Action onNextTurn;
	public Action<Vector3, string> onMakeStonePositioning;
	public List<StoneData> stoneDatas = new List<StoneData>();
	public PlayerLand land;
	string _currentStoneName = null;
	bool _justSelected;

	public void AddStones()
	{
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="булыжник", modelName = "Rock3", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="валун", modelName = "Rock4", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="валун", modelName = "Rock4", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="валун", modelName = "Rock4", totalTurns = 3, radius = 0.15f});
		stoneDatas.Add(new StoneData(){name="валун", modelName = "Rock4", totalTurns = 3, radius = 0.15f});
	}

	public void SetCurrentStoneName(string stoneName)
	{
		_currentStoneName = stoneName;
		_justSelected = true;
	}

	void Update()
	{
		if(_currentStoneName!=null && !_justSelected && Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float t = -ray.origin.y/ray.direction.y;
			Vector3 pos = ray.origin+t*ray.direction;
			if(onMakeStonePositioning!=null)
				onMakeStonePositioning(pos, _currentStoneName);
			MakeStonePositioning(pos, _currentStoneName);
			_currentStoneName = null;
		}
		_justSelected = false;
	}

	public void MakeAIStonePositioning()
	{
		if(stoneDatas.Count == 0)
			return;
		Vector3 pos = Vector3.zero;
		while(pos == Vector3.zero||!Utils.IsInside(pos, land.GetVertexes()))
			pos = new Vector3(UnityEngine.Random.Range(-PlayersSpawner.radius, PlayersSpawner.radius), 0, 
			            UnityEngine.Random.Range(-PlayersSpawner.radius, PlayersSpawner.radius));
		MakeStonePositioning(pos, stoneDatas[UnityEngine.Random.Range(0, stoneDatas.Count)].name);//temp
	}

	public void MakeStonePositioning(Vector3 pos, string stoneName)
	{
		StoneData sd = stoneDatas.Find(d=>d.name == stoneName);
		stoneDatas.Remove(sd);
		Stone stone = new PlayersSpawner().Spawn(sd);
		onNextTurn+=stone.OnNextTurn;
		stone.transform.position = pos;
		stone.transform.localScale = sd.radius*2*Vector3.one;
		if(onFinish != null)
			onFinish();
	}

	public static bool CollidedWithStones(Vector3 pos)
	{
		return Array.Find(FindObjectsOfType<Stone>(), s=>{
			//Debug.LogWarning((s.transform.position-pos).magnitude);
			return (s.transform.position-pos).magnitude<s.data.radius;
		})!=null;
	}
}
