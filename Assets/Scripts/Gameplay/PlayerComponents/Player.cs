﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class Player : MonoBehaviour 
{
	public int idx;
	public PlayerData data;

	public PlayerLand land = new PlayerLand();
	public PlayerScores scores;
	public PlayerMovement movement;
	public PlayerKnifeThrow knifeThrow;
	public PlayerKnifeDash knifeDash;
	public PlayerStonePositioning stonePositioning;
	public Knife knife;
	public Text text;
	public Camera childCamera;
	public Camera childCamera2;
	public Camera dashCamera;
	public Transform knifeParent;
	public SkinnedMeshRenderer mainSkin;
	public Transform hips;

	public void Init(PlayerData playerData)
	{
		data = playerData;
		if(!string.IsNullOrEmpty(playerData.materialName))
			mainSkin.material = Resources.Load<Material>(playerData.materialName);
		childCamera.gameObject.SetActive(false);
		childCamera2.gameObject.SetActive(false);
		dashCamera.gameObject.SetActive(false);
		text.color = playerData.color;
		text.text = "";
		text.transform.parent.GetComponent<UnityEngine.UI.RawImage>().color = Color.white*0;
		GetComponentInChildren<Animator>().enabled = false;

		/*if(playerData.clothes != null)
		{
			SkinnedClothes[] clothes = GetComponentsInChildren<SkinnedClothes>(true);
			foreach(SkinnedClothes cl in clothes)
				if(cl.gameObject != mainSkin.gameObject)
					cl.gameObject.SetActive(playerData.clothes.Contains(cl.name));
		}*/
	}

	public void InitForCombat(int idx, int count, bool isDash)
	{
		this.idx = idx;
		float rad = Mathf.PI*2*(idx+0.5f)/count;
		Vector3 offset = Vector3.zero;
		offset.x = PlayersSpawner.playersRadius*Mathf.Cos(rad);
		offset.z = PlayersSpawner.playersRadius*Mathf.Sin(rad);
		transform.position = offset;
		transform.LookAt(Vector3.zero);
		movement = gameObject.AddComponent<PlayerMovement>();
		knifeThrow = gameObject.AddComponent<PlayerKnifeThrow>();
		knifeDash = gameObject.AddComponent<PlayerKnifeDash> ();
		stonePositioning = gameObject.AddComponent<PlayerStonePositioning>();
		stonePositioning.playerCamera = childCamera;
		stonePositioning.land = land;
		if(!isDash)
			stonePositioning.AddStones();
		knife.throwAnimation = gameObject.AddComponent<PlayerAnimation>();
		land.CreateInitVertexes(Vector3.zero, PlayersSpawner.radius, idx, count);
		scores.onTurboMode = (bool turboMode)=>{knife.ShowSubEffect(turboMode);};
		if(!isDash)
			scores.Hide ();
	}

	public void MapButtons(HUD hud, bool isDash)
	{
		IPlayerKnifeTactic knifeTactic = isDash?(IPlayerKnifeTactic)knifeDash:(IPlayerKnifeTactic)knifeThrow;
		knifeTactic.setStrength = hud.SetThrowStrength;
		knifeTactic.hideStrehgth = hud.HideThrowStrength;

		SwipeControll swipeCtrl = hud.GetComponent<SwipeControll> ();
		swipeCtrl.onSwipeStarted = knifeTactic.StartChoosingStrength;
		swipeCtrl.onSwipe = (type, from, oldto, to) => {
			if(knifeTactic.KnifeFlies())
				return;
			if(!isDash && (type == SwipeControll.Type.Diagonal||type == SwipeControll.Type.Horizontal))
				movement.Rotate((to.x-from.x)/Screen.width);
			if(type == SwipeControll.Type.Diagonal)
			{
				movement.Stop();
				//Debug.LogError(oldto+" "+to);
				knifeTactic.UpdateChoosingStrength((to.y-oldto.y)/Time.deltaTime/Screen.height);
			}
			else
				movement.Step((to.y-from.y)/Screen.height, land);
		};
		hud.showTurns = !isDash;
		swipeCtrl.hasMaxDuration = isDash;
		swipeCtrl.onSwipeFinished = (type, from, to) => {
			if(knifeTactic.KnifeFlies())
				return;
			if(type == SwipeControll.Type.Diagonal)
			{
				knifeTactic.FinishChoosingStrength();
			}
			movement.Stop();
		};

		hud.GetButton(HUD.Buttons.Throw).GetComponent<PointerListener>().onPressedDown = () => {knifeTactic.StartChoosingStrength();};
		hud.GetButton(HUD.Buttons.Throw).GetComponent<PointerListener>().onPressedUp = () => knifeTactic.FinishChoosingStrength();

		//hud.GetButton(HUD.Buttons.Forward).GetComponent<PointerListener>().onPressedDown = () => movement.BeginAnim();
		//hud.GetButton(HUD.Buttons.Back).GetComponent<PointerListener>().onPressedDown = () => movement.BeginAnim();
		//hud.GetButton(HUD.Buttons.Right).GetComponent<PointerListener>().onPressedDown = () => movement.BeginAnim();
		//hud.GetButton(HUD.Buttons.Left).GetComponent<PointerListener>().onPressedDown = () => movement.BeginAnim();

		hud.GetButton(HUD.Buttons.Forward).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();
		hud.GetButton(HUD.Buttons.Back).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();
		hud.GetButton(HUD.Buttons.Right).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();
		hud.GetButton(HUD.Buttons.Left).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();
		hud.GetButton(HUD.Buttons.EnemyRight).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();
		hud.GetButton(HUD.Buttons.EnemyLeft).GetComponent<PointerListener>().onPressedUp = () => movement.Stop();

		hud.GetButton(HUD.Buttons.Forward).GetComponent<PointerListener>().onPressed = () => movement.Step(1, land);
		hud.GetButton(HUD.Buttons.Back).GetComponent<PointerListener>().onPressed = () => movement.Step(-1, land);
		hud.GetButton(HUD.Buttons.Right).GetComponent<PointerListener>().onPressed = () => movement.Rotate(1);
		hud.GetButton(HUD.Buttons.Left).GetComponent<PointerListener>().onPressed = () => movement.Rotate(-1);
		hud.GetButton(HUD.Buttons.EnemyRight).GetComponent<PointerListener>().onPressed = () => movement.Rotate(1);
		hud.GetButton(HUD.Buttons.EnemyLeft).GetComponent<PointerListener>().onPressed = () => movement.Rotate(-1);

	}
}
