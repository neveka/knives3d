﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Knife : MonoBehaviour 
{
	public enum Sound
	{
		metal,
		line,
		notTrusted,
		number
	}

	static AudioSource source;
	static List<AudioClip> clips = new List<AudioClip>();
	public KnifeData data;
	public GameObject effect;
	public int locationIdx;

	//public bool collided;
	//public Player master;
	public Camera throwCamera;
	public PlayerAnimation throwAnimation;
	public bool flies;

	protected void Awake () 
	{
		if(!source)
		{
			source = new GameObject("KnifeSound").AddComponent<AudioSource>();
			source.playOnAwake = false;
			for(int i=0; i<(int)Sound.number; i++)
				clips.Add((AudioClip)Resources.Load(((Sound)i).ToString()));
		}
		effect = new GameObject("Effect");
		effect.transform.parent = transform;
		effect.transform.localPosition = Vector3.zero;
		effect.transform.localRotation = Quaternion.identity;
		effect.gameObject.SetActive(false);
		/*CapsuleCollider cc = gameObject.AddComponent<CapsuleCollider>();
		cc.isTrigger = true;
		cc.radius = 0.03f;
		cc.height = 0.25f;
		cc.direction = 2;
		cc.center = Vector3.zero;
		Rigidbody rb = gameObject.AddComponent<Rigidbody>();
		rb.isKinematic = true;
		rb.useGravity = false;
		rb.collisionDetectionMode = CollisionDetectionMode.Continuous;*/
		Destroy(GetComponent<MeshCollider>());
	}

	public void Init(Player player, KnifeData knifeData, bool isDash, int locationIdx, Func<Transform, string, GameObject> spawnEffect)
	{
		//master = player;
		throwCamera = isDash?player.dashCamera:player.childCamera;
		data = knifeData;
		this.locationIdx = locationIdx;
		transform.parent = player.knifeParent;
		transform.localRotation = Quaternion.AngleAxis(270, Vector3.right);
		transform.localPosition = Vector3.zero;

		if(!isDash && !string.IsNullOrEmpty(knifeData.effectName))
		{
			spawnEffect(effect.transform, knifeData.effectName);
		}
		else if(isDash)
		{
			spawnEffect(effect.transform, "FireTail");
		}
	}

	public void PlaySound(Sound clipName)
	{
		source.clip = clips[(int)clipName];
		source.Play();
	}

	public void ShowSubEffect(bool show)
	{
		Transform effectTransform = effect.transform;
		if(effectTransform.childCount>0)
			effectTransform.GetChild(0).gameObject.SetActive(show);
	}

	public IEnumerator MakeThrowCoroutine(Action onFinish, Func<bool> needFinish, Func<Vector3> getThrowPoint, Func<float> getOffset)
	{
		Transform hand = transform.parent;//
		flies = true;
		transform.parent = throwAnimation.transform;
		transform.localScale = Vector3.one;
		Vector3 startPos = transform.position;
		Quaternion startCamRot = throwCamera.transform.rotation;
		float startFov = throwCamera.fieldOfView;
		throwAnimation.StartThrow();
		PlaySound(Knife.Sound.metal);
		if(effect)
			effect.SetActive(true);
		float time = 0;
		bool continuesFly = false;
		while(throwAnimation.GetThrowPart(time)<1||continuesFly)
		{
			time += Time.deltaTime;
			float p = throwAnimation.GetThrowPart(time);

			Vector3 throwPoint = getThrowPoint();//_scores.GetTargetPoint(startPos);
			Vector3 pos = startPos*(1-p) + throwPoint*p;
			pos.y =  startPos.y+5*p-(startPos.y-throwPoint.y)*p-5*p*p;

			transform.position = pos;
			transform.localRotation = Quaternion.Euler(90+GetAngleByOffset(getOffset()*p), 0, 0);
			throwCamera.transform.LookAt(transform.position);
			throwCamera.fieldOfView = 7;
			if(throwAnimation.GetThrowPart(time)>=1 && needFinish!=null)
			{
				if(!continuesFly)
				{
					transform.position = throwPoint;
					if(needFinish())
						break;
					else
					{
						continuesFly = true;
						throwAnimation.StopThrow();
					}
				}
				else if(transform.position.y<=0)
				{
					break;
				}
			}
			yield return null;
		}
		//Debug.LogError ("final "+getThrowPoint()+" "+GetAngleByOffset(getOffset()));
		if(effect)
			effect.SetActive(false);
		throwAnimation.StopThrow();		
		if(DidKnifeThrust(getOffset()) && needFinish==null)
		{
			throwCamera.fieldOfView = startFov;
			throwCamera.transform.rotation = startCamRot;
			onFinish();
		}
		else
			PlaySound(Knife.Sound.notTrusted);
		yield return new WaitForSeconds(1);
		//collided = false;
		transform.parent = hand;
		transform.localScale = Vector3.one;
		transform.localRotation = Quaternion.Euler(270, 0, 0);
		transform.localPosition = Vector3.zero;
		flies = false;
		if (!DidKnifeThrust (getOffset())||needFinish!=null) 
		{
			throwCamera.fieldOfView = startFov;
			throwCamera.transform.rotation = startCamRot;
			onFinish ();
		}
	}

	public float GetAngleByOffset(float offset)
	{
		return (offset*300+offset*offset*60)%360;//?
	}

	public float GetPullDistance()
	{
		return PlayersSpawner.radius*0.6f+data.lenfth/100f;
	}

	public int GetTurns(float offset)
	{
		return (int)Mathf.Round((offset*300+offset*offset*60)/360);
	}

	public bool DidKnifeThrust(float offset)
	{
		float angle = GetAngleByOffset(offset);
		float slope = data.thrustChance*50/100+10+50*(1-locationIdx/8f);
		//Debug.LogWarning(angle+" "+(360-angle)+" "+slope);
		return angle<slope||360-angle<slope;
	}
	
	/*void OnTriggerEnter(Collider other)
	{
		if(other.transform.root != master && !collided && master.scores && master.scores.CheckOnTriggerEnter(other, transform))
		{
			//Debug.Log(other);
			collided = true;
			transform.parent = other.transform;
		}
	}

	void Update()
	{
		if(transform.position.y<0 && !collided && master && master.scores && master.scores.CheckOnGround())
		{
			collided = true;
		}
	}*/
}
