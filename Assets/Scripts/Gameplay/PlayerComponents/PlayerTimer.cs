﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerTimer : MonoBehaviour 
{
	public Action<float> onTimeChanged;
	public Action onStoped;
	public Action onTimeOut;
	public float time;
	float _time;

	public void StartCountDown () 
	{
		//Debug.LogWarning("PlayerTimer StartCountDown "+time);
		_time = time;
	}

	public void Stop()
	{
		//Debug.LogWarning("PlayerTimer stop");
		_time = 0;
		onStoped();
	}

	public bool IsTimeOut()
	{
		return _time<0;
	}

	// Update is called once per frame
	void Update () 
	{
		if(_time>0)
		{
			_time -= Time.deltaTime;
			onTimeChanged(_time>0?_time:0);
			if(_time<=0)
			{
				//Debug.LogWarning("PlayerTimer onTimeOut");
				if(onTimeOut != null)
					onTimeOut();
				Stop();
			}
		}
		/*if(_time<0)
		{
			_time = 0;
			onTimeChanged(_time);
		}*/
	}
}
