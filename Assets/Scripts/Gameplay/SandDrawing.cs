﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SandDrawing : MonoBehaviour 
{
	public GameObject pointPrefab;
	public RangeFlare rangeFlarePrefab;
	public RangeFlare windFlarePrefab;
	public Color lineColor = new Color (0.2f, 0.2f, 0.1f, 0.5f);
	
	protected int _currentIdx;
	protected List<List<Vector3>> _currentVertexes;
	protected Color _currentColor;
	protected Color _currentEdgeColor;

	public void DrawPoly(List<List<Vector3>> vertexes, int idx, Color c)
	{
		_currentIdx = idx;
		_currentVertexes = vertexes;
		_currentColor = new Color(0.2f, 0.2f, 0.1f, 0.05f);//new Color(c.r, c.g, c.b, 0.05f);
		_currentEdgeColor = lineColor;//new Color(c.r, c.g, c.b, 0.5f);
		DeleteWithName("Point"+_currentIdx);
		for(int i=0; i<vertexes.Count; i++)
		{
			for(int j=0; j<vertexes[i].Count; j++)
			{
				DrawLine(vertexes[i][j], vertexes[i][j<vertexes[i].Count-1?j+1:0], "Point"+_currentIdx, Color.clear);
			}
		}
	}

	public void DrawOutline(List<List<Vector3>> vertexes, float dist, bool hide)
	{
		Color color = new Color(1f, 1f, 1f, 1f);
		float width = 0.03f;
		Vector3 pos = Vector3.zero;
		int count = 40;
		for(int i=-count; i<=count; i++)
			for(int j=-count; j<=count; j++)
		{			
			pos.x = i*3f/count;
			pos.z = j*3f/count;
			if(pos.sqrMagnitude<PlayersSpawner.radius*PlayersSpawner.radius && Utils.IsInside(pos, vertexes))
				continue;
			float d = Utils.GetDistance(vertexes, pos);
			if(d<dist+width && d>dist-width)
				AddPoint(pos, color/**(1-Mathf.Abs(dist-d)/width)*/, "Outline");
		}
		if(hide)
			StartCoroutine(HideOutlineCoroutine());
	}

	public void HideOutline()
	{
		HideWithName("Outline");
	}

	IEnumerator HideOutlineCoroutine()
	{
		yield return new WaitForSeconds(1);
		//DeleteWithName("Outline");
		HideWithName("Outline");
	}

	void HideWithName(string pointName)
	{
		for(int i=0; i< transform.childCount; i++)
		{
			if(transform.GetChild(i).name == pointName)
			{
				transform.GetChild(i).gameObject.SetActive(false);
				pointsHash.Add(transform.GetChild(i).gameObject);
			}
		}
	}

	public void DeleteWithName(string pointName)
	{
		List<Transform> childrenToDelete = new List<Transform>();
		for(int i=0; i< transform.childCount; i++)
		{
			if(transform.GetChild(i).name == pointName)
				childrenToDelete.Add(transform.GetChild(i));
		}
		while(childrenToDelete.Count>0)
		{
			DestroyImmediate(childrenToDelete[0].gameObject);
			childrenToDelete.RemoveAt(0);
		}
	}

	public void AddCutLine(Vector3[] line, bool success)
	{
		DrawLine(line[0], line[1], "CutLine", success?Color.green:Color.red);
		StartCoroutine(HideCutLine());
	}

	IEnumerator HideCutLine()
	{
		yield return new WaitForSeconds(1);
		DestroyImmediate(transform.Find("CutLine").gameObject);
	}

	public void DrawLine(Vector3 fromVertex, Vector3 toVertex, string lineName, Color color)
	{
		Vector3 point = Vector3.zero;
		Vector3 from = fromVertex, to = fromVertex;
		bool oldEdge = false;
		float step = 0.05f;
		int steps = (int)((toVertex-fromVertex).magnitude/step);
		for(int i=0; i<steps; i++)
		{
			float p = ((float)i+0.5f)/steps;
			point = fromVertex*(1-p)+toVertex*p;
			bool edge = _currentVertexes!=null && Utils.IsOnEdge(point, _currentVertexes);
			if(_currentVertexes == null||(i!=0 && oldEdge != edge)||i==steps-1)
			{
				to = i==steps-1?toVertex:point;
				GameObject go = Instantiate(pointPrefab);//?
				go.transform.parent = transform;
				go.name = lineName;
				if(color == Color.clear)
					go.GetComponent<SpriteRenderer>().color = Utils.IsOnEdge((from + to)/2f, _currentVertexes)?_currentEdgeColor:_currentColor;
				else
					go.GetComponent<SpriteRenderer>().color = color;
				go.transform.position = (from + to)/2f;
				go.transform.localScale = new Vector3((from - to).magnitude*50f, 2.5f, 2.5f);
				if(_currentVertexes == null)
					go.transform.LookAt(Vector3.zero);
				else
					go.transform.Rotate(Vector3.forward, Mathf.Atan((from - to).z/(from - to).x)*180/Mathf.PI);
				from = to;
			}
			oldEdge = edge;
		}

		/*float noize = 0;// 0.03f;
		float step = 0.05f;
		int steps = (int)((to-from).magnitude/step);
		Vector3 point = Vector3.zero;
		for(int i=0; i<steps; i++)
		{
			float p = ((float)i+0.5f)/steps;
			point = from*p+to*(1-p);
			bool edge = Utils.IsOnEdge(point, _currentVertexes);
			point.x += Random.Range(-noize, +noize);
			point.z += Random.Range(-noize, +noize);
			AddPoint(point, edge);
		}*/
	}
	List<GameObject> pointsHash = new List<GameObject>();
	void AddPoint(Vector3 pos, Color color, string pointName)
	{
		GameObject go = pointsHash.Count>0?pointsHash[0]:Instantiate(pointPrefab);
		if(pointsHash.Count>0)
		{
			go.SetActive(true);
			pointsHash.RemoveAt(0);
		}
		go.transform.parent = transform;
		go.name = pointName;
		go.GetComponent<SpriteRenderer>().color = color;
		go.transform.position = pos;
		go.transform.localScale = Vector3.one*2;
		go.transform.Rotate(Vector3.forward, Random.Range(0, 180));
	}

	RangeFlare _windFlare;
	RangeFlare _rangeFlare;
	public void AddRangeFlare(Vector3 pos, Vector3 scale)
	{
		if(!_rangeFlare)
		{
			_rangeFlare = Instantiate(rangeFlarePrefab);
			_rangeFlare.transform.parent = transform;
			_rangeFlare.name = "RangeFlare";
		}
		pos.y = 0.05f;
		_rangeFlare.transform.position = pos;
		_rangeFlare.transform.localScale = scale*0.57f;
	}

	public void AddWindFlare(Vector3 pos, float angle, Vector3 scale)
	{
		if(scale == Vector3.zero)
			return;
		if(!_windFlare)
		{
			_windFlare = Instantiate(windFlarePrefab);
			_windFlare.transform.parent = transform;
			_windFlare.name = "WindFlare";
		}
		pos.y = 0.05f;
		_windFlare.transform.position = pos;
		_windFlare.transform.localScale = scale;
		_windFlare.transform.localRotation = Quaternion.Euler(90, angle, 0);
	}

	public void RemoveRangeFlare()
	{
		if(_rangeFlare)
		{
			Destroy(_rangeFlare.gameObject);
		}
	}

	public void RemoveWindFlare()
	{
		if(_windFlare)
		{
			Destroy(_windFlare.gameObject);
		}
	}
}
