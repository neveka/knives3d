﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ResultScreen : GameScreen 
{
	public Text title;
	public CurrencyControll rewardControll;
	public BusinessItemPanel itemPanel;
	public GameObject stars;

	public void ShowVictory(float reward, float rewardForLevel, float exp, ProgressData progressData, int level, string reason, KnifeData knifeData, Action onClose)
	{
		title.text = "Победа!";
		rewardControll.SetText(reason+". Награда : <color=#33aa00ff>$</color>"+reward+
		                       (reward == 0?" Играй по сети чтобы заработать.":"")+"\n\r"+
		                       (exp == 0?"":(" \nОпыт : "+exp))+
		                       (rewardForLevel == 0?"":("\nНовый уровень : "+level+"\nНаграда за уровень: <color=#33aa00ff>$</color>"+rewardForLevel)));
		stars.SetActive(true);
		Show(level, knifeData, progressData, onClose);
	}

	public void ShowLost(float reward, float rewardForLevel, float exp, ProgressData progressData, int level, string reason, KnifeData knifeData, Action onClose)
	{
		title.text = "Проигрыш!";
		rewardControll.SetText(reason+
		                       (exp == 0?"":(" \nОпыт :"+exp))+
		                       (rewardForLevel == 0?"":("\nНовый уровень : "+level+"\nНаграда : <color=#33aa00ff>$</color>"+rewardForLevel)));
		stars.SetActive(false);
		Show(level, knifeData, progressData, onClose);
	}

	public void Show(int level, KnifeData knifeData, ProgressData progressData, Action onClose)
	{
		_onClose = onClose;
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
		itemPanel.gameObject.SetActive(false);
		if(knifeData!=null)
		{
			itemPanel.gameObject.SetActive(true);
			itemPanel.SetKnife(knifeData, progressData, level, null);
		}
	}
}
