﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour
{
	public Button closeButton;
	protected bool _justCreated = true;
	protected float _waitTime = 2;
	protected Action _onClose;

	public static T CreateScreen<T> (T prefab) where T: MonoBehaviour
	{
		T screen = GameObject.Instantiate(prefab);
		ProfileControll menu = GameObject.FindObjectOfType<ProfileControll>();
		ScenaryCreator scenaryCreator = GameObject.FindObjectOfType<ScenaryCreator>();
		CharSelector selector = FindObjectOfType<CharSelector>();
		if(menu)
			screen.transform.SetParent(menu.GetComponentInParent<Canvas>().transform);
		else if(scenaryCreator)
			screen.transform.SetParent(scenaryCreator.GetComponentInChildren<Canvas>().transform);
		else if(selector)
			screen.transform.SetParent(selector.GetComponentInChildren<Canvas>().transform);
		else
			screen.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
		screen.transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
		screen.transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
		screen.transform.localScale = Vector3.one;
		return screen;
	}
	
	public static T CreateScreen<T>() where T: MonoBehaviour
	{
		return CreateScreen(Resources.Load<T>(typeof(T).Name));
	}

	public virtual void Close()
	{
		if(!_justCreated)
		{
			if(_onClose != null)
				_onClose();
			Destroy(gameObject);
		}
	}

	public virtual void ForseClose()
	{
		if(_onClose != null)
			_onClose();
		Destroy(gameObject);
	}
	
	protected virtual void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape) && !_justCreated) 
		{
			if(_onClose != null)_onClose();
			Destroy(gameObject);
		}
		if(_justCreated)
		{
			_waitTime -= Time.deltaTime;
			if(_waitTime<=0)
				_justCreated = false;
		}
		closeButton.enabled = !_justCreated;
	}
}

