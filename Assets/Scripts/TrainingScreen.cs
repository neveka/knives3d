﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class TrainingScreen : MessageScreen 
{
	public Button tvButton;
	public Action startCurrentLocation;
	public void ShowInfo(string titleText, string text, Action onClose)
	{
		_onClose = onClose;
		title.text = titleText;
		rewardControll.SetText(text);
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
		tvButton.onClick.AddListener(()=>{
			ADCAdManager adcolony = FindObjectOfType<ADCAdManager>();
			adcolony.giveVideoBonus = ()=>{startCurrentLocation();};
			adcolony.ShowVideoAdByZoneKey(2);
		});
#if UNITY_WEBPLAYER
		tvButton.gameObject.SetActive(false);
#endif
	}
}
