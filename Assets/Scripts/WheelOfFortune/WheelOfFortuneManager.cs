﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class WheelOfFortuneManager : MonoBehaviour 
{
	public Action onNoGold;

	private ProgressData _progressData;
	private WheelData _wheelData;
	private WheelOfFortuneVisual _wheelVisual;
	private int _result;
	private bool _free;

	// Update is called once per frame
	public void Init (ProgressData progressData, WheelData wheelData) 
	{
		_progressData = progressData;
		_wheelData = wheelData;
		Calc ();
	}

	public void Show()
	{
		_wheelVisual = WheelOfFortuneVisual.CreateScreen<WheelOfFortuneVisual> ();
		_wheelVisual.Init (OnGetButtonText, OnGetResult);
		_wheelVisual.RotationFinished += OnRotationFinished;
		_wheelVisual.ButtonPressed += OnButtonPressed;
		_wheelVisual.SetItems (_wheelData.prizeDatas);
	}

	void Calc()
	{
		_result = 0;
		int dice = UnityEngine.Random.Range (0, 100);
		for(int i=0; i<_wheelData.prizeDatas.Count; i++) 
		{
			dice -= _wheelData.prizeDatas[i].chance;
			if (dice <= 0) 
			{
				_result = i;
				break;
			}
		}
		_free = (DateTime.UtcNow-_progressData.lastSpinTime).TotalDays>1;
	}

	string OnGetButtonText()
	{
		return _free?"Вращать бесплатно":"Вращать за <color=yellow>@</color>1";
	}

	int OnGetResult()
	{
		_progressData.lastSpinTime = DateTime.UtcNow;
		return _result;
	}

	void OnButtonPressed(object sender, EventArgs e)
	{
		if (_free || _progressData.gold >= 1)
			_wheelVisual.WheelGo (200);
		else 
		{
			onNoGold ();
			_wheelVisual.ForseClose ();
		}
	}

	void OnRotationFinished(object sender, EventArgs e)
	{
		if (!_free)
			_progressData.gold -= 1;
		PrizeData prizeData = _wheelData.prizeDatas [_result];
		_wheelVisual.ShowReward (prizeData);
		if (prizeData.type == "knife")
			_progressData.AddKnife (prizeData.name);
		if (prizeData.type == "money")
			_progressData.money += prizeData.amount;
		if (prizeData.type == "gold")
			_progressData.gold += prizeData.amount;
		Calc ();
	}
}
