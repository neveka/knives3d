using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


class WheelOfFortuneVisual : GameScreen
{
	public event EventHandler RotationFinished;
	public event EventHandler ButtonPressed;

    [SerializeField]
    private Transform wheel = null;

    [SerializeField]
	private FortuneArrowHandler _fortuneArrowHandler;

	[SerializeField]
	private Button _spin;

	[SerializeField]
	List<BusinessItemPanel> _items = new List<BusinessItemPanel>();

	[SerializeField]
	GameObject rewardWindow;

	[SerializeField]
	BusinessItemPanel rewardItem;

	private Func<string> _getSpinButtonText;
	private Func<int> _getResult;
	private bool _leftRotate;
	private float _speed;

	void Start()
	{
		ShowSpin ();
	}

	public IFortuneArrowHandler FortuneArrowHandler { get{ return _fortuneArrowHandler; } }
	public void Init(Func<string> getSpinButtonText, Func<int> getResult)
	{
		_getSpinButtonText = getSpinButtonText;
		_getResult = getResult;
		_spin.GetComponentInChildren<Text> ().text = _getSpinButtonText();
		closeButton.onClick.RemoveAllListeners();
		closeButton.onClick.AddListener(()=>Close());
	}

	public void EnableSpin(bool enable)
	{
		_spin.interactable = enable;
	}

	public void EnableExit(bool enable)
	{
		closeButton.interactable = enable;
	}

	public void SetItems(List<PrizeData> prizes)
	{
		for (int i = 0; i < prizes.Count; i++) 
		{
			_items [i].SetPrize(prizes[i]);
		}
	}

    public void ShowSpin () {
		_spin.GetComponentInChildren<Text> ().text = _getSpinButtonText ();
        _spin.gameObject.SetActive (true);
		_spin.onClick.RemoveAllListeners ();
		_spin.onClick.AddListener (()=>ButtonPressed(this, EventArgs.Empty));
    }

    public void WheelGo(float speed)
	{
		_leftRotate = false;//leftRotate;
		_speed = speed;
		_fortuneArrowHandler.SetDirection (_leftRotate);

		_spin.GetComponentInChildren<Text> ().text = "...";
		_spin.gameObject.SetActive (false);

		TweenValue tv = TweenValue.Begin( gameObject , 360f*2 / _speed , wheel.localEulerAngles.z - 360 );
		tv.from = wheel.localEulerAngles.z;
		tv.style = UITweener.Style.Once;
		tv.method = UITweener.Method.EaseIn;
		tv.target = transform;
		tv.onValueChanged.Add( SetValue );
		tv.onFinished = null;
		tv.onFinished += OnStartFinish;
	}

	void OnStartFinish()
	{
		_spin.GetComponentInChildren<Text> ().text = "Остановить";
		_spin.gameObject.SetActive (true);
		_spin.onClick.RemoveAllListeners ();
		_spin.onClick.AddListener (()=>WheelResult (_getResult()));

		TweenValue tv = TweenValue.Begin( gameObject , 360f / _speed , wheel.localEulerAngles.z - 360 );
		tv.from = wheel.localEulerAngles.z;
        tv.method = UITweener.Method.Linear;
        tv.style = UITweener.Style.Loop;
        tv.target = transform;
		tv.onValueChanged.Add( SetValue );
        tv.eventReceiver = gameObject;
    }

	public void WheelResult(int rez)
	{
		_spin.gameObject.SetActive (false);

		if (_leftRotate)
		{
			float startA = 360 - wheel.localEulerAngles.z;
			float finishA = 360 * Mathf.FloorToInt(_speed / 360) + (rez * 45) /*+ UnityEngine.Random.Range(-6, 30)*/;
			float oneWheel = (360 + startA + finishA) / _speed; //?
			ValueToTarget(startA, finishA, oneWheel);
		}
		else
		{
			float startA = 360 + 360 * Mathf.FloorToInt(_speed / 360) + wheel.localEulerAngles.z;
			float finishA = (rez * 45) /*+ UnityEngine.Random.Range(-30, 6)*/;
			float oneWheel = (startA - finishA)*2 / _speed;
			ValueToTarget(startA, finishA, oneWheel);
		}
	}

	void ValueToTarget(float startA, float finishA, float oneWheel)
	{
		TweenValue tv = TweenValue.Begin( gameObject , oneWheel , finishA );
		tv.from = startA;
		tv.style = UITweener.Style.Once;
		tv.method = UITweener.Method.EaseOut;
		tv.target = transform;
		tv.onValueChanged.Add( SetValue );
		tv.onFinished = null;
		tv.onFinished += OnRotationFinish;
	}

	void SetValue(float val)
	{
		wheel.localEulerAngles = new Vector3(0, 0, _leftRotate ? -val : val);
	}

	void OnRotationFinish()
	{
		if (RotationFinished != null)
			RotationFinished (this, EventArgs.Empty);
		
		/*TweenValue tv = TweenValue.Begin(GameObject, 0.8f, 10);
		tv.target = null;
		tv.onFinished.Clear();
		var tmp = new EventDelegate(ShowResult);
		tmp.oneShot = true;
		tv.onFinished.Add(tmp);*/
	}

	public void ShowReward(PrizeData priseData)
	{
		rewardWindow.SetActive (true);
		rewardItem.SetPrize (priseData);
	}

	public void HideReward()
	{
		rewardWindow.SetActive (false);
		ShowSpin ();
	}
}