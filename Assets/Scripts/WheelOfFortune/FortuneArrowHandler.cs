using System;
using System.Collections.Generic;
using UnityEngine;


public interface IFortuneArrowHandler
{
	void SetDirection (bool leftRotate);
}
	
class FortuneArrowHandler : MonoBehaviour , IFortuneArrowHandler
{
	public void SetDirection(bool leftRotate)
	{
		_leftRotate = leftRotate;
	}

    private void OnTriggerStay( Collider other )
    {
		if (_tr && _tr.enabled)
			return;
		Vector3 va = /*_gameCamera.WorldToScreenPoint*/(transform.position);
		Vector3 vc = /*_gameCamera.WorldToScreenPoint*/(other.transform.position);
		if (_leftRotate)
		{
			if (vc.x <= va.x)
			{
				_a0 = Vector2.Angle(Vector3.down, Vector3.Normalize(other.transform.position - transform.position));
				transform.localEulerAngles = new Vector3(0, 0, -_a0);
			}
		}
		else
		{
			if (vc.x >= va.x)
			{
				_a0 = Vector2.Angle(Vector3.down, Vector3.Normalize(other.transform.position - transform.position));
				transform.localEulerAngles = new Vector3(0, 0, _a0);
			}
		}
    }

    private void OnTriggerExit( Collider other )
    {
		_tr = TweenRotation.Begin( transform.gameObject , 0.1f , Quaternion.identity );//0.1 - 0.05
		_tr.from = new Vector3( 0 , 0 , _a0 );
    }

	[SerializeField]
	private bool 		_leftRotate;
	[SerializeField]
	private Camera 		_gameCamera;
	private TweenRotation _tr;
	private float _a0;
}

