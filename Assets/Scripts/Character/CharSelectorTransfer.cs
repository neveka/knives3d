﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CharSelectorTransfer : MonoBehaviour 
{
	public List<PlayerData> playerDatas = new List<PlayerData>();
	public ProgressData progressData;
	public int level;
	public string profileName;
	public Action<int> onEsc;
	public int charIdx;
	public static CharSelectorTransfer GetOrCreate()
	{
		CharSelectorTransfer trans = FindObjectOfType<CharSelectorTransfer>();
		if(trans != null)
			return trans;
		trans = new GameObject("CharSelectorTransfer").AddComponent<CharSelectorTransfer>();
		trans.playerDatas.Add(new PlayerData(){name="игрок N1", color= Color.red, materialName = "", modelName = "Dudess"});
		trans.playerDatas.Add(new PlayerData(){name="игрок N2", color= Color.green, materialName = "", modelName = "Dude"});
		return trans;
	}
}
