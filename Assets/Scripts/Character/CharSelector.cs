﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CharSelector : MonoBehaviour 
{
	public Text lockedText;
	CharSelectorTransfer _transfer;
	Player _lastPlayer;
	int _currentPlayer = 0;
	// Use this for initialization
	void Start () 
	{
		_transfer = CharSelectorTransfer.GetOrCreate();
		_transfer.onEsc+= (int idx)=>{	
			if(GameObject.FindObjectOfType<PhotonLobby>())
			GameObject.DestroyImmediate(GameObject.FindObjectOfType<PhotonLobby>().gameObject);//?
		};
		_currentPlayer = _transfer.charIdx;
		ShowPlayer();
	}

	void ShowPlayer()
	{
		_lastPlayer = new PlayersSpawner().Spawn(_transfer.playerDatas[_currentPlayer]);
		_lastPlayer.GetComponentInChildren<Animator>().enabled = true;

		lockedText.text = _transfer.level<(_transfer.playerDatas[_currentPlayer].unlockLevel-1)?
			"Не доступно до "+_transfer.playerDatas[_currentPlayer].unlockLevel+"-го уровня":"";
	}

	public void OnSelect(bool right) 
	{
		if(_lastPlayer)
			DestroyImmediate(_lastPlayer.gameObject);
		_currentPlayer += right?1:-1;
		if(_currentPlayer<0)
			_currentPlayer = _transfer.playerDatas.Count-1;
		if(_currentPlayer == _transfer.playerDatas.Count)
			_currentPlayer = 0;
		ShowPlayer();
	}

	public void OnAccept()
	{
		if(_transfer.level<_transfer.playerDatas[_currentPlayer].unlockLevel-1)
		{
			MessageScreen screen = GameScreen.CreateScreen<MessageScreen>();
			screen.ShowInfo("Персонаж", "Чтобы выбрать этого персонажа достигни "+_transfer.playerDatas[_currentPlayer].unlockLevel+"-го уровня", ()=>{_transfer.onEsc(-1);});
			return;
		}
		Messager.SendStatistic(_transfer.profileName+" выбрал персонажа "+_currentPlayer);
		_transfer.onEsc(_currentPlayer);
	}

	void OnDestroy()
	{
		if(_transfer && _transfer.gameObject)
			DestroyImmediate(_transfer.gameObject);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && FindObjectOfType<GameScreen>() == null) 
		{
			_transfer.onEsc(-1);
		}
	}
}
