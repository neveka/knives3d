﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using OnePF;

public interface IIAPStore
{
    List<CurrencyIAPData> GetCurrencyIAPs( );
	bool IsBusy();
    IEnumerator PurchaseProduct( string sku , Action onSucсess , Action onFail );
    IEnumerator QueryPrice( string sku , Action<string> onGetPrice );
}

public class OpenIABStore : MonoBehaviour , IIAPStore
{
    public Action<string> statusChanged;

    protected List<CurrencyIAPData> _currencyIAPs;

    //results
    protected bool _isInitialized;
    protected Purchase _purchase;
    protected Inventory _inventory = null;

    protected string _status = "unknown";

    protected bool _isLoading;

	public bool IsBusy()
	{
		return _isLoading;
	}

    public List<CurrencyIAPData> GetCurrencyIAPs( )
    {
        return _currencyIAPs;
    }

    void Awake( )
    {
        OpenIABEventManager.billingSupportedEvent += billingSupportedEvent;
        OpenIABEventManager.billingNotSupportedEvent += billingNotSupportedEvent;
        OpenIABEventManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
        OpenIABEventManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
        OpenIABEventManager.purchaseSucceededEvent += purchaseSucceededEvent;
        OpenIABEventManager.purchaseFailedEvent += purchaseFailedEvent;
        OpenIABEventManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
        OpenIABEventManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
    }

    void OnDestroy( )
    {
        OpenIABEventManager.billingSupportedEvent -= billingSupportedEvent;
        OpenIABEventManager.billingNotSupportedEvent -= billingNotSupportedEvent;
        OpenIABEventManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
        OpenIABEventManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
        OpenIABEventManager.purchaseSucceededEvent -= purchaseSucceededEvent;
        OpenIABEventManager.purchaseFailedEvent -= purchaseFailedEvent;
        OpenIABEventManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
        OpenIABEventManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
    }

    public IEnumerator Init( List<CurrencyIAPData> currencyIAPs )
    {
        _currencyIAPs = currencyIAPs;
        for( int i = 0 ; i < currencyIAPs.Count ; i++ )
        {
            // Map skus for different stores      
#if UNITY_ANDROID
            OpenIAB.mapSku(currencyIAPs[i].sku, OpenIAB_Android.STORE_GOOGLE, currencyIAPs[i].sku);
#endif
#if UNITY_IOS
            OpenIAB.mapSku( currencyIAPs[ i ].sku , OpenIAB_iOS.STORE , currencyIAPs[ i ].sku );
#endif
        }
#if UNITY_ANDROID || UNITY_IOS
        var options = new Options( );
        //options.checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2;
        //options.discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2;
        //options.checkInventory = false;
        //options.verifyMode = OptionsVerifyMode.VERIFY_SKIP;//OptionsVerifyMode.VERIFY_EVERYTHING;
        //options.storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT;
#endif
#if UNITY_ANDROID
		// Application public key
		var googlePublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgF+rJ7G8KrDbbuCDACD/+zer5Hmg8ShHX3My8vXyw696QooNEnVQu3AlWXkq8jepU780pgKdxuD+OpTR9E0DOP4BOkhbIqTY0EXV07NVajGzu3p5aqcs+uVD4iunZgSyMdOWwocB4mHvilIKHTrD0Sqzh8PqDXwJGvsEuEi9TcwyviCr9v9N6pvUSfT/khKuEYfLgM9Qd07RV+JRdWfnHT3Bfnb1qrY2F9lbfJYqsvFJXitK0CaR3zKtMTqshviE/TRv7nko41a5/aF4AYbagTvES0iSr6yDz787M6Rc2sUHG3ngCkMlYiXNz3YwGn7zQP6xIwB19v96qNWPvLWzEwIDAQAB";
		options.storeKeys = new Dictionary<string, string> { { OpenIAB_Android.STORE_GOOGLE, googlePublicKey } };
#endif
        // Transmit options and start the service
		if(statusChanged != null)
        	statusChanged( "Init IAPs" );
#if UNITY_ANDROID || UNITY_IOS
        OpenIAB.init( options );
        _isLoading = true;
        if( Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor )
            _isLoading = false;
#endif
        while( _isLoading )
        {
            yield return null;
        }

    }

    public IEnumerator QueryPrice( string sku , Action<string> onGetPrice )//make one request?
    {
#if UNITY_ANDROID || UNITY_IOS
        if( _isInitialized && _inventory == null )
        {
            OpenIAB.queryInventory( _currencyIAPs.ConvertAll( i => i.sku ).ToArray( ) );
            _isLoading = true;
            while( _isLoading )
            {
                yield return null;
            }
        }
        string priceString = "Неизвестнo";
        if( _isInitialized && _inventory != null )
        {
            List<SkuDetails> list = _inventory.GetAllAvailableSkus( );
            if( list != null )
            {
                SkuDetails details = list.Find( d => d != null && d.Sku == sku );
                if( details != null )
                {
                    priceString = details.Price;
                }
            }
        }
        onGetPrice( priceString );
#else
        yield return new WaitForSeconds(3);//simulate delay
		onGetPrice(_currencyIAPs.Find(i => i.sku == sku).revenu + " RUB");
#endif
    }

    public IEnumerator PurchaseProduct( string sku , Action onSucсess , Action onFail )
    {
#if UNITY_ANDROID || UNITY_IOS
		if(_isLoading)
		{
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Покупка", "Подождите завершения предыдущей покупки.", onFail);
			yield break;
		}
        _purchase = null;
        if( _isInitialized )
        {
            OpenIAB.purchaseProduct( sku , "" );
            _isLoading = true;
            while( _isLoading )
            {
                yield return null;
            }
        }
		if( _isInitialized && _purchase != null && _purchase.Sku == sku )
		{
			OpenIAB.consumeProduct(_purchase);
			_isLoading = true;
			while( _isLoading )
			{
				yield return null;
			}
		}
        if( _isInitialized && _purchase != null && _purchase.Sku == sku )
        {
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Покупка", "Вы купили "+_purchase.AppstoreName , onSucсess);
        }
        else
        {
			GameScreen.CreateScreen<MessageScreen>().ShowInfo("Покупка", "Ошибка : "+(!_isInitialized?"Магазин не загрузился.":(_purchase == null?"Товар не найден.":"Не тот товар."))+_status, onFail);
        }
#else
        yield return new WaitForSeconds(3);//simulate delay
		GameScreen.CreateScreen<MessageScreen>().ShowInfo("Покупка", "Вы купили "+_currencyIAPs.Find(i=>i.sku == sku).name, onSucсess);
		//MessagePopup.ShowInfo(Localization.GetString("You purchased ") + _currencyIAPs.Find(i=>i.sku == sku).name, onSucсess, true, true);
#endif
    }

	/*public IEnumerator ConsumeProduct( string sku )
    {
        if( _inventory != null && _inventory.HasPurchase( sku ) )
        {
            OpenIAB.consumeProduct( _inventory.GetPurchase( sku ) );
            _isLoading = true;
            while( _isLoading )
            {
                yield return null;
            }
        }
    }*/

    private void SetStatus( string status )
    {
        _status = status;
        Debug.Log( _status );
    }

	private void ConsumeAllPurchases()
	{
		List<Purchase> purchases = _inventory.GetAllPurchases ();
		purchases.ForEach (p=>OpenIAB.consumeProduct(p));
	}

    private void billingSupportedEvent( )
    {
        _isInitialized = true;
        _isLoading = false;
        SetStatus( "billingSupported" );
    }

    private void billingNotSupportedEvent( string error )
    {
        _isLoading = false;
        SetStatus( "billingNotSupported: " + error );
    }

    private void queryInventorySucceededEvent( Inventory inventory )
    {
        SetStatus( "queryInvSucceeded: " + inventory );
        if( inventory != null )
        {
            _inventory = inventory;
        }
		ConsumeAllPurchases ();//temp
        _isLoading = false;
    }

    private void queryInventoryFailedEvent( string error )
    {
        SetStatus( "queryInvFailed: " + error );
        _inventory = null;
        _isLoading = false;
    }

    private void purchaseSucceededEvent( Purchase purchase )
    {
        SetStatus( "purchaseSucceeded: " + purchase );
        _purchase = purchase;
        _isLoading = false;
    }

    private void purchaseFailedEvent( int errorCode , string errorMessage )
    {
        SetStatus( "purchaseFailed: " + errorMessage );
        _purchase = null;
        _isLoading = false;
    }

    private void consumePurchaseSucceededEvent( Purchase purchase )
    {
        SetStatus( "consumePurchaseSucceeded: " + purchase );
        _purchase = purchase;
        _isLoading = false;
    }

    private void consumePurchaseFailedEvent( string error )
    {
        SetStatus( "consumePurchaseFailed: " + error );
        _purchase = null;
        _isLoading = false;
    }

#if UNITY_EDITOR
    //public void OnGUI( )
    //{
    //    GUI.Label( new Rect( Screen.width * 0.5f , Screen.height - 15 , Screen.width * 0.5f , Screen.height * 0.1f ) , "OpenIAB:" + _status );
    //}
#endif
}
